/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.util;

import org.apache.commons.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Signs strings and verifies signed strings using a SHA digest.
 */
public class Signer {public static class __CLR3_0_2tftfiw50z4nn{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,1094);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  private static final String SIGNATURE = "&s=";

  private byte[] secret;

  /**
   * Creates a Signer instance using the specified secret.
   *
   * @param secret secret to use for creating the digest.
   */
  public Signer(byte[] secret) {try{__CLR3_0_2tftfiw50z4nn.R.inc(1059);
    __CLR3_0_2tftfiw50z4nn.R.inc(1060);if ((((secret == null)&&(__CLR3_0_2tftfiw50z4nn.R.iget(1061)!=0|true))||(__CLR3_0_2tftfiw50z4nn.R.iget(1062)==0&false))) {{
      __CLR3_0_2tftfiw50z4nn.R.inc(1063);throw new IllegalArgumentException("secret cannot be NULL");
    }
    }__CLR3_0_2tftfiw50z4nn.R.inc(1064);this.secret = secret.clone();
  }finally{__CLR3_0_2tftfiw50z4nn.R.flushNeeded();}}

  /**
   * Returns a signed string.
   * <p/>
   * The signature '&s=SIGNATURE' is appended at the end of the string.
   *
   * @param str string to sign.
   *
   * @return the signed string.
   */
  public String sign(String str) {try{__CLR3_0_2tftfiw50z4nn.R.inc(1065);
    __CLR3_0_2tftfiw50z4nn.R.inc(1066);if ((((str == null || str.length() == 0)&&(__CLR3_0_2tftfiw50z4nn.R.iget(1067)!=0|true))||(__CLR3_0_2tftfiw50z4nn.R.iget(1068)==0&false))) {{
      __CLR3_0_2tftfiw50z4nn.R.inc(1069);throw new IllegalArgumentException("NULL or empty string to sign");
    }
    }__CLR3_0_2tftfiw50z4nn.R.inc(1070);String signature = computeSignature(str);
    __CLR3_0_2tftfiw50z4nn.R.inc(1071);return str + SIGNATURE + signature;
  }finally{__CLR3_0_2tftfiw50z4nn.R.flushNeeded();}}

  /**
   * Verifies a signed string and extracts the original string.
   *
   * @param signedStr the signed string to verify and extract.
   *
   * @return the extracted original string.
   *
   * @throws SignerException thrown if the given string is not a signed string or if the signature is invalid.
   */
  public String verifyAndExtract(String signedStr) throws SignerException {try{__CLR3_0_2tftfiw50z4nn.R.inc(1072);
    __CLR3_0_2tftfiw50z4nn.R.inc(1073);int index = signedStr.lastIndexOf(SIGNATURE);
    __CLR3_0_2tftfiw50z4nn.R.inc(1074);if ((((index == -1)&&(__CLR3_0_2tftfiw50z4nn.R.iget(1075)!=0|true))||(__CLR3_0_2tftfiw50z4nn.R.iget(1076)==0&false))) {{
      __CLR3_0_2tftfiw50z4nn.R.inc(1077);throw new SignerException("Invalid signed text: " + signedStr);
    }
    }__CLR3_0_2tftfiw50z4nn.R.inc(1078);String originalSignature = signedStr.substring(index + SIGNATURE.length());
    __CLR3_0_2tftfiw50z4nn.R.inc(1079);String rawValue = signedStr.substring(0, index);
    __CLR3_0_2tftfiw50z4nn.R.inc(1080);String currentSignature = computeSignature(rawValue);
    __CLR3_0_2tftfiw50z4nn.R.inc(1081);if ((((!originalSignature.equals(currentSignature))&&(__CLR3_0_2tftfiw50z4nn.R.iget(1082)!=0|true))||(__CLR3_0_2tftfiw50z4nn.R.iget(1083)==0&false))) {{
      __CLR3_0_2tftfiw50z4nn.R.inc(1084);throw new SignerException("Invalid signature");
    }
    }__CLR3_0_2tftfiw50z4nn.R.inc(1085);return rawValue;
  }finally{__CLR3_0_2tftfiw50z4nn.R.flushNeeded();}}

  /**
   * Returns then signature of a string.
   *
   * @param str string to sign.
   *
   * @return the signature for the string.
   */
  protected String computeSignature(String str) {try{__CLR3_0_2tftfiw50z4nn.R.inc(1086);
    __CLR3_0_2tftfiw50z4nn.R.inc(1087);try {
      __CLR3_0_2tftfiw50z4nn.R.inc(1088);MessageDigest md = MessageDigest.getInstance("SHA");
      __CLR3_0_2tftfiw50z4nn.R.inc(1089);md.update(str.getBytes());
      __CLR3_0_2tftfiw50z4nn.R.inc(1090);md.update(secret);
      __CLR3_0_2tftfiw50z4nn.R.inc(1091);byte[] digest = md.digest();
      __CLR3_0_2tftfiw50z4nn.R.inc(1092);return new Base64(0).encodeToString(digest);
    } catch (NoSuchAlgorithmException ex) {
      __CLR3_0_2tftfiw50z4nn.R.inc(1093);throw new RuntimeException("It should not happen, " + ex.getMessage(), ex);
    }
  }finally{__CLR3_0_2tftfiw50z4nn.R.flushNeeded();}}

}
