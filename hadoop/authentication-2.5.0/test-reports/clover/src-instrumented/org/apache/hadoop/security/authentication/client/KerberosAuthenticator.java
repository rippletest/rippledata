/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.apache.commons.codec.binary.Base64;
import org.apache.hadoop.security.authentication.util.KerberosUtil;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.ietf.jgss.Oid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Subject;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.AccessControlContext;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.HashMap;
import java.util.Map;

import static org.apache.hadoop.util.PlatformName.IBM_JAVA;

/**
 * The {@link KerberosAuthenticator} implements the Kerberos SPNEGO authentication sequence.
 * <p/>
 * It uses the default principal for the Kerberos cache (normally set via kinit).
 * <p/>
 * It falls back to the {@link PseudoAuthenticator} if the HTTP endpoint does not trigger an SPNEGO authentication
 * sequence.
 */
public class KerberosAuthenticator implements Authenticator {public static class __CLR3_0_22v2viw50z4h0{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,264);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  
  private static Logger LOG = LoggerFactory.getLogger(
      KerberosAuthenticator.class);

  /**
   * HTTP header used by the SPNEGO server endpoint during an authentication sequence.
   */
  public static final String WWW_AUTHENTICATE = "WWW-Authenticate";

  /**
   * HTTP header used by the SPNEGO client endpoint during an authentication sequence.
   */
  public static final String AUTHORIZATION = "Authorization";

  /**
   * HTTP header prefix used by the SPNEGO client/server endpoints during an authentication sequence.
   */
  public static final String NEGOTIATE = "Negotiate";

  private static final String AUTH_HTTP_METHOD = "OPTIONS";

  /*
  * Defines the Kerberos configuration that will be used to obtain the Kerberos principal from the
  * Kerberos cache.
  */
  private static class KerberosConfiguration extends Configuration {

    private static final String OS_LOGIN_MODULE_NAME;
    private static final boolean windows = System.getProperty("os.name").startsWith("Windows");
    private static final boolean is64Bit = System.getProperty("os.arch").contains("64");
    private static final boolean aix = System.getProperty("os.name").equals("AIX");

    /* Return the OS login module class name */
    private static String getOSLoginModuleName() {try{__CLR3_0_22v2viw50z4h0.R.inc(103);
      __CLR3_0_22v2viw50z4h0.R.inc(104);if ((((IBM_JAVA)&&(__CLR3_0_22v2viw50z4h0.R.iget(105)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(106)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(107);if ((((windows)&&(__CLR3_0_22v2viw50z4h0.R.iget(108)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(109)==0&false))) {{
          __CLR3_0_22v2viw50z4h0.R.inc(110);return (((is64Bit )&&(__CLR3_0_22v2viw50z4h0.R.iget(111)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(112)==0&false))? "com.ibm.security.auth.module.Win64LoginModule"
              : "com.ibm.security.auth.module.NTLoginModule";
        } }else {__CLR3_0_22v2viw50z4h0.R.inc(113);if ((((aix)&&(__CLR3_0_22v2viw50z4h0.R.iget(114)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(115)==0&false))) {{
          __CLR3_0_22v2viw50z4h0.R.inc(116);return (((is64Bit )&&(__CLR3_0_22v2viw50z4h0.R.iget(117)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(118)==0&false))? "com.ibm.security.auth.module.AIX64LoginModule"
              : "com.ibm.security.auth.module.AIXLoginModule";
        } }else {{
          __CLR3_0_22v2viw50z4h0.R.inc(119);return "com.ibm.security.auth.module.LinuxLoginModule";
        }
      }}} }else {{
        __CLR3_0_22v2viw50z4h0.R.inc(120);return (((windows )&&(__CLR3_0_22v2viw50z4h0.R.iget(121)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(122)==0&false))? "com.sun.security.auth.module.NTLoginModule"
            : "com.sun.security.auth.module.UnixLoginModule";
      }
    }}finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

    static {try{__CLR3_0_22v2viw50z4h0.R.inc(123);
      __CLR3_0_22v2viw50z4h0.R.inc(124);OS_LOGIN_MODULE_NAME = getOSLoginModuleName();
    }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

    private static final AppConfigurationEntry OS_SPECIFIC_LOGIN =
      new AppConfigurationEntry(OS_LOGIN_MODULE_NAME,
                                AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
                                new HashMap<String, String>());

    private static final Map<String, String> USER_KERBEROS_OPTIONS = new HashMap<String, String>();

    static {try{__CLR3_0_22v2viw50z4h0.R.inc(125);
      __CLR3_0_22v2viw50z4h0.R.inc(126);String ticketCache = System.getenv("KRB5CCNAME");
      __CLR3_0_22v2viw50z4h0.R.inc(127);if ((((IBM_JAVA)&&(__CLR3_0_22v2viw50z4h0.R.iget(128)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(129)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(130);USER_KERBEROS_OPTIONS.put("useDefaultCcache", "true");
      } }else {{
        __CLR3_0_22v2viw50z4h0.R.inc(131);USER_KERBEROS_OPTIONS.put("doNotPrompt", "true");
        __CLR3_0_22v2viw50z4h0.R.inc(132);USER_KERBEROS_OPTIONS.put("useTicketCache", "true");
      }
      }__CLR3_0_22v2viw50z4h0.R.inc(133);if ((((ticketCache != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(134)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(135)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(136);if ((((IBM_JAVA)&&(__CLR3_0_22v2viw50z4h0.R.iget(137)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(138)==0&false))) {{
          // The first value searched when "useDefaultCcache" is used.
          __CLR3_0_22v2viw50z4h0.R.inc(139);System.setProperty("KRB5CCNAME", ticketCache);
        } }else {{
          __CLR3_0_22v2viw50z4h0.R.inc(140);USER_KERBEROS_OPTIONS.put("ticketCache", ticketCache);
        }
      }}
      }__CLR3_0_22v2viw50z4h0.R.inc(141);USER_KERBEROS_OPTIONS.put("renewTGT", "true");
    }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

    private static final AppConfigurationEntry USER_KERBEROS_LOGIN =
      new AppConfigurationEntry(KerberosUtil.getKrb5LoginModuleName(),
                                AppConfigurationEntry.LoginModuleControlFlag.OPTIONAL,
                                USER_KERBEROS_OPTIONS);

    private static final AppConfigurationEntry[] USER_KERBEROS_CONF =
      new AppConfigurationEntry[]{OS_SPECIFIC_LOGIN, USER_KERBEROS_LOGIN};

    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String appName) {try{__CLR3_0_22v2viw50z4h0.R.inc(142);
      __CLR3_0_22v2viw50z4h0.R.inc(143);return USER_KERBEROS_CONF;
    }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}
  }
  
  private URL url;
  private HttpURLConnection conn;
  private Base64 base64;
  private ConnectionConfigurator connConfigurator;

  /**
   * Sets a {@link ConnectionConfigurator} instance to use for
   * configuring connections.
   *
   * @param configurator the {@link ConnectionConfigurator} instance.
   */
  @Override
  public void setConnectionConfigurator(ConnectionConfigurator configurator) {try{__CLR3_0_22v2viw50z4h0.R.inc(144);
    __CLR3_0_22v2viw50z4h0.R.inc(145);connConfigurator = configurator;
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /**
   * Performs SPNEGO authentication against the specified URL.
   * <p/>
   * If a token is given it does a NOP and returns the given token.
   * <p/>
   * If no token is given, it will perform the SPNEGO authentication sequence using an
   * HTTP <code>OPTIONS</code> request.
   *
   * @param url the URl to authenticate against.
   * @param token the authentication token being used for the user.
   *
   * @throws IOException if an IO error occurred.
   * @throws AuthenticationException if an authentication error occurred.
   */
  @Override
  public void authenticate(URL url, AuthenticatedURL.Token token)
    throws IOException, AuthenticationException {try{__CLR3_0_22v2viw50z4h0.R.inc(146);
    __CLR3_0_22v2viw50z4h0.R.inc(147);if ((((!token.isSet())&&(__CLR3_0_22v2viw50z4h0.R.iget(148)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(149)==0&false))) {{
      __CLR3_0_22v2viw50z4h0.R.inc(150);this.url = url;
      __CLR3_0_22v2viw50z4h0.R.inc(151);base64 = new Base64(0);
      __CLR3_0_22v2viw50z4h0.R.inc(152);conn = (HttpURLConnection) url.openConnection();
      __CLR3_0_22v2viw50z4h0.R.inc(153);if ((((connConfigurator != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(154)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(155)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(156);conn = connConfigurator.configure(conn);
      }
      }__CLR3_0_22v2viw50z4h0.R.inc(157);conn.setRequestMethod(AUTH_HTTP_METHOD);
      __CLR3_0_22v2viw50z4h0.R.inc(158);conn.connect();
      
      __CLR3_0_22v2viw50z4h0.R.inc(159);if ((((conn.getResponseCode() == HttpURLConnection.HTTP_OK)&&(__CLR3_0_22v2viw50z4h0.R.iget(160)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(161)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(162);LOG.debug("JDK performed authentication on our behalf.");
        // If the JDK already did the SPNEGO back-and-forth for
        // us, just pull out the token.
        __CLR3_0_22v2viw50z4h0.R.inc(163);AuthenticatedURL.extractToken(conn, token);
        __CLR3_0_22v2viw50z4h0.R.inc(164);return;
      } }else {__CLR3_0_22v2viw50z4h0.R.inc(165);if ((((isNegotiate())&&(__CLR3_0_22v2viw50z4h0.R.iget(166)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(167)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(168);LOG.debug("Performing our own SPNEGO sequence.");
        __CLR3_0_22v2viw50z4h0.R.inc(169);doSpnegoSequence(token);
      } }else {{
        __CLR3_0_22v2viw50z4h0.R.inc(170);LOG.debug("Using fallback authenticator sequence.");
        __CLR3_0_22v2viw50z4h0.R.inc(171);Authenticator auth = getFallBackAuthenticator();
        // Make sure that the fall back authenticator have the same
        // ConnectionConfigurator, since the method might be overridden.
        // Otherwise the fall back authenticator might not have the information
        // to make the connection (e.g., SSL certificates)
        __CLR3_0_22v2viw50z4h0.R.inc(172);auth.setConnectionConfigurator(connConfigurator);
        __CLR3_0_22v2viw50z4h0.R.inc(173);auth.authenticate(url, token);
      }
    }}}
  }}finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /**
   * If the specified URL does not support SPNEGO authentication, a fallback {@link Authenticator} will be used.
   * <p/>
   * This implementation returns a {@link PseudoAuthenticator}.
   *
   * @return the fallback {@link Authenticator}.
   */
  protected Authenticator getFallBackAuthenticator() {try{__CLR3_0_22v2viw50z4h0.R.inc(174);
    __CLR3_0_22v2viw50z4h0.R.inc(175);Authenticator auth = new PseudoAuthenticator();
    __CLR3_0_22v2viw50z4h0.R.inc(176);if ((((connConfigurator != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(177)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(178)==0&false))) {{
      __CLR3_0_22v2viw50z4h0.R.inc(179);auth.setConnectionConfigurator(connConfigurator);
    }
    }__CLR3_0_22v2viw50z4h0.R.inc(180);return auth;
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /*
  * Indicates if the response is starting a SPNEGO negotiation.
  */
  private boolean isNegotiate() throws IOException {try{__CLR3_0_22v2viw50z4h0.R.inc(181);
    __CLR3_0_22v2viw50z4h0.R.inc(182);boolean negotiate = false;
    __CLR3_0_22v2viw50z4h0.R.inc(183);if ((((conn.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED)&&(__CLR3_0_22v2viw50z4h0.R.iget(184)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(185)==0&false))) {{
      __CLR3_0_22v2viw50z4h0.R.inc(186);String authHeader = conn.getHeaderField(WWW_AUTHENTICATE);
      __CLR3_0_22v2viw50z4h0.R.inc(187);negotiate = authHeader != null && authHeader.trim().startsWith(NEGOTIATE);
    }
    }__CLR3_0_22v2viw50z4h0.R.inc(188);return negotiate;
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /**
   * Implements the SPNEGO authentication sequence interaction using the current default principal
   * in the Kerberos cache (normally set via kinit).
   *
   * @param token the authentication token being used for the user.
   *
   * @throws IOException if an IO error occurred.
   * @throws AuthenticationException if an authentication error occurred.
   */
  private void doSpnegoSequence(AuthenticatedURL.Token token) throws IOException, AuthenticationException {try{__CLR3_0_22v2viw50z4h0.R.inc(189);
    __CLR3_0_22v2viw50z4h0.R.inc(190);try {
      __CLR3_0_22v2viw50z4h0.R.inc(191);AccessControlContext context = AccessController.getContext();
      __CLR3_0_22v2viw50z4h0.R.inc(192);Subject subject = Subject.getSubject(context);
      __CLR3_0_22v2viw50z4h0.R.inc(193);if ((((subject == null)&&(__CLR3_0_22v2viw50z4h0.R.iget(194)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(195)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(196);LOG.debug("No subject in context, logging in");
        __CLR3_0_22v2viw50z4h0.R.inc(197);subject = new Subject();
        __CLR3_0_22v2viw50z4h0.R.inc(198);LoginContext login = new LoginContext("", subject,
            null, new KerberosConfiguration());
        __CLR3_0_22v2viw50z4h0.R.inc(199);login.login();
      }

      }__CLR3_0_22v2viw50z4h0.R.inc(200);if ((((LOG.isDebugEnabled())&&(__CLR3_0_22v2viw50z4h0.R.iget(201)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(202)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(203);LOG.debug("Using subject: " + subject);
      }
      }__CLR3_0_22v2viw50z4h0.R.inc(204);Subject.doAs(subject, new PrivilegedExceptionAction<Void>() {

        @Override
        public Void run() throws Exception {try{__CLR3_0_22v2viw50z4h0.R.inc(205);
          __CLR3_0_22v2viw50z4h0.R.inc(206);GSSContext gssContext = null;
          __CLR3_0_22v2viw50z4h0.R.inc(207);try {
            __CLR3_0_22v2viw50z4h0.R.inc(208);GSSManager gssManager = GSSManager.getInstance();
            __CLR3_0_22v2viw50z4h0.R.inc(209);String servicePrincipal = KerberosUtil.getServicePrincipal("HTTP",
                KerberosAuthenticator.this.url.getHost());
            __CLR3_0_22v2viw50z4h0.R.inc(210);Oid oid = KerberosUtil.getOidInstance("NT_GSS_KRB5_PRINCIPAL");
            __CLR3_0_22v2viw50z4h0.R.inc(211);GSSName serviceName = gssManager.createName(servicePrincipal,
                                                        oid);
            __CLR3_0_22v2viw50z4h0.R.inc(212);oid = KerberosUtil.getOidInstance("GSS_KRB5_MECH_OID");
            __CLR3_0_22v2viw50z4h0.R.inc(213);gssContext = gssManager.createContext(serviceName, oid, null,
                                                  GSSContext.DEFAULT_LIFETIME);
            __CLR3_0_22v2viw50z4h0.R.inc(214);gssContext.requestCredDeleg(true);
            __CLR3_0_22v2viw50z4h0.R.inc(215);gssContext.requestMutualAuth(true);

            __CLR3_0_22v2viw50z4h0.R.inc(216);byte[] inToken = new byte[0];
            __CLR3_0_22v2viw50z4h0.R.inc(217);byte[] outToken;
            __CLR3_0_22v2viw50z4h0.R.inc(218);boolean established = false;

            // Loop while the context is still not established
            __CLR3_0_22v2viw50z4h0.R.inc(219);while ((((!established)&&(__CLR3_0_22v2viw50z4h0.R.iget(220)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(221)==0&false))) {{
              __CLR3_0_22v2viw50z4h0.R.inc(222);outToken = gssContext.initSecContext(inToken, 0, inToken.length);
              __CLR3_0_22v2viw50z4h0.R.inc(223);if ((((outToken != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(224)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(225)==0&false))) {{
                __CLR3_0_22v2viw50z4h0.R.inc(226);sendToken(outToken);
              }

              }__CLR3_0_22v2viw50z4h0.R.inc(227);if ((((!gssContext.isEstablished())&&(__CLR3_0_22v2viw50z4h0.R.iget(228)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(229)==0&false))) {{
                __CLR3_0_22v2viw50z4h0.R.inc(230);inToken = readToken();
              } }else {{
                __CLR3_0_22v2viw50z4h0.R.inc(231);established = true;
              }
            }}
          }} finally {
            __CLR3_0_22v2viw50z4h0.R.inc(232);if ((((gssContext != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(233)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(234)==0&false))) {{
              __CLR3_0_22v2viw50z4h0.R.inc(235);gssContext.dispose();
              __CLR3_0_22v2viw50z4h0.R.inc(236);gssContext = null;
            }
          }}
          __CLR3_0_22v2viw50z4h0.R.inc(237);return null;
        }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}
      });
    } catch (PrivilegedActionException ex) {
      __CLR3_0_22v2viw50z4h0.R.inc(238);throw new AuthenticationException(ex.getException());
    } catch (LoginException ex) {
      __CLR3_0_22v2viw50z4h0.R.inc(239);throw new AuthenticationException(ex);
    }
    __CLR3_0_22v2viw50z4h0.R.inc(240);AuthenticatedURL.extractToken(conn, token);
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /*
  * Sends the Kerberos token to the server.
  */
  private void sendToken(byte[] outToken) throws IOException {try{__CLR3_0_22v2viw50z4h0.R.inc(241);
    __CLR3_0_22v2viw50z4h0.R.inc(242);String token = base64.encodeToString(outToken);
    __CLR3_0_22v2viw50z4h0.R.inc(243);conn = (HttpURLConnection) url.openConnection();
    __CLR3_0_22v2viw50z4h0.R.inc(244);if ((((connConfigurator != null)&&(__CLR3_0_22v2viw50z4h0.R.iget(245)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(246)==0&false))) {{
      __CLR3_0_22v2viw50z4h0.R.inc(247);conn = connConfigurator.configure(conn);
    }
    }__CLR3_0_22v2viw50z4h0.R.inc(248);conn.setRequestMethod(AUTH_HTTP_METHOD);
    __CLR3_0_22v2viw50z4h0.R.inc(249);conn.setRequestProperty(AUTHORIZATION, NEGOTIATE + " " + token);
    __CLR3_0_22v2viw50z4h0.R.inc(250);conn.connect();
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

  /*
  * Retrieves the Kerberos token returned by the server.
  */
  private byte[] readToken() throws IOException, AuthenticationException {try{__CLR3_0_22v2viw50z4h0.R.inc(251);
    __CLR3_0_22v2viw50z4h0.R.inc(252);int status = conn.getResponseCode();
    __CLR3_0_22v2viw50z4h0.R.inc(253);if ((((status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_UNAUTHORIZED)&&(__CLR3_0_22v2viw50z4h0.R.iget(254)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(255)==0&false))) {{
      __CLR3_0_22v2viw50z4h0.R.inc(256);String authHeader = conn.getHeaderField(WWW_AUTHENTICATE);
      __CLR3_0_22v2viw50z4h0.R.inc(257);if ((((authHeader == null || !authHeader.trim().startsWith(NEGOTIATE))&&(__CLR3_0_22v2viw50z4h0.R.iget(258)!=0|true))||(__CLR3_0_22v2viw50z4h0.R.iget(259)==0&false))) {{
        __CLR3_0_22v2viw50z4h0.R.inc(260);throw new AuthenticationException("Invalid SPNEGO sequence, '" + WWW_AUTHENTICATE +
                                          "' header incorrect: " + authHeader);
      }
      }__CLR3_0_22v2viw50z4h0.R.inc(261);String negotiation = authHeader.trim().substring((NEGOTIATE + " ").length()).trim();
      __CLR3_0_22v2viw50z4h0.R.inc(262);return base64.decode(negotiation);
    }
    }__CLR3_0_22v2viw50z4h0.R.inc(263);throw new AuthenticationException("Invalid SPNEGO sequence, status code: " + status);
  }finally{__CLR3_0_22v2viw50z4h0.R.flushNeeded();}}

}
