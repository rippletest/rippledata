/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ */package org.apache.hadoop.security.authentication.util;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class implements parsing and handling of Kerberos principal names. In
 * particular, it splits them apart and translates them down into local
 * operating system names.
 */
@SuppressWarnings("all")
@InterfaceAudience.LimitedPrivate({"HDFS", "MapReduce"})
@InterfaceStability.Evolving
public class KerberosName {public static class __CLR3_0_2loloiw50z4me{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,999);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  private static final Logger LOG = LoggerFactory.getLogger(KerberosName.class);

  /** The first component of the name */
  private final String serviceName;
  /** The second component of the name. It may be null. */
  private final String hostName;
  /** The realm of the name. */
  private final String realm;

  /**
   * A pattern that matches a Kerberos name with at most 2 components.
   */
  private static final Pattern nameParser =
    Pattern.compile("([^/@]*)(/([^/@]*))?@([^/@]*)");

  /**
   * A pattern that matches a string with out '$' and then a single
   * parameter with $n.
   */
  private static Pattern parameterPattern =
    Pattern.compile("([^$]*)(\\$(\\d*))?");

  /**
   * A pattern for parsing a auth_to_local rule.
   */
  private static final Pattern ruleParser =
    Pattern.compile("\\s*((DEFAULT)|(RULE:\\[(\\d*):([^\\]]*)](\\(([^)]*)\\))?"+
                    "(s/([^/]*)/([^/]*)/(g)?)?))/?(L)?");

  /**
   * A pattern that recognizes simple/non-simple names.
   */
  private static final Pattern nonSimplePattern = Pattern.compile("[/@]");

  /**
   * The list of translation rules.
   */
  private static List<Rule> rules;

  private static String defaultRealm;

  static {try{__CLR3_0_2loloiw50z4me.R.inc(780);
    __CLR3_0_2loloiw50z4me.R.inc(781);try {
      __CLR3_0_2loloiw50z4me.R.inc(782);defaultRealm = KerberosUtil.getDefaultRealm();
    } catch (Exception ke) {
        __CLR3_0_2loloiw50z4me.R.inc(783);LOG.debug("Kerberos krb5 configuration not found, setting default realm to empty");
        __CLR3_0_2loloiw50z4me.R.inc(784);defaultRealm="";
    }
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Create a name from the full Kerberos principal name.
   * @param name
   */
  public KerberosName(String name) {try{__CLR3_0_2loloiw50z4me.R.inc(785);
    __CLR3_0_2loloiw50z4me.R.inc(786);Matcher match = nameParser.matcher(name);
    __CLR3_0_2loloiw50z4me.R.inc(787);if ((((!match.matches())&&(__CLR3_0_2loloiw50z4me.R.iget(788)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(789)==0&false))) {{
      __CLR3_0_2loloiw50z4me.R.inc(790);if ((((name.contains("@"))&&(__CLR3_0_2loloiw50z4me.R.iget(791)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(792)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(793);throw new IllegalArgumentException("Malformed Kerberos name: " + name);
      } }else {{
        __CLR3_0_2loloiw50z4me.R.inc(794);serviceName = name;
        __CLR3_0_2loloiw50z4me.R.inc(795);hostName = null;
        __CLR3_0_2loloiw50z4me.R.inc(796);realm = null;
      }
    }} }else {{
      __CLR3_0_2loloiw50z4me.R.inc(797);serviceName = match.group(1);
      __CLR3_0_2loloiw50z4me.R.inc(798);hostName = match.group(3);
      __CLR3_0_2loloiw50z4me.R.inc(799);realm = match.group(4);
    }
  }}finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Get the configured default realm.
   * @return the default realm from the krb5.conf
   */
  public String getDefaultRealm() {try{__CLR3_0_2loloiw50z4me.R.inc(800);
    __CLR3_0_2loloiw50z4me.R.inc(801);return defaultRealm;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Put the name back together from the parts.
   */
  @Override
  public String toString() {try{__CLR3_0_2loloiw50z4me.R.inc(802);
    __CLR3_0_2loloiw50z4me.R.inc(803);StringBuilder result = new StringBuilder();
    __CLR3_0_2loloiw50z4me.R.inc(804);result.append(serviceName);
    __CLR3_0_2loloiw50z4me.R.inc(805);if ((((hostName != null)&&(__CLR3_0_2loloiw50z4me.R.iget(806)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(807)==0&false))) {{
      __CLR3_0_2loloiw50z4me.R.inc(808);result.append('/');
      __CLR3_0_2loloiw50z4me.R.inc(809);result.append(hostName);
    }
    }__CLR3_0_2loloiw50z4me.R.inc(810);if ((((realm != null)&&(__CLR3_0_2loloiw50z4me.R.iget(811)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(812)==0&false))) {{
      __CLR3_0_2loloiw50z4me.R.inc(813);result.append('@');
      __CLR3_0_2loloiw50z4me.R.inc(814);result.append(realm);
    }
    }__CLR3_0_2loloiw50z4me.R.inc(815);return result.toString();
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Get the first component of the name.
   * @return the first section of the Kerberos principal name
   */
  public String getServiceName() {try{__CLR3_0_2loloiw50z4me.R.inc(816);
    __CLR3_0_2loloiw50z4me.R.inc(817);return serviceName;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Get the second component of the name.
   * @return the second section of the Kerberos principal name, and may be null
   */
  public String getHostName() {try{__CLR3_0_2loloiw50z4me.R.inc(818);
    __CLR3_0_2loloiw50z4me.R.inc(819);return hostName;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Get the realm of the name.
   * @return the realm of the name, may be null
   */
  public String getRealm() {try{__CLR3_0_2loloiw50z4me.R.inc(820);
    __CLR3_0_2loloiw50z4me.R.inc(821);return realm;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * An encoding of a rule for translating kerberos names.
   */
  private static class Rule {
    private final boolean isDefault;
    private final int numOfComponents;
    private final String format;
    private final Pattern match;
    private final Pattern fromPattern;
    private final String toPattern;
    private final boolean repeat;
    private final boolean toLowerCase;

    Rule() {try{__CLR3_0_2loloiw50z4me.R.inc(822);
      __CLR3_0_2loloiw50z4me.R.inc(823);isDefault = true;
      __CLR3_0_2loloiw50z4me.R.inc(824);numOfComponents = 0;
      __CLR3_0_2loloiw50z4me.R.inc(825);format = null;
      __CLR3_0_2loloiw50z4me.R.inc(826);match = null;
      __CLR3_0_2loloiw50z4me.R.inc(827);fromPattern = null;
      __CLR3_0_2loloiw50z4me.R.inc(828);toPattern = null;
      __CLR3_0_2loloiw50z4me.R.inc(829);repeat = false;
      __CLR3_0_2loloiw50z4me.R.inc(830);toLowerCase = false;
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

    Rule(int numOfComponents, String format, String match, String fromPattern,
         String toPattern, boolean repeat, boolean toLowerCase) {try{__CLR3_0_2loloiw50z4me.R.inc(831);
      __CLR3_0_2loloiw50z4me.R.inc(832);isDefault = false;
      __CLR3_0_2loloiw50z4me.R.inc(833);this.numOfComponents = numOfComponents;
      __CLR3_0_2loloiw50z4me.R.inc(834);this.format = format;
      __CLR3_0_2loloiw50z4me.R.inc(835);this.match = (((match == null )&&(__CLR3_0_2loloiw50z4me.R.iget(836)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(837)==0&false))? null : Pattern.compile(match);
      __CLR3_0_2loloiw50z4me.R.inc(838);this.fromPattern =
        (((fromPattern == null )&&(__CLR3_0_2loloiw50z4me.R.iget(839)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(840)==0&false))? null : Pattern.compile(fromPattern);
      __CLR3_0_2loloiw50z4me.R.inc(841);this.toPattern = toPattern;
      __CLR3_0_2loloiw50z4me.R.inc(842);this.repeat = repeat;
      __CLR3_0_2loloiw50z4me.R.inc(843);this.toLowerCase = toLowerCase;
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

    @Override
    public String toString() {try{__CLR3_0_2loloiw50z4me.R.inc(844);
      __CLR3_0_2loloiw50z4me.R.inc(845);StringBuilder buf = new StringBuilder();
      __CLR3_0_2loloiw50z4me.R.inc(846);if ((((isDefault)&&(__CLR3_0_2loloiw50z4me.R.iget(847)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(848)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(849);buf.append("DEFAULT");
      } }else {{
        __CLR3_0_2loloiw50z4me.R.inc(850);buf.append("RULE:[");
        __CLR3_0_2loloiw50z4me.R.inc(851);buf.append(numOfComponents);
        __CLR3_0_2loloiw50z4me.R.inc(852);buf.append(':');
        __CLR3_0_2loloiw50z4me.R.inc(853);buf.append(format);
        __CLR3_0_2loloiw50z4me.R.inc(854);buf.append(']');
        __CLR3_0_2loloiw50z4me.R.inc(855);if ((((match != null)&&(__CLR3_0_2loloiw50z4me.R.iget(856)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(857)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(858);buf.append('(');
          __CLR3_0_2loloiw50z4me.R.inc(859);buf.append(match);
          __CLR3_0_2loloiw50z4me.R.inc(860);buf.append(')');
        }
        }__CLR3_0_2loloiw50z4me.R.inc(861);if ((((fromPattern != null)&&(__CLR3_0_2loloiw50z4me.R.iget(862)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(863)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(864);buf.append("s/");
          __CLR3_0_2loloiw50z4me.R.inc(865);buf.append(fromPattern);
          __CLR3_0_2loloiw50z4me.R.inc(866);buf.append('/');
          __CLR3_0_2loloiw50z4me.R.inc(867);buf.append(toPattern);
          __CLR3_0_2loloiw50z4me.R.inc(868);buf.append('/');
          __CLR3_0_2loloiw50z4me.R.inc(869);if ((((repeat)&&(__CLR3_0_2loloiw50z4me.R.iget(870)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(871)==0&false))) {{
            __CLR3_0_2loloiw50z4me.R.inc(872);buf.append('g');
          }
        }}
        }__CLR3_0_2loloiw50z4me.R.inc(873);if ((((toLowerCase)&&(__CLR3_0_2loloiw50z4me.R.iget(874)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(875)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(876);buf.append("/L");
        }
      }}
      }__CLR3_0_2loloiw50z4me.R.inc(877);return buf.toString();
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

    /**
     * Replace the numbered parameters of the form $n where n is from 1 to
     * the length of params. Normal text is copied directly and $n is replaced
     * by the corresponding parameter.
     * @param format the string to replace parameters again
     * @param params the list of parameters
     * @return the generated string with the parameter references replaced.
     * @throws BadFormatString
     */
    static String replaceParameters(String format,
                                    String[] params) throws BadFormatString {try{__CLR3_0_2loloiw50z4me.R.inc(878);
      __CLR3_0_2loloiw50z4me.R.inc(879);Matcher match = parameterPattern.matcher(format);
      __CLR3_0_2loloiw50z4me.R.inc(880);int start = 0;
      __CLR3_0_2loloiw50z4me.R.inc(881);StringBuilder result = new StringBuilder();
      __CLR3_0_2loloiw50z4me.R.inc(882);while ((((start < format.length() && match.find(start))&&(__CLR3_0_2loloiw50z4me.R.iget(883)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(884)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(885);result.append(match.group(1));
        __CLR3_0_2loloiw50z4me.R.inc(886);String paramNum = match.group(3);
        __CLR3_0_2loloiw50z4me.R.inc(887);if ((((paramNum != null)&&(__CLR3_0_2loloiw50z4me.R.iget(888)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(889)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(890);try {
            __CLR3_0_2loloiw50z4me.R.inc(891);int num = Integer.parseInt(paramNum);
            __CLR3_0_2loloiw50z4me.R.inc(892);if ((((num < 0 || num > params.length)&&(__CLR3_0_2loloiw50z4me.R.iget(893)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(894)==0&false))) {{
              __CLR3_0_2loloiw50z4me.R.inc(895);throw new BadFormatString("index " + num + " from " + format +
                                        " is outside of the valid range 0 to " +
                                        (params.length - 1));
            }
            }__CLR3_0_2loloiw50z4me.R.inc(896);result.append(params[num]);
          } catch (NumberFormatException nfe) {
            __CLR3_0_2loloiw50z4me.R.inc(897);throw new BadFormatString("bad format in username mapping in " +
                                      paramNum, nfe);
          }

        }
        }__CLR3_0_2loloiw50z4me.R.inc(898);start = match.end();
      }
      }__CLR3_0_2loloiw50z4me.R.inc(899);return result.toString();
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

    /**
     * Replace the matches of the from pattern in the base string with the value
     * of the to string.
     * @param base the string to transform
     * @param from the pattern to look for in the base string
     * @param to the string to replace matches of the pattern with
     * @param repeat whether the substitution should be repeated
     * @return
     */
    static String replaceSubstitution(String base, Pattern from, String to,
                                      boolean repeat) {try{__CLR3_0_2loloiw50z4me.R.inc(900);
      __CLR3_0_2loloiw50z4me.R.inc(901);Matcher match = from.matcher(base);
      __CLR3_0_2loloiw50z4me.R.inc(902);if ((((repeat)&&(__CLR3_0_2loloiw50z4me.R.iget(903)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(904)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(905);return match.replaceAll(to);
      } }else {{
        __CLR3_0_2loloiw50z4me.R.inc(906);return match.replaceFirst(to);
      }
    }}finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

    /**
     * Try to apply this rule to the given name represented as a parameter
     * array.
     * @param params first element is the realm, second and later elements are
     *        are the components of the name "a/b@FOO" -> {"FOO", "a", "b"}
     * @return the short name if this rule applies or null
     * @throws IOException throws if something is wrong with the rules
     */
    String apply(String[] params) throws IOException {try{__CLR3_0_2loloiw50z4me.R.inc(907);
      __CLR3_0_2loloiw50z4me.R.inc(908);String result = null;
      __CLR3_0_2loloiw50z4me.R.inc(909);if ((((isDefault)&&(__CLR3_0_2loloiw50z4me.R.iget(910)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(911)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(912);if ((((defaultRealm.equals(params[0]))&&(__CLR3_0_2loloiw50z4me.R.iget(913)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(914)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(915);result = params[1];
        }
      }} }else {__CLR3_0_2loloiw50z4me.R.inc(916);if ((((params.length - 1 == numOfComponents)&&(__CLR3_0_2loloiw50z4me.R.iget(917)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(918)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(919);String base = replaceParameters(format, params);
        __CLR3_0_2loloiw50z4me.R.inc(920);if ((((match == null || match.matcher(base).matches())&&(__CLR3_0_2loloiw50z4me.R.iget(921)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(922)==0&false))) {{
          __CLR3_0_2loloiw50z4me.R.inc(923);if ((((fromPattern == null)&&(__CLR3_0_2loloiw50z4me.R.iget(924)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(925)==0&false))) {{
            __CLR3_0_2loloiw50z4me.R.inc(926);result = base;
          } }else {{
            __CLR3_0_2loloiw50z4me.R.inc(927);result = replaceSubstitution(base, fromPattern, toPattern,  repeat);
          }
        }}
      }}
      }}__CLR3_0_2loloiw50z4me.R.inc(928);if ((((result != null && nonSimplePattern.matcher(result).find())&&(__CLR3_0_2loloiw50z4me.R.iget(929)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(930)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(931);throw new NoMatchingRule("Non-simple name " + result +
                                 " after auth_to_local rule " + this);
      }
      }__CLR3_0_2loloiw50z4me.R.inc(932);if ((((toLowerCase && result != null)&&(__CLR3_0_2loloiw50z4me.R.iget(933)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(934)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(935);result = result.toLowerCase(Locale.ENGLISH);
      }
      }__CLR3_0_2loloiw50z4me.R.inc(936);return result;
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
  }

  static List<Rule> parseRules(String rules) {try{__CLR3_0_2loloiw50z4me.R.inc(937);
    __CLR3_0_2loloiw50z4me.R.inc(938);List<Rule> result = new ArrayList<Rule>();
    __CLR3_0_2loloiw50z4me.R.inc(939);String remaining = rules.trim();
    __CLR3_0_2loloiw50z4me.R.inc(940);while ((((remaining.length() > 0)&&(__CLR3_0_2loloiw50z4me.R.iget(941)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(942)==0&false))) {{
      __CLR3_0_2loloiw50z4me.R.inc(943);Matcher matcher = ruleParser.matcher(remaining);
      __CLR3_0_2loloiw50z4me.R.inc(944);if ((((!matcher.lookingAt())&&(__CLR3_0_2loloiw50z4me.R.iget(945)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(946)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(947);throw new IllegalArgumentException("Invalid rule: " + remaining);
      }
      }__CLR3_0_2loloiw50z4me.R.inc(948);if ((((matcher.group(2) != null)&&(__CLR3_0_2loloiw50z4me.R.iget(949)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(950)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(951);result.add(new Rule());
      } }else {{
        __CLR3_0_2loloiw50z4me.R.inc(952);result.add(new Rule(Integer.parseInt(matcher.group(4)),
                            matcher.group(5),
                            matcher.group(7),
                            matcher.group(9),
                            matcher.group(10),
                            "g".equals(matcher.group(11)),
                            "L".equals(matcher.group(12))));
      }
      }__CLR3_0_2loloiw50z4me.R.inc(953);remaining = remaining.substring(matcher.end());
    }
    }__CLR3_0_2loloiw50z4me.R.inc(954);return result;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  @SuppressWarnings("serial")
  public static class BadFormatString extends IOException {
    BadFormatString(String msg) {
      super(msg);__CLR3_0_2loloiw50z4me.R.inc(956);try{__CLR3_0_2loloiw50z4me.R.inc(955);
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
    BadFormatString(String msg, Throwable err) {
      super(msg, err);__CLR3_0_2loloiw50z4me.R.inc(958);try{__CLR3_0_2loloiw50z4me.R.inc(957);
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
  }

  @SuppressWarnings("serial")
  public static class NoMatchingRule extends IOException {
    NoMatchingRule(String msg) {
      super(msg);__CLR3_0_2loloiw50z4me.R.inc(960);try{__CLR3_0_2loloiw50z4me.R.inc(959);
    }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
  }

  /**
   * Get the translation of the principal name into an operating system
   * user name.
   * @return the short name
   * @throws IOException
   */
  public String getShortName() throws IOException {try{__CLR3_0_2loloiw50z4me.R.inc(961);
    __CLR3_0_2loloiw50z4me.R.inc(962);String[] params;
    __CLR3_0_2loloiw50z4me.R.inc(963);if ((((hostName == null)&&(__CLR3_0_2loloiw50z4me.R.iget(964)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(965)==0&false))) {{
      // if it is already simple, just return it
      __CLR3_0_2loloiw50z4me.R.inc(966);if ((((realm == null)&&(__CLR3_0_2loloiw50z4me.R.iget(967)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(968)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(969);return serviceName;
      }
      }__CLR3_0_2loloiw50z4me.R.inc(970);params = new String[]{realm, serviceName};
    } }else {{
      __CLR3_0_2loloiw50z4me.R.inc(971);params = new String[]{realm, serviceName, hostName};
    }
    }__CLR3_0_2loloiw50z4me.R.inc(972);for(Rule r: rules) {{
      __CLR3_0_2loloiw50z4me.R.inc(973);String result = r.apply(params);
      __CLR3_0_2loloiw50z4me.R.inc(974);if ((((result != null)&&(__CLR3_0_2loloiw50z4me.R.iget(975)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(976)==0&false))) {{
        __CLR3_0_2loloiw50z4me.R.inc(977);return result;
      }
    }}
    }__CLR3_0_2loloiw50z4me.R.inc(978);throw new NoMatchingRule("No rules applied to " + toString());
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Set the rules.
   * @param ruleString the rules string.
   */
  public static void setRules(String ruleString) {try{__CLR3_0_2loloiw50z4me.R.inc(979);
    __CLR3_0_2loloiw50z4me.R.inc(980);rules = ((((ruleString != null) )&&(__CLR3_0_2loloiw50z4me.R.iget(981)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(982)==0&false))? parseRules(ruleString) : null;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

  /**
   * Get the rules.
   * @return String of configured rules, or null if not yet configured
   */
  public static String getRules() {try{__CLR3_0_2loloiw50z4me.R.inc(983);
    __CLR3_0_2loloiw50z4me.R.inc(984);String ruleString = null;
    __CLR3_0_2loloiw50z4me.R.inc(985);if ((((rules != null)&&(__CLR3_0_2loloiw50z4me.R.iget(986)!=0|true))||(__CLR3_0_2loloiw50z4me.R.iget(987)==0&false))) {{
      __CLR3_0_2loloiw50z4me.R.inc(988);StringBuilder sb = new StringBuilder();
      __CLR3_0_2loloiw50z4me.R.inc(989);for (Rule rule : rules) {{
        __CLR3_0_2loloiw50z4me.R.inc(990);sb.append(rule.toString()).append("\n");
      }
      }__CLR3_0_2loloiw50z4me.R.inc(991);ruleString = sb.toString().trim();
    }
    }__CLR3_0_2loloiw50z4me.R.inc(992);return ruleString;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
  
  /**
   * Indicates if the name rules have been set.
   * 
   * @return if the name rules have been set.
   */
  public static boolean hasRulesBeenSet() {try{__CLR3_0_2loloiw50z4me.R.inc(993);
    __CLR3_0_2loloiw50z4me.R.inc(994);return rules != null;
  }finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}
  
  static void printRules() throws IOException {try{__CLR3_0_2loloiw50z4me.R.inc(995);
    __CLR3_0_2loloiw50z4me.R.inc(996);int i = 0;
    __CLR3_0_2loloiw50z4me.R.inc(997);for(Rule r: rules) {{
      __CLR3_0_2loloiw50z4me.R.inc(998);System.out.println(++i + " " + r);
    }
  }}finally{__CLR3_0_2loloiw50z4me.R.flushNeeded();}}

}
