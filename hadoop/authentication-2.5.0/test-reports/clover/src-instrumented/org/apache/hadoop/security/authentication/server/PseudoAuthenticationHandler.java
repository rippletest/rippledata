/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.client.PseudoAuthenticator;

import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.NameValuePair;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;

/**
 * The <code>PseudoAuthenticationHandler</code> provides a pseudo authentication mechanism that accepts
 * the user name specified as a query string parameter.
 * <p/>
 * This mimics the model of Hadoop Simple authentication which trust the 'user.name' property provided in
 * the configuration object.
 * <p/>
 * This handler can be configured to support anonymous users.
 * <p/>
 * The only supported configuration property is:
 * <ul>
 * <li>simple.anonymous.allowed: <code>true|false</code>, default value is <code>false</code></li>
 * </ul>
 */
public class PseudoAuthenticationHandler implements AuthenticationHandler {public static class __CLR3_0_2krkriw50z4l9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,780);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /**
   * Constant that identifies the authentication mechanism.
   */
  public static final String TYPE = "simple";

  /**
   * Constant for the configuration property that indicates if anonymous users are allowed.
   */
  public static final String ANONYMOUS_ALLOWED = TYPE + ".anonymous.allowed";

  private static final Charset UTF8_CHARSET = Charset.forName("UTF-8");
  private boolean acceptAnonymous;

  /**
   * Initializes the authentication handler instance.
   * <p/>
   * This method is invoked by the {@link AuthenticationFilter#init} method.
   *
   * @param config configuration properties to initialize the handler.
   *
   * @throws ServletException thrown if the handler could not be initialized.
   */
  @Override
  public void init(Properties config) throws ServletException {try{__CLR3_0_2krkriw50z4l9.R.inc(747);
    __CLR3_0_2krkriw50z4l9.R.inc(748);acceptAnonymous = Boolean.parseBoolean(config.getProperty(ANONYMOUS_ALLOWED, "false"));
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  /**
   * Returns if the handler is configured to support anonymous users.
   *
   * @return if the handler is configured to support anonymous users.
   */
  protected boolean getAcceptAnonymous() {try{__CLR3_0_2krkriw50z4l9.R.inc(749);
    __CLR3_0_2krkriw50z4l9.R.inc(750);return acceptAnonymous;
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  /**
   * Releases any resources initialized by the authentication handler.
   * <p/>
   * This implementation does a NOP.
   */
  @Override
  public void destroy() {try{__CLR3_0_2krkriw50z4l9.R.inc(751);
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  /**
   * Returns the authentication type of the authentication handler, 'simple'.
   * <p/>
   *
   * @return the authentication type of the authentication handler, 'simple'.
   */
  @Override
  public String getType() {try{__CLR3_0_2krkriw50z4l9.R.inc(752);
    __CLR3_0_2krkriw50z4l9.R.inc(753);return TYPE;
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  /**
   * This is an empty implementation, it always returns <code>TRUE</code>.
   *
   *
   *
   * @param token the authentication token if any, otherwise <code>NULL</code>.
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   *
   * @return <code>TRUE</code>
   * @throws IOException it is never thrown.
   * @throws AuthenticationException it is never thrown.
   */
  @Override
  public boolean managementOperation(AuthenticationToken token,
                                     HttpServletRequest request,
                                     HttpServletResponse response)
    throws IOException, AuthenticationException {try{__CLR3_0_2krkriw50z4l9.R.inc(754);
    __CLR3_0_2krkriw50z4l9.R.inc(755);return true;
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  private String getUserName(HttpServletRequest request) {try{__CLR3_0_2krkriw50z4l9.R.inc(756);
    __CLR3_0_2krkriw50z4l9.R.inc(757);List<NameValuePair> list = URLEncodedUtils.parse(request.getQueryString(), UTF8_CHARSET);
    __CLR3_0_2krkriw50z4l9.R.inc(758);if ((((list != null)&&(__CLR3_0_2krkriw50z4l9.R.iget(759)!=0|true))||(__CLR3_0_2krkriw50z4l9.R.iget(760)==0&false))) {{
      __CLR3_0_2krkriw50z4l9.R.inc(761);for (NameValuePair nv : list) {{
        __CLR3_0_2krkriw50z4l9.R.inc(762);if ((((PseudoAuthenticator.USER_NAME.equals(nv.getName()))&&(__CLR3_0_2krkriw50z4l9.R.iget(763)!=0|true))||(__CLR3_0_2krkriw50z4l9.R.iget(764)==0&false))) {{
          __CLR3_0_2krkriw50z4l9.R.inc(765);return nv.getValue();
        }
      }}
    }}
    }__CLR3_0_2krkriw50z4l9.R.inc(766);return null;
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

  /**
   * Authenticates an HTTP client request.
   * <p/>
   * It extracts the {@link PseudoAuthenticator#USER_NAME} parameter from the query string and creates
   * an {@link AuthenticationToken} with it.
   * <p/>
   * If the HTTP client request does not contain the {@link PseudoAuthenticator#USER_NAME} parameter and
   * the handler is configured to allow anonymous users it returns the {@link AuthenticationToken#ANONYMOUS}
   * token.
   * <p/>
   * If the HTTP client request does not contain the {@link PseudoAuthenticator#USER_NAME} parameter and
   * the handler is configured to disallow anonymous users it throws an {@link AuthenticationException}.
   *
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   *
   * @return an authentication token if the HTTP client request is accepted and credentials are valid.
   *
   * @throws IOException thrown if an IO error occurred.
   * @throws AuthenticationException thrown if HTTP client request was not accepted as an authentication request.
   */
  @Override
  public AuthenticationToken authenticate(HttpServletRequest request, HttpServletResponse response)
    throws IOException, AuthenticationException {try{__CLR3_0_2krkriw50z4l9.R.inc(767);
    __CLR3_0_2krkriw50z4l9.R.inc(768);AuthenticationToken token;
    __CLR3_0_2krkriw50z4l9.R.inc(769);String userName = getUserName(request);
    __CLR3_0_2krkriw50z4l9.R.inc(770);if ((((userName == null)&&(__CLR3_0_2krkriw50z4l9.R.iget(771)!=0|true))||(__CLR3_0_2krkriw50z4l9.R.iget(772)==0&false))) {{
      __CLR3_0_2krkriw50z4l9.R.inc(773);if ((((getAcceptAnonymous())&&(__CLR3_0_2krkriw50z4l9.R.iget(774)!=0|true))||(__CLR3_0_2krkriw50z4l9.R.iget(775)==0&false))) {{
        __CLR3_0_2krkriw50z4l9.R.inc(776);token = AuthenticationToken.ANONYMOUS;
      } }else {{
        __CLR3_0_2krkriw50z4l9.R.inc(777);throw new AuthenticationException("Anonymous requests are disallowed");
      }
    }} }else {{
      __CLR3_0_2krkriw50z4l9.R.inc(778);token = new AuthenticationToken(userName, userName, getType());
    }
    }__CLR3_0_2krkriw50z4l9.R.inc(779);return token;
  }finally{__CLR3_0_2krkriw50z4l9.R.flushNeeded();}}

}
