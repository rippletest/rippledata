/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

/**
 * Exception thrown when an authentication error occurrs.
 */
public class AuthenticationException extends Exception {public static class __CLR3_0_22p2piw50z4g9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,103);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  
  static final long serialVersionUID = 0;

  /**
   * Creates an {@link AuthenticationException}.
   *
   * @param cause original exception.
   */
  public AuthenticationException(Throwable cause) {
    super(cause);__CLR3_0_22p2piw50z4g9.R.inc(98);try{__CLR3_0_22p2piw50z4g9.R.inc(97);
  }finally{__CLR3_0_22p2piw50z4g9.R.flushNeeded();}}

  /**
   * Creates an {@link AuthenticationException}.
   *
   * @param msg exception message.
   */
  public AuthenticationException(String msg) {
    super(msg);__CLR3_0_22p2piw50z4g9.R.inc(100);try{__CLR3_0_22p2piw50z4g9.R.inc(99);
  }finally{__CLR3_0_22p2piw50z4g9.R.flushNeeded();}}

  /**
   * Creates an {@link AuthenticationException}.
   *
   * @param msg exception message.
   * @param cause original exception.
   */
  public AuthenticationException(String msg, Throwable cause) {
    super(msg, cause);__CLR3_0_22p2piw50z4g9.R.inc(102);try{__CLR3_0_22p2piw50z4g9.R.inc(101);
  }finally{__CLR3_0_22p2piw50z4g9.R.flushNeeded();}}
}
