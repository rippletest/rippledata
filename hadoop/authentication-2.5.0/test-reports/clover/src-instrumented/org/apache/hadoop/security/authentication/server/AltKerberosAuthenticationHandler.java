/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.hadoop.security.authentication.client.AuthenticationException;

 /**
 * The {@link AltKerberosAuthenticationHandler} behaves exactly the same way as
 * the {@link KerberosAuthenticationHandler}, except that it allows for an
 * alternative form of authentication for browsers while still using Kerberos
 * for Java access.  This is an abstract class that should be subclassed
 * to allow a developer to implement their own custom authentication for browser
 * access.  The alternateAuthenticate method will be called whenever a request
 * comes from a browser.
 * <p/>
 */
public abstract class AltKerberosAuthenticationHandler
                        extends KerberosAuthenticationHandler {public static class __CLR3_0_27v7viw50z4hr{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,314);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /**
   * Constant that identifies the authentication mechanism.
   */
  public static final String TYPE = "alt-kerberos";

  /**
   * Constant for the configuration property that indicates which user agents
   * are not considered browsers (comma separated)
   */
  public static final String NON_BROWSER_USER_AGENTS =
          TYPE + ".non-browser.user-agents";
  private static final String NON_BROWSER_USER_AGENTS_DEFAULT =
          "java,curl,wget,perl";

  private String[] nonBrowserUserAgents;

  /**
   * Returns the authentication type of the authentication handler,
   * 'alt-kerberos'.
   * <p/>
   *
   * @return the authentication type of the authentication handler,
   * 'alt-kerberos'.
   */
  @Override
  public String getType() {try{__CLR3_0_27v7viw50z4hr.R.inc(283);
    __CLR3_0_27v7viw50z4hr.R.inc(284);return TYPE;
  }finally{__CLR3_0_27v7viw50z4hr.R.flushNeeded();}}

  @Override
  public void init(Properties config) throws ServletException {try{__CLR3_0_27v7viw50z4hr.R.inc(285);
    __CLR3_0_27v7viw50z4hr.R.inc(286);super.init(config);

    __CLR3_0_27v7viw50z4hr.R.inc(287);nonBrowserUserAgents = config.getProperty(
            NON_BROWSER_USER_AGENTS, NON_BROWSER_USER_AGENTS_DEFAULT)
            .split("\\W*,\\W*");
    __CLR3_0_27v7viw50z4hr.R.inc(288);for (int i = 0; (((i < nonBrowserUserAgents.length)&&(__CLR3_0_27v7viw50z4hr.R.iget(289)!=0|true))||(__CLR3_0_27v7viw50z4hr.R.iget(290)==0&false)); i++) {{
        __CLR3_0_27v7viw50z4hr.R.inc(291);nonBrowserUserAgents[i] = nonBrowserUserAgents[i].toLowerCase();
    }
  }}finally{__CLR3_0_27v7viw50z4hr.R.flushNeeded();}}

  /**
   * It enforces the the Kerberos SPNEGO authentication sequence returning an
   * {@link AuthenticationToken} only after the Kerberos SPNEGO sequence has
   * completed successfully (in the case of Java access) and only after the
   * custom authentication implemented by the subclass in alternateAuthenticate
   * has completed successfully (in the case of browser access).
   * <p/>
   *
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   *
   * @return an authentication token if the request is authorized or null
   *
   * @throws IOException thrown if an IO error occurred
   * @throws AuthenticationException thrown if an authentication error occurred
   */
  @Override
  public AuthenticationToken authenticate(HttpServletRequest request,
      HttpServletResponse response)
      throws IOException, AuthenticationException {try{__CLR3_0_27v7viw50z4hr.R.inc(292);
    __CLR3_0_27v7viw50z4hr.R.inc(293);AuthenticationToken token;
    __CLR3_0_27v7viw50z4hr.R.inc(294);if ((((isBrowser(request.getHeader("User-Agent")))&&(__CLR3_0_27v7viw50z4hr.R.iget(295)!=0|true))||(__CLR3_0_27v7viw50z4hr.R.iget(296)==0&false))) {{
      __CLR3_0_27v7viw50z4hr.R.inc(297);token = alternateAuthenticate(request, response);
    }
    }else {{
      __CLR3_0_27v7viw50z4hr.R.inc(298);token = super.authenticate(request, response);
    }
    }__CLR3_0_27v7viw50z4hr.R.inc(299);return token;
  }finally{__CLR3_0_27v7viw50z4hr.R.flushNeeded();}}

  /**
   * This method parses the User-Agent String and returns whether or not it
   * refers to a browser.  If its not a browser, then Kerberos authentication
   * will be used; if it is a browser, alternateAuthenticate from the subclass
   * will be used.
   * <p/>
   * A User-Agent String is considered to be a browser if it does not contain
   * any of the values from alt-kerberos.non-browser.user-agents; the default
   * behavior is to consider everything a browser unless it contains one of:
   * "java", "curl", "wget", or "perl".  Subclasses can optionally override
   * this method to use different behavior.
   *
   * @param userAgent The User-Agent String, or null if there isn't one
   * @return true if the User-Agent String refers to a browser, false if not
   */
  protected boolean isBrowser(String userAgent) {try{__CLR3_0_27v7viw50z4hr.R.inc(300);
    __CLR3_0_27v7viw50z4hr.R.inc(301);if ((((userAgent == null)&&(__CLR3_0_27v7viw50z4hr.R.iget(302)!=0|true))||(__CLR3_0_27v7viw50z4hr.R.iget(303)==0&false))) {{
      __CLR3_0_27v7viw50z4hr.R.inc(304);return false;
    }
    }__CLR3_0_27v7viw50z4hr.R.inc(305);userAgent = userAgent.toLowerCase();
    __CLR3_0_27v7viw50z4hr.R.inc(306);boolean isBrowser = true;
    __CLR3_0_27v7viw50z4hr.R.inc(307);for (String nonBrowserUserAgent : nonBrowserUserAgents) {{
        __CLR3_0_27v7viw50z4hr.R.inc(308);if ((((userAgent.contains(nonBrowserUserAgent))&&(__CLR3_0_27v7viw50z4hr.R.iget(309)!=0|true))||(__CLR3_0_27v7viw50z4hr.R.iget(310)==0&false))) {{
            __CLR3_0_27v7viw50z4hr.R.inc(311);isBrowser = false;
            __CLR3_0_27v7viw50z4hr.R.inc(312);break;
        }
    }}
    }__CLR3_0_27v7viw50z4hr.R.inc(313);return isBrowser;
  }finally{__CLR3_0_27v7viw50z4hr.R.flushNeeded();}}

  /**
   * Subclasses should implement this method to provide the custom
   * authentication to be used for browsers.
   *
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   * @return an authentication token if the request is authorized, or null
   * @throws IOException thrown if an IO error occurs
   * @throws AuthenticationException thrown if an authentication error occurs
   */
  public abstract AuthenticationToken alternateAuthenticate(
      HttpServletRequest request, HttpServletResponse response)
      throws IOException, AuthenticationException;
}
