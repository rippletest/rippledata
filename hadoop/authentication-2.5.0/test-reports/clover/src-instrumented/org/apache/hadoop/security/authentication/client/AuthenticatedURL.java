/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.apache.hadoop.security.authentication.server.AuthenticationFilter;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * The {@link AuthenticatedURL} class enables the use of the JDK {@link URL} class
 * against HTTP endpoints protected with the {@link AuthenticationFilter}.
 * <p/>
 * The authentication mechanisms supported by default are Hadoop Simple  authentication
 * (also known as pseudo authentication) and Kerberos SPNEGO authentication.
 * <p/>
 * Additional authentication mechanisms can be supported via {@link Authenticator} implementations.
 * <p/>
 * The default {@link Authenticator} is the {@link KerberosAuthenticator} class which supports
 * automatic fallback from Kerberos SPNEGO to Hadoop Simple authentication.
 * <p/>
 * <code>AuthenticatedURL</code> instances are not thread-safe.
 * <p/>
 * The usage pattern of the {@link AuthenticatedURL} is:
 * <p/>
 * <pre>
 *
 * // establishing an initial connection
 *
 * URL url = new URL("http://foo:8080/bar");
 * AuthenticatedURL.Token token = new AuthenticatedURL.Token();
 * AuthenticatedURL aUrl = new AuthenticatedURL();
 * HttpURLConnection conn = new AuthenticatedURL(url, token).openConnection();
 * ....
 * // use the 'conn' instance
 * ....
 *
 * // establishing a follow up connection using a token from the previous connection
 *
 * HttpURLConnection conn = new AuthenticatedURL(url, token).openConnection();
 * ....
 * // use the 'conn' instance
 * ....
 *
 * </pre>
 */
public class AuthenticatedURL {public static class __CLR3_0_200iw50z4fy{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,97);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /**
   * Name of the HTTP cookie used for the authentication token between the client and the server.
   */
  public static final String AUTH_COOKIE = "hadoop.auth";

  private static final String AUTH_COOKIE_EQ = AUTH_COOKIE + "=";

  /**
   * Client side authentication token.
   */
  public static class Token {

    private String token;

    /**
     * Creates a token.
     */
    public Token() {try{__CLR3_0_200iw50z4fy.R.inc(0);
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Creates a token using an existing string representation of the token.
     *
     * @param tokenStr string representation of the tokenStr.
     */
    public Token(String tokenStr) {try{__CLR3_0_200iw50z4fy.R.inc(1);
      __CLR3_0_200iw50z4fy.R.inc(2);if ((((tokenStr == null)&&(__CLR3_0_200iw50z4fy.R.iget(3)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(4)==0&false))) {{
        __CLR3_0_200iw50z4fy.R.inc(5);throw new IllegalArgumentException("tokenStr cannot be null");
      }
      }__CLR3_0_200iw50z4fy.R.inc(6);set(tokenStr);
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Returns if a token from the server has been set.
     *
     * @return if a token from the server has been set.
     */
    public boolean isSet() {try{__CLR3_0_200iw50z4fy.R.inc(7);
      __CLR3_0_200iw50z4fy.R.inc(8);return token != null;
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Sets a token.
     *
     * @param tokenStr string representation of the tokenStr.
     */
    void set(String tokenStr) {try{__CLR3_0_200iw50z4fy.R.inc(9);
      __CLR3_0_200iw50z4fy.R.inc(10);token = tokenStr;
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Returns the string representation of the token.
     *
     * @return the string representation of the token.
     */
    @Override
    public String toString() {try{__CLR3_0_200iw50z4fy.R.inc(11);
      __CLR3_0_200iw50z4fy.R.inc(12);return token;
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Return the hashcode for the token.
     *
     * @return the hashcode for the token.
     */
    @Override
    public int hashCode() {try{__CLR3_0_200iw50z4fy.R.inc(13);
      __CLR3_0_200iw50z4fy.R.inc(14);return ((((token != null) )&&(__CLR3_0_200iw50z4fy.R.iget(15)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(16)==0&false))? token.hashCode() : 0;
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

    /**
     * Return if two token instances are equal.
     *
     * @param o the other token instance.
     *
     * @return if this instance and the other instance are equal.
     */
    @Override
    public boolean equals(Object o) {try{__CLR3_0_200iw50z4fy.R.inc(17);
      __CLR3_0_200iw50z4fy.R.inc(18);boolean eq = false;
      __CLR3_0_200iw50z4fy.R.inc(19);if ((((o instanceof Token)&&(__CLR3_0_200iw50z4fy.R.iget(20)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(21)==0&false))) {{
        __CLR3_0_200iw50z4fy.R.inc(22);Token other = (Token) o;
        __CLR3_0_200iw50z4fy.R.inc(23);eq = (token == null && other.token == null) || (token != null && this.token.equals(other.token));
      }
      }__CLR3_0_200iw50z4fy.R.inc(24);return eq;
    }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}
  }

  private static Class<? extends Authenticator> DEFAULT_AUTHENTICATOR = KerberosAuthenticator.class;

  /**
   * Sets the default {@link Authenticator} class to use when an {@link AuthenticatedURL} instance
   * is created without specifying an authenticator.
   *
   * @param authenticator the authenticator class to use as default.
   */
  public static void setDefaultAuthenticator(Class<? extends Authenticator> authenticator) {try{__CLR3_0_200iw50z4fy.R.inc(25);
    __CLR3_0_200iw50z4fy.R.inc(26);DEFAULT_AUTHENTICATOR = authenticator;
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Returns the default {@link Authenticator} class to use when an {@link AuthenticatedURL} instance
   * is created without specifying an authenticator.
   *
   * @return the authenticator class to use as default.
   */
  public static Class<? extends Authenticator> getDefaultAuthenticator() {try{__CLR3_0_200iw50z4fy.R.inc(27);
    __CLR3_0_200iw50z4fy.R.inc(28);return DEFAULT_AUTHENTICATOR;
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  private Authenticator authenticator;
  private ConnectionConfigurator connConfigurator;

  /**
   * Creates an {@link AuthenticatedURL}.
   */
  public AuthenticatedURL() {
    this(null);__CLR3_0_200iw50z4fy.R.inc(30);try{__CLR3_0_200iw50z4fy.R.inc(29);
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Creates an <code>AuthenticatedURL</code>.
   *
   * @param authenticator the {@link Authenticator} instance to use, if <code>null</code> a {@link
   * KerberosAuthenticator} is used.
   */
  public AuthenticatedURL(Authenticator authenticator) {
    this(authenticator, null);__CLR3_0_200iw50z4fy.R.inc(32);try{__CLR3_0_200iw50z4fy.R.inc(31);
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Creates an <code>AuthenticatedURL</code>.
   *
   * @param authenticator the {@link Authenticator} instance to use, if <code>null</code> a {@link
   * KerberosAuthenticator} is used.
   * @param connConfigurator a connection configurator.
   */
  public AuthenticatedURL(Authenticator authenticator,
                          ConnectionConfigurator connConfigurator) {try{__CLR3_0_200iw50z4fy.R.inc(33);
    __CLR3_0_200iw50z4fy.R.inc(34);try {
      __CLR3_0_200iw50z4fy.R.inc(35);this.authenticator = ((((authenticator != null) )&&(__CLR3_0_200iw50z4fy.R.iget(36)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(37)==0&false))? authenticator : DEFAULT_AUTHENTICATOR.newInstance();
    } catch (Exception ex) {
      __CLR3_0_200iw50z4fy.R.inc(38);throw new RuntimeException(ex);
    }
    __CLR3_0_200iw50z4fy.R.inc(39);this.connConfigurator = connConfigurator;
    __CLR3_0_200iw50z4fy.R.inc(40);this.authenticator.setConnectionConfigurator(connConfigurator);
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Returns an authenticated {@link HttpURLConnection}.
   *
   * @param url the URL to connect to. Only HTTP/S URLs are supported.
   * @param token the authentication token being used for the user.
   *
   * @return an authenticated {@link HttpURLConnection}.
   *
   * @throws IOException if an IO error occurred.
   * @throws AuthenticationException if an authentication exception occurred.
   */
  public HttpURLConnection openConnection(URL url, Token token) throws IOException, AuthenticationException {try{__CLR3_0_200iw50z4fy.R.inc(41);
    __CLR3_0_200iw50z4fy.R.inc(42);if ((((url == null)&&(__CLR3_0_200iw50z4fy.R.iget(43)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(44)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(45);throw new IllegalArgumentException("url cannot be NULL");
    }
    }__CLR3_0_200iw50z4fy.R.inc(46);if ((((!url.getProtocol().equalsIgnoreCase("http") && !url.getProtocol().equalsIgnoreCase("https"))&&(__CLR3_0_200iw50z4fy.R.iget(47)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(48)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(49);throw new IllegalArgumentException("url must be for a HTTP or HTTPS resource");
    }
    }__CLR3_0_200iw50z4fy.R.inc(50);if ((((token == null)&&(__CLR3_0_200iw50z4fy.R.iget(51)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(52)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(53);throw new IllegalArgumentException("token cannot be NULL");
    }
    }__CLR3_0_200iw50z4fy.R.inc(54);authenticator.authenticate(url, token);
    __CLR3_0_200iw50z4fy.R.inc(55);HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    __CLR3_0_200iw50z4fy.R.inc(56);if ((((connConfigurator != null)&&(__CLR3_0_200iw50z4fy.R.iget(57)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(58)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(59);conn = connConfigurator.configure(conn);
    }
    }__CLR3_0_200iw50z4fy.R.inc(60);injectToken(conn, token);
    __CLR3_0_200iw50z4fy.R.inc(61);return conn;
  }finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Helper method that injects an authentication token to send with a connection.
   *
   * @param conn connection to inject the authentication token into.
   * @param token authentication token to inject.
   */
  public static void injectToken(HttpURLConnection conn, Token token) {try{__CLR3_0_200iw50z4fy.R.inc(62);
    __CLR3_0_200iw50z4fy.R.inc(63);String t = token.token;
    __CLR3_0_200iw50z4fy.R.inc(64);if ((((t != null)&&(__CLR3_0_200iw50z4fy.R.iget(65)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(66)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(67);if ((((!t.startsWith("\""))&&(__CLR3_0_200iw50z4fy.R.iget(68)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(69)==0&false))) {{
        __CLR3_0_200iw50z4fy.R.inc(70);t = "\"" + t + "\"";
      }
      }__CLR3_0_200iw50z4fy.R.inc(71);conn.addRequestProperty("Cookie", AUTH_COOKIE_EQ + t);
    }
  }}finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

  /**
   * Helper method that extracts an authentication token received from a connection.
   * <p/>
   * This method is used by {@link Authenticator} implementations.
   *
   * @param conn connection to extract the authentication token from.
   * @param token the authentication token.
   *
   * @throws IOException if an IO error occurred.
   * @throws AuthenticationException if an authentication exception occurred.
   */
  public static void extractToken(HttpURLConnection conn, Token token) throws IOException, AuthenticationException {try{__CLR3_0_200iw50z4fy.R.inc(72);
    __CLR3_0_200iw50z4fy.R.inc(73);if ((((conn.getResponseCode() == HttpURLConnection.HTTP_OK)&&(__CLR3_0_200iw50z4fy.R.iget(74)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(75)==0&false))) {{
      __CLR3_0_200iw50z4fy.R.inc(76);Map<String, List<String>> headers = conn.getHeaderFields();
      __CLR3_0_200iw50z4fy.R.inc(77);List<String> cookies = headers.get("Set-Cookie");
      __CLR3_0_200iw50z4fy.R.inc(78);if ((((cookies != null)&&(__CLR3_0_200iw50z4fy.R.iget(79)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(80)==0&false))) {{
        __CLR3_0_200iw50z4fy.R.inc(81);for (String cookie : cookies) {{
          __CLR3_0_200iw50z4fy.R.inc(82);if ((((cookie.startsWith(AUTH_COOKIE_EQ))&&(__CLR3_0_200iw50z4fy.R.iget(83)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(84)==0&false))) {{
            __CLR3_0_200iw50z4fy.R.inc(85);String value = cookie.substring(AUTH_COOKIE_EQ.length());
            __CLR3_0_200iw50z4fy.R.inc(86);int separator = value.indexOf(";");
            __CLR3_0_200iw50z4fy.R.inc(87);if ((((separator > -1)&&(__CLR3_0_200iw50z4fy.R.iget(88)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(89)==0&false))) {{
              __CLR3_0_200iw50z4fy.R.inc(90);value = value.substring(0, separator);
            }
            }__CLR3_0_200iw50z4fy.R.inc(91);if ((((value.length() > 0)&&(__CLR3_0_200iw50z4fy.R.iget(92)!=0|true))||(__CLR3_0_200iw50z4fy.R.iget(93)==0&false))) {{
              __CLR3_0_200iw50z4fy.R.inc(94);token.set(value);
            }
          }}
        }}
      }}
    }} }else {{
      __CLR3_0_200iw50z4fy.R.inc(95);token.set(null);
      __CLR3_0_200iw50z4fy.R.inc(96);throw new AuthenticationException("Authentication failed, status: " + conn.getResponseCode() +
                                        ", message: " + conn.getResponseMessage());
    }
  }}finally{__CLR3_0_200iw50z4fy.R.flushNeeded();}}

}
