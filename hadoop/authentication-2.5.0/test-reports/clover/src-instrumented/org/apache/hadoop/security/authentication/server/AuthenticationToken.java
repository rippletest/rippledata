/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.security.authentication.client.AuthenticationException;

import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

/**
 * The {@link AuthenticationToken} contains information about an authenticated
 * HTTP client and doubles as the {@link Principal} to be returned by
 * authenticated {@link HttpServletRequest}s
 * <p/>
 * The token can be serialized/deserialized to and from a string as it is sent
 * and received in HTTP client responses and requests as a HTTP cookie (this is
 * done by the {@link AuthenticationFilter}).
 */
public class AuthenticationToken implements Principal {public static class __CLR3_0_2edediw50z4jm{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,588);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /**
   * Constant that identifies an anonymous request.
   */
  public static final AuthenticationToken ANONYMOUS = new AuthenticationToken();

  private static final String ATTR_SEPARATOR = "&";
  private static final String USER_NAME = "u";
  private static final String PRINCIPAL = "p";
  private static final String EXPIRES = "e";
  private static final String TYPE = "t";

  private final static Set<String> ATTRIBUTES =
    new HashSet<String>(Arrays.asList(USER_NAME, PRINCIPAL, EXPIRES, TYPE));

  private String userName;
  private String principal;
  private String type;
  private long expires;
  private String token;

  private AuthenticationToken() {try{__CLR3_0_2edediw50z4jm.R.inc(517);
    __CLR3_0_2edediw50z4jm.R.inc(518);userName = null;
    __CLR3_0_2edediw50z4jm.R.inc(519);principal = null;
    __CLR3_0_2edediw50z4jm.R.inc(520);type = null;
    __CLR3_0_2edediw50z4jm.R.inc(521);expires = -1;
    __CLR3_0_2edediw50z4jm.R.inc(522);token = "ANONYMOUS";
    __CLR3_0_2edediw50z4jm.R.inc(523);generateToken();
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  private static final String ILLEGAL_ARG_MSG = " is NULL, empty or contains a '" + ATTR_SEPARATOR + "'";

  /**
   * Creates an authentication token.
   *
   * @param userName user name.
   * @param principal principal (commonly matches the user name, with Kerberos is the full/long principal
   * name while the userName is the short name).
   * @param type the authentication mechanism name.
   * (<code>System.currentTimeMillis() + validityPeriod</code>).
   */
  public AuthenticationToken(String userName, String principal, String type) {try{__CLR3_0_2edediw50z4jm.R.inc(524);
    __CLR3_0_2edediw50z4jm.R.inc(525);checkForIllegalArgument(userName, "userName");
    __CLR3_0_2edediw50z4jm.R.inc(526);checkForIllegalArgument(principal, "principal");
    __CLR3_0_2edediw50z4jm.R.inc(527);checkForIllegalArgument(type, "type");
    __CLR3_0_2edediw50z4jm.R.inc(528);this.userName = userName;
    __CLR3_0_2edediw50z4jm.R.inc(529);this.principal = principal;
    __CLR3_0_2edediw50z4jm.R.inc(530);this.type = type;
    __CLR3_0_2edediw50z4jm.R.inc(531);this.expires = -1;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}
  
  /**
   * Check if the provided value is invalid. Throw an error if it is invalid, NOP otherwise.
   * 
   * @param value the value to check.
   * @param name the parameter name to use in an error message if the value is invalid.
   */
  private static void checkForIllegalArgument(String value, String name) {try{__CLR3_0_2edediw50z4jm.R.inc(532);
    __CLR3_0_2edediw50z4jm.R.inc(533);if ((((value == null || value.length() == 0 || value.contains(ATTR_SEPARATOR))&&(__CLR3_0_2edediw50z4jm.R.iget(534)!=0|true))||(__CLR3_0_2edediw50z4jm.R.iget(535)==0&false))) {{
      __CLR3_0_2edediw50z4jm.R.inc(536);throw new IllegalArgumentException(name + ILLEGAL_ARG_MSG);
    }
  }}finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Sets the expiration of the token.
   *
   * @param expires expiration time of the token in milliseconds since the epoch.
   */
  public void setExpires(long expires) {try{__CLR3_0_2edediw50z4jm.R.inc(537);
    __CLR3_0_2edediw50z4jm.R.inc(538);if ((((this != AuthenticationToken.ANONYMOUS)&&(__CLR3_0_2edediw50z4jm.R.iget(539)!=0|true))||(__CLR3_0_2edediw50z4jm.R.iget(540)==0&false))) {{
      __CLR3_0_2edediw50z4jm.R.inc(541);this.expires = expires;
      __CLR3_0_2edediw50z4jm.R.inc(542);generateToken();
    }
  }}finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Generates the token.
   */
  private void generateToken() {try{__CLR3_0_2edediw50z4jm.R.inc(543);
    __CLR3_0_2edediw50z4jm.R.inc(544);StringBuffer sb = new StringBuffer();
    __CLR3_0_2edediw50z4jm.R.inc(545);sb.append(USER_NAME).append("=").append(getUserName()).append(ATTR_SEPARATOR);
    __CLR3_0_2edediw50z4jm.R.inc(546);sb.append(PRINCIPAL).append("=").append(getName()).append(ATTR_SEPARATOR);
    __CLR3_0_2edediw50z4jm.R.inc(547);sb.append(TYPE).append("=").append(getType()).append(ATTR_SEPARATOR);
    __CLR3_0_2edediw50z4jm.R.inc(548);sb.append(EXPIRES).append("=").append(getExpires());
    __CLR3_0_2edediw50z4jm.R.inc(549);token = sb.toString();
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns the user name.
   *
   * @return the user name.
   */
  public String getUserName() {try{__CLR3_0_2edediw50z4jm.R.inc(550);
    __CLR3_0_2edediw50z4jm.R.inc(551);return userName;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns the principal name (this method name comes from the JDK {@link Principal} interface).
   *
   * @return the principal name.
   */
  @Override
  public String getName() {try{__CLR3_0_2edediw50z4jm.R.inc(552);
    __CLR3_0_2edediw50z4jm.R.inc(553);return principal;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns the authentication mechanism of the token.
   *
   * @return the authentication mechanism of the token.
   */
  public String getType() {try{__CLR3_0_2edediw50z4jm.R.inc(554);
    __CLR3_0_2edediw50z4jm.R.inc(555);return type;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns the expiration time of the token.
   *
   * @return the expiration time of the token, in milliseconds since Epoc.
   */
  public long getExpires() {try{__CLR3_0_2edediw50z4jm.R.inc(556);
    __CLR3_0_2edediw50z4jm.R.inc(557);return expires;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns if the token has expired.
   *
   * @return if the token has expired.
   */
  public boolean isExpired() {try{__CLR3_0_2edediw50z4jm.R.inc(558);
    __CLR3_0_2edediw50z4jm.R.inc(559);return getExpires() != -1 && System.currentTimeMillis() > getExpires();
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Returns the string representation of the token.
   * <p/>
   * This string representation is parseable by the {@link #parse} method.
   *
   * @return the string representation of the token.
   */
  @Override
  public String toString() {try{__CLR3_0_2edediw50z4jm.R.inc(560);
    __CLR3_0_2edediw50z4jm.R.inc(561);return token;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Parses a string into an authentication token.
   *
   * @param tokenStr string representation of a token.
   *
   * @return the parsed authentication token.
   *
   * @throws AuthenticationException thrown if the string representation could not be parsed into
   * an authentication token.
   */
  public static AuthenticationToken parse(String tokenStr) throws AuthenticationException {try{__CLR3_0_2edediw50z4jm.R.inc(562);
    __CLR3_0_2edediw50z4jm.R.inc(563);Map<String, String> map = split(tokenStr);
    __CLR3_0_2edediw50z4jm.R.inc(564);if ((((!map.keySet().equals(ATTRIBUTES))&&(__CLR3_0_2edediw50z4jm.R.iget(565)!=0|true))||(__CLR3_0_2edediw50z4jm.R.iget(566)==0&false))) {{
      __CLR3_0_2edediw50z4jm.R.inc(567);throw new AuthenticationException("Invalid token string, missing attributes");
    }
    }__CLR3_0_2edediw50z4jm.R.inc(568);long expires = Long.parseLong(map.get(EXPIRES));
    __CLR3_0_2edediw50z4jm.R.inc(569);AuthenticationToken token = new AuthenticationToken(map.get(USER_NAME), map.get(PRINCIPAL), map.get(TYPE));
    __CLR3_0_2edediw50z4jm.R.inc(570);token.setExpires(expires);
    __CLR3_0_2edediw50z4jm.R.inc(571);return token;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

  /**
   * Splits the string representation of a token into attributes pairs.
   *
   * @param tokenStr string representation of a token.
   *
   * @return a map with the attribute pairs of the token.
   *
   * @throws AuthenticationException thrown if the string representation of the token could not be broken into
   * attribute pairs.
   */
  private static Map<String, String> split(String tokenStr) throws AuthenticationException {try{__CLR3_0_2edediw50z4jm.R.inc(572);
    __CLR3_0_2edediw50z4jm.R.inc(573);Map<String, String> map = new HashMap<String, String>();
    __CLR3_0_2edediw50z4jm.R.inc(574);StringTokenizer st = new StringTokenizer(tokenStr, ATTR_SEPARATOR);
    __CLR3_0_2edediw50z4jm.R.inc(575);while ((((st.hasMoreTokens())&&(__CLR3_0_2edediw50z4jm.R.iget(576)!=0|true))||(__CLR3_0_2edediw50z4jm.R.iget(577)==0&false))) {{
      __CLR3_0_2edediw50z4jm.R.inc(578);String part = st.nextToken();
      __CLR3_0_2edediw50z4jm.R.inc(579);int separator = part.indexOf('=');
      __CLR3_0_2edediw50z4jm.R.inc(580);if ((((separator == -1)&&(__CLR3_0_2edediw50z4jm.R.iget(581)!=0|true))||(__CLR3_0_2edediw50z4jm.R.iget(582)==0&false))) {{
        __CLR3_0_2edediw50z4jm.R.inc(583);throw new AuthenticationException("Invalid authentication token");
      }
      }__CLR3_0_2edediw50z4jm.R.inc(584);String key = part.substring(0, separator);
      __CLR3_0_2edediw50z4jm.R.inc(585);String value = part.substring(separator + 1);
      __CLR3_0_2edediw50z4jm.R.inc(586);map.put(key, value);
    }
    }__CLR3_0_2edediw50z4jm.R.inc(587);return map;
  }finally{__CLR3_0_2edediw50z4jm.R.flushNeeded();}}

}
