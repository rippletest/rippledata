/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.hadoop.security.authentication.util;

import static org.apache.hadoop.util.PlatformName.IBM_JAVA;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.directory.server.kerberos.shared.keytab.Keytab;
import org.apache.directory.server.kerberos.shared.keytab.KeytabEntry;
import org.ietf.jgss.GSSException;
import org.ietf.jgss.Oid;

public class KerberosUtil {public static class __CLR3_0_2rrrriw50z4n9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,1059);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /* Return the Kerberos login module name */
  public static String getKrb5LoginModuleName() {try{__CLR3_0_2rrrriw50z4n9.R.inc(999);
    __CLR3_0_2rrrriw50z4n9.R.inc(1000);return (((System.getProperty("java.vendor").contains("IBM")
      )&&(__CLR3_0_2rrrriw50z4n9.R.iget(1001)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1002)==0&false))? "com.ibm.security.auth.module.Krb5LoginModule"
      : "com.sun.security.auth.module.Krb5LoginModule";
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}
  
  public static Oid getOidInstance(String oidName) 
      throws ClassNotFoundException, GSSException, NoSuchFieldException,
      IllegalAccessException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1003);
    __CLR3_0_2rrrriw50z4n9.R.inc(1004);Class<?> oidClass;
    __CLR3_0_2rrrriw50z4n9.R.inc(1005);if ((((IBM_JAVA)&&(__CLR3_0_2rrrriw50z4n9.R.iget(1006)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1007)==0&false))) {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1008);if (((("NT_GSS_KRB5_PRINCIPAL".equals(oidName))&&(__CLR3_0_2rrrriw50z4n9.R.iget(1009)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1010)==0&false))) {{
        // IBM JDK GSSUtil class does not have field for krb5 principal oid
        __CLR3_0_2rrrriw50z4n9.R.inc(1011);return new Oid("1.2.840.113554.1.2.2.1");
      }
      }__CLR3_0_2rrrriw50z4n9.R.inc(1012);oidClass = Class.forName("com.ibm.security.jgss.GSSUtil");
    } }else {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1013);oidClass = Class.forName("sun.security.jgss.GSSUtil");
    }
    }__CLR3_0_2rrrriw50z4n9.R.inc(1014);Field oidField = oidClass.getDeclaredField(oidName);
    __CLR3_0_2rrrriw50z4n9.R.inc(1015);return (Oid)oidField.get(oidClass);
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}

  public static String getDefaultRealm() 
      throws ClassNotFoundException, NoSuchMethodException, 
      IllegalArgumentException, IllegalAccessException, 
      InvocationTargetException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1016);
    __CLR3_0_2rrrriw50z4n9.R.inc(1017);Object kerbConf;
    __CLR3_0_2rrrriw50z4n9.R.inc(1018);Class<?> classRef;
    __CLR3_0_2rrrriw50z4n9.R.inc(1019);Method getInstanceMethod;
    __CLR3_0_2rrrriw50z4n9.R.inc(1020);Method getDefaultRealmMethod;
    __CLR3_0_2rrrriw50z4n9.R.inc(1021);if ((((System.getProperty("java.vendor").contains("IBM"))&&(__CLR3_0_2rrrriw50z4n9.R.iget(1022)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1023)==0&false))) {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1024);classRef = Class.forName("com.ibm.security.krb5.internal.Config");
    } }else {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1025);classRef = Class.forName("sun.security.krb5.Config");
    }
    }__CLR3_0_2rrrriw50z4n9.R.inc(1026);getInstanceMethod = classRef.getMethod("getInstance", new Class[0]);
    __CLR3_0_2rrrriw50z4n9.R.inc(1027);kerbConf = getInstanceMethod.invoke(classRef, new Object[0]);
    __CLR3_0_2rrrriw50z4n9.R.inc(1028);getDefaultRealmMethod = classRef.getDeclaredMethod("getDefaultRealm",
         new Class[0]);
    __CLR3_0_2rrrriw50z4n9.R.inc(1029);return (String)getDefaultRealmMethod.invoke(kerbConf, new Object[0]);
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}
  
  /* Return fqdn of the current host */
  static String getLocalHostName() throws UnknownHostException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1030);
    __CLR3_0_2rrrriw50z4n9.R.inc(1031);return InetAddress.getLocalHost().getCanonicalHostName();
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}
  
  /**
   * Create Kerberos principal for a given service and hostname. It converts
   * hostname to lower case. If hostname is null or "0.0.0.0", it uses
   * dynamically looked-up fqdn of the current host instead.
   * 
   * @param service
   *          Service for which you want to generate the principal.
   * @param hostname
   *          Fully-qualified domain name.
   * @return Converted Kerberos principal name.
   * @throws UnknownHostException
   *           If no IP address for the local host could be found.
   */
  public static final String getServicePrincipal(String service, String hostname)
      throws UnknownHostException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1032);
    __CLR3_0_2rrrriw50z4n9.R.inc(1033);String fqdn = hostname;
    __CLR3_0_2rrrriw50z4n9.R.inc(1034);if ((((null == fqdn || fqdn.equals("") || fqdn.equals("0.0.0.0"))&&(__CLR3_0_2rrrriw50z4n9.R.iget(1035)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1036)==0&false))) {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1037);fqdn = getLocalHostName();
    }
    // convert hostname to lowercase as kerberos does not work with hostnames
    // with uppercase characters.
    }__CLR3_0_2rrrriw50z4n9.R.inc(1038);return service + "/" + fqdn.toLowerCase(Locale.US);
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}

  /**
   * Get all the unique principals present in the keytabfile.
   * 
   * @param keytabFileName 
   *          Name of the keytab file to be read.
   * @return list of unique principals in the keytab.
   * @throws IOException 
   *          If keytab entries cannot be read from the file.
   */
  static final String[] getPrincipalNames(String keytabFileName) throws IOException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1039);
      __CLR3_0_2rrrriw50z4n9.R.inc(1040);Keytab keytab = Keytab.read(new File(keytabFileName));
      __CLR3_0_2rrrriw50z4n9.R.inc(1041);Set<String> principals = new HashSet<String>();
      __CLR3_0_2rrrriw50z4n9.R.inc(1042);List<KeytabEntry> entries = keytab.getEntries();
      __CLR3_0_2rrrriw50z4n9.R.inc(1043);for (KeytabEntry entry: entries){{
        __CLR3_0_2rrrriw50z4n9.R.inc(1044);principals.add(entry.getPrincipalName().replace("\\", "/"));
      }
      }__CLR3_0_2rrrriw50z4n9.R.inc(1045);return principals.toArray(new String[0]);
    }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}

  /**
   * Get all the unique principals from keytabfile which matches a pattern.
   * 
   * @param keytab 
   *          Name of the keytab file to be read.
   * @param pattern 
   *         pattern to be matched.
   * @return list of unique principals which matches the pattern.
   * @throws IOException 
   */
  public static final String[] getPrincipalNames(String keytab,
      Pattern pattern) throws IOException {try{__CLR3_0_2rrrriw50z4n9.R.inc(1046);
    __CLR3_0_2rrrriw50z4n9.R.inc(1047);String[] principals = getPrincipalNames(keytab);
    __CLR3_0_2rrrriw50z4n9.R.inc(1048);if ((((principals.length != 0)&&(__CLR3_0_2rrrriw50z4n9.R.iget(1049)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1050)==0&false))) {{
      __CLR3_0_2rrrriw50z4n9.R.inc(1051);List<String> matchingPrincipals = new ArrayList<String>();
      __CLR3_0_2rrrriw50z4n9.R.inc(1052);for (String principal : principals) {{
        __CLR3_0_2rrrriw50z4n9.R.inc(1053);if ((((pattern.matcher(principal).matches())&&(__CLR3_0_2rrrriw50z4n9.R.iget(1054)!=0|true))||(__CLR3_0_2rrrriw50z4n9.R.iget(1055)==0&false))) {{
          __CLR3_0_2rrrriw50z4n9.R.inc(1056);matchingPrincipals.add(principal);
        }
      }}
      }__CLR3_0_2rrrriw50z4n9.R.inc(1057);principals = matchingPrincipals.toArray(new String[0]);
    }
    }__CLR3_0_2rrrriw50z4n9.R.inc(1058);return principals;
  }finally{__CLR3_0_2rrrriw50z4n9.R.flushNeeded();}}
}
