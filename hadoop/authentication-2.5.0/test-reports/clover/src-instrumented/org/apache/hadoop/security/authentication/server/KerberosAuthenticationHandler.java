/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.client.KerberosAuthenticator;
import org.apache.commons.codec.binary.Base64;
import org.apache.hadoop.security.authentication.util.KerberosName;
import org.apache.hadoop.security.authentication.util.KerberosUtil;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSCredential;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.Oid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.Subject;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import static org.apache.hadoop.util.PlatformName.IBM_JAVA;

/**
 * The {@link KerberosAuthenticationHandler} implements the Kerberos SPNEGO authentication mechanism for HTTP.
 * <p/>
 * The supported configuration properties are:
 * <ul>
 * <li>kerberos.principal: the Kerberos principal to used by the server. As stated by the Kerberos SPNEGO
 * specification, it should be <code>HTTP/${HOSTNAME}@{REALM}</code>. The realm can be omitted from the
 * principal as the JDK GSS libraries will use the realm name of the configured default realm.
 * It does not have a default value.</li>
 * <li>kerberos.keytab: the keytab file containing the credentials for the Kerberos principal.
 * It does not have a default value.</li>
 * <li>kerberos.name.rules: kerberos names rules to resolve principal names, see 
 * {@link KerberosName#setRules(String)}</li>
 * </ul>
 */
public class KerberosAuthenticationHandler implements AuthenticationHandler {public static class __CLR3_0_2gcgciw50z4kl{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,747);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  private static Logger LOG = LoggerFactory.getLogger(KerberosAuthenticationHandler.class);

  /**
   * Kerberos context configuration for the JDK GSS library.
   */
  private static class KerberosConfiguration extends Configuration {
    private String keytab;
    private String principal;

    public KerberosConfiguration(String keytab, String principal) {try{__CLR3_0_2gcgciw50z4kl.R.inc(588);
      __CLR3_0_2gcgciw50z4kl.R.inc(589);this.keytab = keytab;
      __CLR3_0_2gcgciw50z4kl.R.inc(590);this.principal = principal;
    }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {try{__CLR3_0_2gcgciw50z4kl.R.inc(591);
      __CLR3_0_2gcgciw50z4kl.R.inc(592);Map<String, String> options = new HashMap<String, String>();
      __CLR3_0_2gcgciw50z4kl.R.inc(593);if ((((IBM_JAVA)&&(__CLR3_0_2gcgciw50z4kl.R.iget(594)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(595)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(596);options.put("useKeytab",
            (((keytab.startsWith("file://") )&&(__CLR3_0_2gcgciw50z4kl.R.iget(597)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(598)==0&false))? keytab : "file://" + keytab);
        __CLR3_0_2gcgciw50z4kl.R.inc(599);options.put("principal", principal);
        __CLR3_0_2gcgciw50z4kl.R.inc(600);options.put("credsType", "acceptor");
      } }else {{
        __CLR3_0_2gcgciw50z4kl.R.inc(601);options.put("keyTab", keytab);
        __CLR3_0_2gcgciw50z4kl.R.inc(602);options.put("principal", principal);
        __CLR3_0_2gcgciw50z4kl.R.inc(603);options.put("useKeyTab", "true");
        __CLR3_0_2gcgciw50z4kl.R.inc(604);options.put("storeKey", "true");
        __CLR3_0_2gcgciw50z4kl.R.inc(605);options.put("doNotPrompt", "true");
        __CLR3_0_2gcgciw50z4kl.R.inc(606);options.put("useTicketCache", "true");
        __CLR3_0_2gcgciw50z4kl.R.inc(607);options.put("renewTGT", "true");
        __CLR3_0_2gcgciw50z4kl.R.inc(608);options.put("isInitiator", "false");
      }
      }__CLR3_0_2gcgciw50z4kl.R.inc(609);options.put("refreshKrb5Config", "true");
      __CLR3_0_2gcgciw50z4kl.R.inc(610);String ticketCache = System.getenv("KRB5CCNAME");
      __CLR3_0_2gcgciw50z4kl.R.inc(611);if ((((ticketCache != null)&&(__CLR3_0_2gcgciw50z4kl.R.iget(612)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(613)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(614);if ((((IBM_JAVA)&&(__CLR3_0_2gcgciw50z4kl.R.iget(615)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(616)==0&false))) {{
          __CLR3_0_2gcgciw50z4kl.R.inc(617);options.put("useDefaultCcache", "true");
          // The first value searched when "useDefaultCcache" is used.
          __CLR3_0_2gcgciw50z4kl.R.inc(618);System.setProperty("KRB5CCNAME", ticketCache);
          __CLR3_0_2gcgciw50z4kl.R.inc(619);options.put("renewTGT", "true");
          __CLR3_0_2gcgciw50z4kl.R.inc(620);options.put("credsType", "both");
        } }else {{
          __CLR3_0_2gcgciw50z4kl.R.inc(621);options.put("ticketCache", ticketCache);
        }
      }}
      }__CLR3_0_2gcgciw50z4kl.R.inc(622);if ((((LOG.isDebugEnabled())&&(__CLR3_0_2gcgciw50z4kl.R.iget(623)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(624)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(625);options.put("debug", "true");
      }

      }__CLR3_0_2gcgciw50z4kl.R.inc(626);return new AppConfigurationEntry[]{
          new AppConfigurationEntry(KerberosUtil.getKrb5LoginModuleName(),
                                  AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
                                  options),};
    }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}
  }

  /**
   * Constant that identifies the authentication mechanism.
   */
  public static final String TYPE = "kerberos";

  /**
   * Constant for the configuration property that indicates the kerberos principal.
   */
  public static final String PRINCIPAL = TYPE + ".principal";

  /**
   * Constant for the configuration property that indicates the keytab file path.
   */
  public static final String KEYTAB = TYPE + ".keytab";

  /**
   * Constant for the configuration property that indicates the Kerberos name
   * rules for the Kerberos principals.
   */
  public static final String NAME_RULES = TYPE + ".name.rules";

  private String keytab;
  private GSSManager gssManager;
  private Subject serverSubject = new Subject();
  private List<LoginContext> loginContexts = new ArrayList<LoginContext>();

  /**
   * Initializes the authentication handler instance.
   * <p/>
   * It creates a Kerberos context using the principal and keytab specified in the configuration.
   * <p/>
   * This method is invoked by the {@link AuthenticationFilter#init} method.
   *
   * @param config configuration properties to initialize the handler.
   *
   * @throws ServletException thrown if the handler could not be initialized.
   */
  @Override
  public void init(Properties config) throws ServletException {try{__CLR3_0_2gcgciw50z4kl.R.inc(627);
    __CLR3_0_2gcgciw50z4kl.R.inc(628);try {
      __CLR3_0_2gcgciw50z4kl.R.inc(629);String principal = config.getProperty(PRINCIPAL);
      __CLR3_0_2gcgciw50z4kl.R.inc(630);if ((((principal == null || principal.trim().length() == 0)&&(__CLR3_0_2gcgciw50z4kl.R.iget(631)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(632)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(633);throw new ServletException("Principal not defined in configuration");
      }
      }__CLR3_0_2gcgciw50z4kl.R.inc(634);keytab = config.getProperty(KEYTAB, keytab);
      __CLR3_0_2gcgciw50z4kl.R.inc(635);if ((((keytab == null || keytab.trim().length() == 0)&&(__CLR3_0_2gcgciw50z4kl.R.iget(636)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(637)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(638);throw new ServletException("Keytab not defined in configuration");
      }
      }__CLR3_0_2gcgciw50z4kl.R.inc(639);if ((((!new File(keytab).exists())&&(__CLR3_0_2gcgciw50z4kl.R.iget(640)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(641)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(642);throw new ServletException("Keytab does not exist: " + keytab);
      }
      
      // use all SPNEGO principals in the keytab if a principal isn't
      // specifically configured
      }__CLR3_0_2gcgciw50z4kl.R.inc(643);final String[] spnegoPrincipals;
      __CLR3_0_2gcgciw50z4kl.R.inc(644);if ((((principal.equals("*"))&&(__CLR3_0_2gcgciw50z4kl.R.iget(645)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(646)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(647);spnegoPrincipals = KerberosUtil.getPrincipalNames(
            keytab, Pattern.compile("HTTP/.*"));
        __CLR3_0_2gcgciw50z4kl.R.inc(648);if ((((spnegoPrincipals.length == 0)&&(__CLR3_0_2gcgciw50z4kl.R.iget(649)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(650)==0&false))) {{
          __CLR3_0_2gcgciw50z4kl.R.inc(651);throw new ServletException("Principals do not exist in the keytab");
        }
      }} }else {{
        __CLR3_0_2gcgciw50z4kl.R.inc(652);spnegoPrincipals = new String[]{principal};
      }

      }__CLR3_0_2gcgciw50z4kl.R.inc(653);String nameRules = config.getProperty(NAME_RULES, null);
      __CLR3_0_2gcgciw50z4kl.R.inc(654);if ((((nameRules != null)&&(__CLR3_0_2gcgciw50z4kl.R.iget(655)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(656)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(657);KerberosName.setRules(nameRules);
      }
      
      }__CLR3_0_2gcgciw50z4kl.R.inc(658);for (String spnegoPrincipal : spnegoPrincipals) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(659);LOG.info("Login using keytab {}, for principal {}",
            keytab, spnegoPrincipal);
        __CLR3_0_2gcgciw50z4kl.R.inc(660);final KerberosConfiguration kerberosConfiguration =
            new KerberosConfiguration(keytab, spnegoPrincipal);
        __CLR3_0_2gcgciw50z4kl.R.inc(661);final LoginContext loginContext =
            new LoginContext("", serverSubject, null, kerberosConfiguration);
        __CLR3_0_2gcgciw50z4kl.R.inc(662);try {
          __CLR3_0_2gcgciw50z4kl.R.inc(663);loginContext.login();
        } catch (LoginException le) {
          __CLR3_0_2gcgciw50z4kl.R.inc(664);LOG.warn("Failed to login as [{}]", spnegoPrincipal, le);
          __CLR3_0_2gcgciw50z4kl.R.inc(665);throw new AuthenticationException(le);          
        }
        __CLR3_0_2gcgciw50z4kl.R.inc(666);loginContexts.add(loginContext);
      }
      }__CLR3_0_2gcgciw50z4kl.R.inc(667);try {
        __CLR3_0_2gcgciw50z4kl.R.inc(668);gssManager = Subject.doAs(serverSubject, new PrivilegedExceptionAction<GSSManager>() {

          @Override
          public GSSManager run() throws Exception {try{__CLR3_0_2gcgciw50z4kl.R.inc(669);
            __CLR3_0_2gcgciw50z4kl.R.inc(670);return GSSManager.getInstance();
          }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}
        });
      } catch (PrivilegedActionException ex) {
        __CLR3_0_2gcgciw50z4kl.R.inc(671);throw ex.getException();
      }
    } catch (Exception ex) {
      __CLR3_0_2gcgciw50z4kl.R.inc(672);throw new ServletException(ex);
    }
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * Releases any resources initialized by the authentication handler.
   * <p/>
   * It destroys the Kerberos context.
   */
  @Override
  public void destroy() {try{__CLR3_0_2gcgciw50z4kl.R.inc(673);
    __CLR3_0_2gcgciw50z4kl.R.inc(674);keytab = null;
    __CLR3_0_2gcgciw50z4kl.R.inc(675);serverSubject = null;
    __CLR3_0_2gcgciw50z4kl.R.inc(676);for (LoginContext loginContext : loginContexts) {{
      __CLR3_0_2gcgciw50z4kl.R.inc(677);try {
        __CLR3_0_2gcgciw50z4kl.R.inc(678);loginContext.logout();
      } catch (LoginException ex) {
        __CLR3_0_2gcgciw50z4kl.R.inc(679);LOG.warn(ex.getMessage(), ex);
      }
    }
    }__CLR3_0_2gcgciw50z4kl.R.inc(680);loginContexts.clear();
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * Returns the authentication type of the authentication handler, 'kerberos'.
   * <p/>
   *
   * @return the authentication type of the authentication handler, 'kerberos'.
   */
  @Override
  public String getType() {try{__CLR3_0_2gcgciw50z4kl.R.inc(681);
    __CLR3_0_2gcgciw50z4kl.R.inc(682);return TYPE;
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * Returns the Kerberos principals used by the authentication handler.
   *
   * @return the Kerberos principals used by the authentication handler.
   */
  protected Set<KerberosPrincipal> getPrincipals() {try{__CLR3_0_2gcgciw50z4kl.R.inc(683);
    __CLR3_0_2gcgciw50z4kl.R.inc(684);return serverSubject.getPrincipals(KerberosPrincipal.class);
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * Returns the keytab used by the authentication handler.
   *
   * @return the keytab used by the authentication handler.
   */
  protected String getKeytab() {try{__CLR3_0_2gcgciw50z4kl.R.inc(685);
    __CLR3_0_2gcgciw50z4kl.R.inc(686);return keytab;
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * This is an empty implementation, it always returns <code>TRUE</code>.
   *
   *
   *
   * @param token the authentication token if any, otherwise <code>NULL</code>.
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   *
   * @return <code>TRUE</code>
   * @throws IOException it is never thrown.
   * @throws AuthenticationException it is never thrown.
   */
  @Override
  public boolean managementOperation(AuthenticationToken token,
                                     HttpServletRequest request,
                                     HttpServletResponse response)
    throws IOException, AuthenticationException {try{__CLR3_0_2gcgciw50z4kl.R.inc(687);
    __CLR3_0_2gcgciw50z4kl.R.inc(688);return true;
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

  /**
   * It enforces the the Kerberos SPNEGO authentication sequence returning an {@link AuthenticationToken} only
   * after the Kerberos SPNEGO sequence has completed successfully.
   * <p/>
   *
   * @param request the HTTP client request.
   * @param response the HTTP client response.
   *
   * @return an authentication token if the Kerberos SPNEGO sequence is complete and valid,
   *         <code>null</code> if it is in progress (in this case the handler handles the response to the client).
   *
   * @throws IOException thrown if an IO error occurred.
   * @throws AuthenticationException thrown if Kerberos SPNEGO sequence failed.
   */
  @Override
  public AuthenticationToken authenticate(HttpServletRequest request, final HttpServletResponse response)
    throws IOException, AuthenticationException {try{__CLR3_0_2gcgciw50z4kl.R.inc(689);
    __CLR3_0_2gcgciw50z4kl.R.inc(690);AuthenticationToken token = null;
    __CLR3_0_2gcgciw50z4kl.R.inc(691);String authorization = request.getHeader(KerberosAuthenticator.AUTHORIZATION);

    __CLR3_0_2gcgciw50z4kl.R.inc(692);if ((((authorization == null || !authorization.startsWith(KerberosAuthenticator.NEGOTIATE))&&(__CLR3_0_2gcgciw50z4kl.R.iget(693)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(694)==0&false))) {{
      __CLR3_0_2gcgciw50z4kl.R.inc(695);response.setHeader(KerberosAuthenticator.WWW_AUTHENTICATE, KerberosAuthenticator.NEGOTIATE);
      __CLR3_0_2gcgciw50z4kl.R.inc(696);response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
      __CLR3_0_2gcgciw50z4kl.R.inc(697);if ((((authorization == null)&&(__CLR3_0_2gcgciw50z4kl.R.iget(698)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(699)==0&false))) {{
        __CLR3_0_2gcgciw50z4kl.R.inc(700);LOG.trace("SPNEGO starting");
      } }else {{
        __CLR3_0_2gcgciw50z4kl.R.inc(701);LOG.warn("'" + KerberosAuthenticator.AUTHORIZATION + "' does not start with '" +
            KerberosAuthenticator.NEGOTIATE + "' :  {}", authorization);
      }
    }} }else {{
      __CLR3_0_2gcgciw50z4kl.R.inc(702);authorization = authorization.substring(KerberosAuthenticator.NEGOTIATE.length()).trim();
      __CLR3_0_2gcgciw50z4kl.R.inc(703);final Base64 base64 = new Base64(0);
      __CLR3_0_2gcgciw50z4kl.R.inc(704);final byte[] clientToken = base64.decode(authorization);
      __CLR3_0_2gcgciw50z4kl.R.inc(705);final String serverName = request.getServerName();
      __CLR3_0_2gcgciw50z4kl.R.inc(706);try {
        __CLR3_0_2gcgciw50z4kl.R.inc(707);token = Subject.doAs(serverSubject, new PrivilegedExceptionAction<AuthenticationToken>() {

          @Override
          public AuthenticationToken run() throws Exception {try{__CLR3_0_2gcgciw50z4kl.R.inc(708);
            __CLR3_0_2gcgciw50z4kl.R.inc(709);AuthenticationToken token = null;
            __CLR3_0_2gcgciw50z4kl.R.inc(710);GSSContext gssContext = null;
            __CLR3_0_2gcgciw50z4kl.R.inc(711);GSSCredential gssCreds = null;
            __CLR3_0_2gcgciw50z4kl.R.inc(712);try {
              __CLR3_0_2gcgciw50z4kl.R.inc(713);gssCreds = gssManager.createCredential(
                  gssManager.createName(
                      KerberosUtil.getServicePrincipal("HTTP", serverName),
                      KerberosUtil.getOidInstance("NT_GSS_KRB5_PRINCIPAL")),
                  GSSCredential.INDEFINITE_LIFETIME,
                  new Oid[]{
                    KerberosUtil.getOidInstance("GSS_SPNEGO_MECH_OID"),
                    KerberosUtil.getOidInstance("GSS_KRB5_MECH_OID")},
                  GSSCredential.ACCEPT_ONLY);
              __CLR3_0_2gcgciw50z4kl.R.inc(714);gssContext = gssManager.createContext(gssCreds);
              __CLR3_0_2gcgciw50z4kl.R.inc(715);byte[] serverToken = gssContext.acceptSecContext(clientToken, 0, clientToken.length);
              __CLR3_0_2gcgciw50z4kl.R.inc(716);if ((((serverToken != null && serverToken.length > 0)&&(__CLR3_0_2gcgciw50z4kl.R.iget(717)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(718)==0&false))) {{
                __CLR3_0_2gcgciw50z4kl.R.inc(719);String authenticate = base64.encodeToString(serverToken);
                __CLR3_0_2gcgciw50z4kl.R.inc(720);response.setHeader(KerberosAuthenticator.WWW_AUTHENTICATE,
                                   KerberosAuthenticator.NEGOTIATE + " " + authenticate);
              }
              }__CLR3_0_2gcgciw50z4kl.R.inc(721);if ((((!gssContext.isEstablished())&&(__CLR3_0_2gcgciw50z4kl.R.iget(722)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(723)==0&false))) {{
                __CLR3_0_2gcgciw50z4kl.R.inc(724);response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                __CLR3_0_2gcgciw50z4kl.R.inc(725);LOG.trace("SPNEGO in progress");
              } }else {{
                __CLR3_0_2gcgciw50z4kl.R.inc(726);String clientPrincipal = gssContext.getSrcName().toString();
                __CLR3_0_2gcgciw50z4kl.R.inc(727);KerberosName kerberosName = new KerberosName(clientPrincipal);
                __CLR3_0_2gcgciw50z4kl.R.inc(728);String userName = kerberosName.getShortName();
                __CLR3_0_2gcgciw50z4kl.R.inc(729);token = new AuthenticationToken(userName, clientPrincipal, getType());
                __CLR3_0_2gcgciw50z4kl.R.inc(730);response.setStatus(HttpServletResponse.SC_OK);
                __CLR3_0_2gcgciw50z4kl.R.inc(731);LOG.trace("SPNEGO completed for principal [{}]", clientPrincipal);
              }
            }} finally {
              __CLR3_0_2gcgciw50z4kl.R.inc(732);if ((((gssContext != null)&&(__CLR3_0_2gcgciw50z4kl.R.iget(733)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(734)==0&false))) {{
                __CLR3_0_2gcgciw50z4kl.R.inc(735);gssContext.dispose();
              }
              }__CLR3_0_2gcgciw50z4kl.R.inc(736);if ((((gssCreds != null)&&(__CLR3_0_2gcgciw50z4kl.R.iget(737)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(738)==0&false))) {{
                __CLR3_0_2gcgciw50z4kl.R.inc(739);gssCreds.dispose();
              }
            }}
            __CLR3_0_2gcgciw50z4kl.R.inc(740);return token;
          }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}
        });
      } catch (PrivilegedActionException ex) {
        __CLR3_0_2gcgciw50z4kl.R.inc(741);if ((((ex.getException() instanceof IOException)&&(__CLR3_0_2gcgciw50z4kl.R.iget(742)!=0|true))||(__CLR3_0_2gcgciw50z4kl.R.iget(743)==0&false))) {{
          __CLR3_0_2gcgciw50z4kl.R.inc(744);throw (IOException) ex.getException();
        }
        }else {{
          __CLR3_0_2gcgciw50z4kl.R.inc(745);throw new AuthenticationException(ex.getException());
        }
      }}
    }
    }__CLR3_0_2gcgciw50z4kl.R.inc(746);return token;
  }finally{__CLR3_0_2gcgciw50z4kl.R.flushNeeded();}}

}
