/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The {@link PseudoAuthenticator} implementation provides an authentication equivalent to Hadoop's
 * Simple authentication, it trusts the value of the 'user.name' Java System property.
 * <p/>
 * The 'user.name' value is propagated using an additional query string parameter {@link #USER_NAME} ('user.name').
 */
public class PseudoAuthenticator implements Authenticator {public static class __CLR3_0_27c7ciw50z4hf{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,283);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  /**
   * Name of the additional parameter that carries the 'user.name' value.
   */
  public static final String USER_NAME = "user.name";

  private static final String USER_NAME_EQ = USER_NAME + "=";

  private ConnectionConfigurator connConfigurator;

  /**
   * Sets a {@link ConnectionConfigurator} instance to use for
   * configuring connections.
   *
   * @param configurator the {@link ConnectionConfigurator} instance.
   */
  @Override
  public void setConnectionConfigurator(ConnectionConfigurator configurator) {try{__CLR3_0_27c7ciw50z4hf.R.inc(264);
    __CLR3_0_27c7ciw50z4hf.R.inc(265);connConfigurator = configurator;
  }finally{__CLR3_0_27c7ciw50z4hf.R.flushNeeded();}}

  /**
   * Performs simple authentication against the specified URL.
   * <p/>
   * If a token is given it does a NOP and returns the given token.
   * <p/>
   * If no token is given, it will perform an HTTP <code>OPTIONS</code> request injecting an additional
   * parameter {@link #USER_NAME} in the query string with the value returned by the {@link #getUserName()}
   * method.
   * <p/>
   * If the response is successful it will update the authentication token.
   *
   * @param url the URl to authenticate against.
   * @param token the authencation token being used for the user.
   *
   * @throws IOException if an IO error occurred.
   * @throws AuthenticationException if an authentication error occurred.
   */
  @Override
  public void authenticate(URL url, AuthenticatedURL.Token token) throws IOException, AuthenticationException {try{__CLR3_0_27c7ciw50z4hf.R.inc(266);
    __CLR3_0_27c7ciw50z4hf.R.inc(267);String strUrl = url.toString();
    __CLR3_0_27c7ciw50z4hf.R.inc(268);String paramSeparator = ((((strUrl.contains("?")) )&&(__CLR3_0_27c7ciw50z4hf.R.iget(269)!=0|true))||(__CLR3_0_27c7ciw50z4hf.R.iget(270)==0&false))? "&" : "?";
    __CLR3_0_27c7ciw50z4hf.R.inc(271);strUrl += paramSeparator + USER_NAME_EQ + getUserName();
    __CLR3_0_27c7ciw50z4hf.R.inc(272);url = new URL(strUrl);
    __CLR3_0_27c7ciw50z4hf.R.inc(273);HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    __CLR3_0_27c7ciw50z4hf.R.inc(274);if ((((connConfigurator != null)&&(__CLR3_0_27c7ciw50z4hf.R.iget(275)!=0|true))||(__CLR3_0_27c7ciw50z4hf.R.iget(276)==0&false))) {{
      __CLR3_0_27c7ciw50z4hf.R.inc(277);conn = connConfigurator.configure(conn);
    }
    }__CLR3_0_27c7ciw50z4hf.R.inc(278);conn.setRequestMethod("OPTIONS");
    __CLR3_0_27c7ciw50z4hf.R.inc(279);conn.connect();
    __CLR3_0_27c7ciw50z4hf.R.inc(280);AuthenticatedURL.extractToken(conn, token);
  }finally{__CLR3_0_27c7ciw50z4hf.R.flushNeeded();}}

  /**
   * Returns the current user name.
   * <p/>
   * This implementation returns the value of the Java system property 'user.name'
   *
   * @return the current user name.
   */
  protected String getUserName() {try{__CLR3_0_27c7ciw50z4hf.R.inc(281);
    __CLR3_0_27c7ciw50z4hf.R.inc(282);return System.getProperty("user.name");
  }finally{__CLR3_0_27c7ciw50z4hf.R.flushNeeded();}}
}
