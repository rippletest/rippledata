/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.classification.InterfaceAudience;
import org.apache.hadoop.classification.InterfaceStability;
import org.apache.hadoop.security.authentication.client.AuthenticatedURL;
import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.util.Signer;
import org.apache.hadoop.security.authentication.util.SignerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The {@link AuthenticationFilter} enables protecting web application resources with different (pluggable)
 * authentication mechanisms.
 * <p/>
 * Out of the box it provides 2 authentication mechanisms: Pseudo and Kerberos SPNEGO.
 * <p/>
 * Additional authentication mechanisms are supported via the {@link AuthenticationHandler} interface.
 * <p/>
 * This filter delegates to the configured authentication handler for authentication and once it obtains an
 * {@link AuthenticationToken} from it, sets a signed HTTP cookie with the token. For client requests
 * that provide the signed HTTP cookie, it verifies the validity of the cookie, extracts the user information
 * and lets the request proceed to the target resource.
 * <p/>
 * The supported configuration properties are:
 * <ul>
 * <li>config.prefix: indicates the prefix to be used by all other configuration properties, the default value
 * is no prefix. See below for details on how/why this prefix is used.</li>
 * <li>[#PREFIX#.]type: simple|kerberos|#CLASS#, 'simple' is short for the
 * {@link PseudoAuthenticationHandler}, 'kerberos' is short for {@link KerberosAuthenticationHandler}, otherwise
 * the full class name of the {@link AuthenticationHandler} must be specified.</li>
 * <li>[#PREFIX#.]signature.secret: the secret used to sign the HTTP cookie value. The default value is a random
 * value. Unless multiple webapp instances need to share the secret the random value is adequate.</li>
 * <li>[#PREFIX#.]token.validity: time -in seconds- that the generated token is valid before a
 * new authentication is triggered, default value is <code>3600</code> seconds.</li>
 * <li>[#PREFIX#.]cookie.domain: domain to use for the HTTP cookie that stores the authentication token.</li>
 * <li>[#PREFIX#.]cookie.path: path to use for the HTTP cookie that stores the authentication token.</li>
 * </ul>
 * <p/>
 * The rest of the configuration properties are specific to the {@link AuthenticationHandler} implementation and the
 * {@link AuthenticationFilter} will take all the properties that start with the prefix #PREFIX#, it will remove
 * the prefix from it and it will pass them to the the authentication handler for initialization. Properties that do
 * not start with the prefix will not be passed to the authentication handler initialization.
 */

@InterfaceAudience.Private
@InterfaceStability.Unstable
public class AuthenticationFilter implements Filter {public static class __CLR3_0_28q8qiw50z4ip{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958055L,8589935092L,517);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  private static Logger LOG = LoggerFactory.getLogger(AuthenticationFilter.class);

  /**
   * Constant for the property that specifies the configuration prefix.
   */
  public static final String CONFIG_PREFIX = "config.prefix";

  /**
   * Constant for the property that specifies the authentication handler to use.
   */
  public static final String AUTH_TYPE = "type";

  /**
   * Constant for the property that specifies the secret to use for signing the HTTP Cookies.
   */
  public static final String SIGNATURE_SECRET = "signature.secret";

  /**
   * Constant for the configuration property that indicates the validity of the generated token.
   */
  public static final String AUTH_TOKEN_VALIDITY = "token.validity";

  /**
   * Constant for the configuration property that indicates the domain to use in the HTTP cookie.
   */
  public static final String COOKIE_DOMAIN = "cookie.domain";

  /**
   * Constant for the configuration property that indicates the path to use in the HTTP cookie.
   */
  public static final String COOKIE_PATH = "cookie.path";

  private static final Random RAN = new Random();

  private Signer signer;
  private AuthenticationHandler authHandler;
  private boolean randomSecret;
  private long validity;
  private String cookieDomain;
  private String cookiePath;

  /**
   * Initializes the authentication filter.
   * <p/>
   * It instantiates and initializes the specified {@link AuthenticationHandler}.
   * <p/>
   *
   * @param filterConfig filter configuration.
   *
   * @throws ServletException thrown if the filter or the authentication handler could not be initialized properly.
   */
  @Override
  public void init(FilterConfig filterConfig) throws ServletException {try{__CLR3_0_28q8qiw50z4ip.R.inc(314);
    __CLR3_0_28q8qiw50z4ip.R.inc(315);String configPrefix = filterConfig.getInitParameter(CONFIG_PREFIX);
    __CLR3_0_28q8qiw50z4ip.R.inc(316);configPrefix = ((((configPrefix != null) )&&(__CLR3_0_28q8qiw50z4ip.R.iget(317)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(318)==0&false))? configPrefix + "." : "";
    __CLR3_0_28q8qiw50z4ip.R.inc(319);Properties config = getConfiguration(configPrefix, filterConfig);
    __CLR3_0_28q8qiw50z4ip.R.inc(320);String authHandlerName = config.getProperty(AUTH_TYPE, null);
    __CLR3_0_28q8qiw50z4ip.R.inc(321);String authHandlerClassName;
    __CLR3_0_28q8qiw50z4ip.R.inc(322);if ((((authHandlerName == null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(323)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(324)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(325);throw new ServletException("Authentication type must be specified: " +
          PseudoAuthenticationHandler.TYPE + "|" + 
          KerberosAuthenticationHandler.TYPE + "|<class>");
    }
    }__CLR3_0_28q8qiw50z4ip.R.inc(326);if ((((authHandlerName.toLowerCase(Locale.ENGLISH).equals(
        PseudoAuthenticationHandler.TYPE))&&(__CLR3_0_28q8qiw50z4ip.R.iget(327)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(328)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(329);authHandlerClassName = PseudoAuthenticationHandler.class.getName();
    } }else {__CLR3_0_28q8qiw50z4ip.R.inc(330);if ((((authHandlerName.toLowerCase(Locale.ENGLISH).equals(
        KerberosAuthenticationHandler.TYPE))&&(__CLR3_0_28q8qiw50z4ip.R.iget(331)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(332)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(333);authHandlerClassName = KerberosAuthenticationHandler.class.getName();
    } }else {{
      __CLR3_0_28q8qiw50z4ip.R.inc(334);authHandlerClassName = authHandlerName;
    }

    }}__CLR3_0_28q8qiw50z4ip.R.inc(335);try {
      __CLR3_0_28q8qiw50z4ip.R.inc(336);Class<?> klass = Thread.currentThread().getContextClassLoader().loadClass(authHandlerClassName);
      __CLR3_0_28q8qiw50z4ip.R.inc(337);authHandler = (AuthenticationHandler) klass.newInstance();
      __CLR3_0_28q8qiw50z4ip.R.inc(338);authHandler.init(config);
    } catch (ClassNotFoundException ex) {
      __CLR3_0_28q8qiw50z4ip.R.inc(339);throw new ServletException(ex);
    } catch (InstantiationException ex) {
      __CLR3_0_28q8qiw50z4ip.R.inc(340);throw new ServletException(ex);
    } catch (IllegalAccessException ex) {
      __CLR3_0_28q8qiw50z4ip.R.inc(341);throw new ServletException(ex);
    }
    __CLR3_0_28q8qiw50z4ip.R.inc(342);String signatureSecret = config.getProperty(configPrefix + SIGNATURE_SECRET);
    __CLR3_0_28q8qiw50z4ip.R.inc(343);if ((((signatureSecret == null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(344)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(345)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(346);signatureSecret = Long.toString(RAN.nextLong());
      __CLR3_0_28q8qiw50z4ip.R.inc(347);randomSecret = true;
      __CLR3_0_28q8qiw50z4ip.R.inc(348);LOG.warn("'signature.secret' configuration not set, using a random value as secret");
    }
    }__CLR3_0_28q8qiw50z4ip.R.inc(349);signer = new Signer(signatureSecret.getBytes());
    __CLR3_0_28q8qiw50z4ip.R.inc(350);validity = Long.parseLong(config.getProperty(AUTH_TOKEN_VALIDITY, "36000")) * 1000; //10 hours

    __CLR3_0_28q8qiw50z4ip.R.inc(351);cookieDomain = config.getProperty(COOKIE_DOMAIN, null);
    __CLR3_0_28q8qiw50z4ip.R.inc(352);cookiePath = config.getProperty(COOKIE_PATH, null);
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the authentication handler being used.
   *
   * @return the authentication handler being used.
   */
  protected AuthenticationHandler getAuthenticationHandler() {try{__CLR3_0_28q8qiw50z4ip.R.inc(353);
    __CLR3_0_28q8qiw50z4ip.R.inc(354);return authHandler;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns if a random secret is being used.
   *
   * @return if a random secret is being used.
   */
  protected boolean isRandomSecret() {try{__CLR3_0_28q8qiw50z4ip.R.inc(355);
    __CLR3_0_28q8qiw50z4ip.R.inc(356);return randomSecret;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the validity time of the generated tokens.
   *
   * @return the validity time of the generated tokens, in seconds.
   */
  protected long getValidity() {try{__CLR3_0_28q8qiw50z4ip.R.inc(357);
    __CLR3_0_28q8qiw50z4ip.R.inc(358);return validity / 1000;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the cookie domain to use for the HTTP cookie.
   *
   * @return the cookie domain to use for the HTTP cookie.
   */
  protected String getCookieDomain() {try{__CLR3_0_28q8qiw50z4ip.R.inc(359);
    __CLR3_0_28q8qiw50z4ip.R.inc(360);return cookieDomain;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the cookie path to use for the HTTP cookie.
   *
   * @return the cookie path to use for the HTTP cookie.
   */
  protected String getCookiePath() {try{__CLR3_0_28q8qiw50z4ip.R.inc(361);
    __CLR3_0_28q8qiw50z4ip.R.inc(362);return cookiePath;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Destroys the filter.
   * <p/>
   * It invokes the {@link AuthenticationHandler#destroy()} method to release any resources it may hold.
   */
  @Override
  public void destroy() {try{__CLR3_0_28q8qiw50z4ip.R.inc(363);
    __CLR3_0_28q8qiw50z4ip.R.inc(364);if ((((authHandler != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(365)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(366)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(367);authHandler.destroy();
      __CLR3_0_28q8qiw50z4ip.R.inc(368);authHandler = null;
    }
  }}finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the filtered configuration (only properties starting with the specified prefix). The property keys
   * are also trimmed from the prefix. The returned {@link Properties} object is used to initialized the
   * {@link AuthenticationHandler}.
   * <p/>
   * This method can be overriden by subclasses to obtain the configuration from other configuration source than
   * the web.xml file.
   *
   * @param configPrefix configuration prefix to use for extracting configuration properties.
   * @param filterConfig filter configuration object
   *
   * @return the configuration to be used with the {@link AuthenticationHandler} instance.
   *
   * @throws ServletException thrown if the configuration could not be created.
   */
  protected Properties getConfiguration(String configPrefix, FilterConfig filterConfig) throws ServletException {try{__CLR3_0_28q8qiw50z4ip.R.inc(369);
    __CLR3_0_28q8qiw50z4ip.R.inc(370);Properties props = new Properties();
    __CLR3_0_28q8qiw50z4ip.R.inc(371);Enumeration<?> names = filterConfig.getInitParameterNames();
    __CLR3_0_28q8qiw50z4ip.R.inc(372);while ((((names.hasMoreElements())&&(__CLR3_0_28q8qiw50z4ip.R.iget(373)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(374)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(375);String name = (String) names.nextElement();
      __CLR3_0_28q8qiw50z4ip.R.inc(376);if ((((name.startsWith(configPrefix))&&(__CLR3_0_28q8qiw50z4ip.R.iget(377)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(378)==0&false))) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(379);String value = filterConfig.getInitParameter(name);
        __CLR3_0_28q8qiw50z4ip.R.inc(380);props.put(name.substring(configPrefix.length()), value);
      }
    }}
    }__CLR3_0_28q8qiw50z4ip.R.inc(381);return props;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the full URL of the request including the query string.
   * <p/>
   * Used as a convenience method for logging purposes.
   *
   * @param request the request object.
   *
   * @return the full URL of the request including the query string.
   */
  protected String getRequestURL(HttpServletRequest request) {try{__CLR3_0_28q8qiw50z4ip.R.inc(382);
    __CLR3_0_28q8qiw50z4ip.R.inc(383);StringBuffer sb = request.getRequestURL();
    __CLR3_0_28q8qiw50z4ip.R.inc(384);if ((((request.getQueryString() != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(385)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(386)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(387);sb.append("?").append(request.getQueryString());
    }
    }__CLR3_0_28q8qiw50z4ip.R.inc(388);return sb.toString();
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Returns the {@link AuthenticationToken} for the request.
   * <p/>
   * It looks at the received HTTP cookies and extracts the value of the {@link AuthenticatedURL#AUTH_COOKIE}
   * if present. It verifies the signature and if correct it creates the {@link AuthenticationToken} and returns
   * it.
   * <p/>
   * If this method returns <code>null</code> the filter will invoke the configured {@link AuthenticationHandler}
   * to perform user authentication.
   *
   * @param request request object.
   *
   * @return the Authentication token if the request is authenticated, <code>null</code> otherwise.
   *
   * @throws IOException thrown if an IO error occurred.
   * @throws AuthenticationException thrown if the token is invalid or if it has expired.
   */
  protected AuthenticationToken getToken(HttpServletRequest request) throws IOException, AuthenticationException {try{__CLR3_0_28q8qiw50z4ip.R.inc(389);
    __CLR3_0_28q8qiw50z4ip.R.inc(390);AuthenticationToken token = null;
    __CLR3_0_28q8qiw50z4ip.R.inc(391);String tokenStr = null;
    __CLR3_0_28q8qiw50z4ip.R.inc(392);Cookie[] cookies = request.getCookies();
    __CLR3_0_28q8qiw50z4ip.R.inc(393);if ((((cookies != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(394)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(395)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(396);for (Cookie cookie : cookies) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(397);if ((((cookie.getName().equals(AuthenticatedURL.AUTH_COOKIE))&&(__CLR3_0_28q8qiw50z4ip.R.iget(398)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(399)==0&false))) {{
          __CLR3_0_28q8qiw50z4ip.R.inc(400);tokenStr = cookie.getValue();
          __CLR3_0_28q8qiw50z4ip.R.inc(401);try {
            __CLR3_0_28q8qiw50z4ip.R.inc(402);tokenStr = signer.verifyAndExtract(tokenStr);
          } catch (SignerException ex) {
            __CLR3_0_28q8qiw50z4ip.R.inc(403);throw new AuthenticationException(ex);
          }
          __CLR3_0_28q8qiw50z4ip.R.inc(404);break;
        }
      }}
    }}
    }__CLR3_0_28q8qiw50z4ip.R.inc(405);if ((((tokenStr != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(406)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(407)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(408);token = AuthenticationToken.parse(tokenStr);
      __CLR3_0_28q8qiw50z4ip.R.inc(409);if ((((!token.getType().equals(authHandler.getType()))&&(__CLR3_0_28q8qiw50z4ip.R.iget(410)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(411)==0&false))) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(412);throw new AuthenticationException("Invalid AuthenticationToken type");
      }
      }__CLR3_0_28q8qiw50z4ip.R.inc(413);if ((((token.isExpired())&&(__CLR3_0_28q8qiw50z4ip.R.iget(414)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(415)==0&false))) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(416);throw new AuthenticationException("AuthenticationToken expired");
      }
    }}
    }__CLR3_0_28q8qiw50z4ip.R.inc(417);return token;
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * If the request has a valid authentication token it allows the request to continue to the target resource,
   * otherwise it triggers an authentication sequence using the configured {@link AuthenticationHandler}.
   *
   * @param request the request object.
   * @param response the response object.
   * @param filterChain the filter chain object.
   *
   * @throws IOException thrown if an IO error occurred.
   * @throws ServletException thrown if a processing error occurred.
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
      throws IOException, ServletException {try{__CLR3_0_28q8qiw50z4ip.R.inc(418);
    __CLR3_0_28q8qiw50z4ip.R.inc(419);boolean unauthorizedResponse = true;
    __CLR3_0_28q8qiw50z4ip.R.inc(420);int errCode = HttpServletResponse.SC_UNAUTHORIZED;
    __CLR3_0_28q8qiw50z4ip.R.inc(421);AuthenticationException authenticationEx = null;
    __CLR3_0_28q8qiw50z4ip.R.inc(422);HttpServletRequest httpRequest = (HttpServletRequest) request;
    __CLR3_0_28q8qiw50z4ip.R.inc(423);HttpServletResponse httpResponse = (HttpServletResponse) response;
    __CLR3_0_28q8qiw50z4ip.R.inc(424);boolean isHttps = "https".equals(httpRequest.getScheme());
    __CLR3_0_28q8qiw50z4ip.R.inc(425);try {
      __CLR3_0_28q8qiw50z4ip.R.inc(426);boolean newToken = false;
      __CLR3_0_28q8qiw50z4ip.R.inc(427);AuthenticationToken token;
      __CLR3_0_28q8qiw50z4ip.R.inc(428);try {
        __CLR3_0_28q8qiw50z4ip.R.inc(429);token = getToken(httpRequest);
      }
      catch (AuthenticationException ex) {
        __CLR3_0_28q8qiw50z4ip.R.inc(430);LOG.warn("AuthenticationToken ignored: " + ex.getMessage());
        // will be sent back in a 401 unless filter authenticates
        __CLR3_0_28q8qiw50z4ip.R.inc(431);authenticationEx = ex;
        __CLR3_0_28q8qiw50z4ip.R.inc(432);token = null;
      }
      __CLR3_0_28q8qiw50z4ip.R.inc(433);if ((((authHandler.managementOperation(token, httpRequest, httpResponse))&&(__CLR3_0_28q8qiw50z4ip.R.iget(434)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(435)==0&false))) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(436);if ((((token == null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(437)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(438)==0&false))) {{
          __CLR3_0_28q8qiw50z4ip.R.inc(439);if ((((LOG.isDebugEnabled())&&(__CLR3_0_28q8qiw50z4ip.R.iget(440)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(441)==0&false))) {{
            __CLR3_0_28q8qiw50z4ip.R.inc(442);LOG.debug("Request [{}] triggering authentication", getRequestURL(httpRequest));
          }
          }__CLR3_0_28q8qiw50z4ip.R.inc(443);token = authHandler.authenticate(httpRequest, httpResponse);
          __CLR3_0_28q8qiw50z4ip.R.inc(444);if ((((token != null && token.getExpires() != 0 &&
              token != AuthenticationToken.ANONYMOUS)&&(__CLR3_0_28q8qiw50z4ip.R.iget(445)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(446)==0&false))) {{
            __CLR3_0_28q8qiw50z4ip.R.inc(447);token.setExpires(System.currentTimeMillis() + getValidity() * 1000);
          }
          }__CLR3_0_28q8qiw50z4ip.R.inc(448);newToken = true;
        }
        }__CLR3_0_28q8qiw50z4ip.R.inc(449);if ((((token != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(450)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(451)==0&false))) {{
          __CLR3_0_28q8qiw50z4ip.R.inc(452);unauthorizedResponse = false;
          __CLR3_0_28q8qiw50z4ip.R.inc(453);if ((((LOG.isDebugEnabled())&&(__CLR3_0_28q8qiw50z4ip.R.iget(454)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(455)==0&false))) {{
            __CLR3_0_28q8qiw50z4ip.R.inc(456);LOG.debug("Request [{}] user [{}] authenticated", getRequestURL(httpRequest), token.getUserName());
          }
          }__CLR3_0_28q8qiw50z4ip.R.inc(457);final AuthenticationToken authToken = token;
          __CLR3_0_28q8qiw50z4ip.R.inc(458);httpRequest = new HttpServletRequestWrapper(httpRequest) {

            @Override
            public String getAuthType() {try{__CLR3_0_28q8qiw50z4ip.R.inc(459);
              __CLR3_0_28q8qiw50z4ip.R.inc(460);return authToken.getType();
            }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

            @Override
            public String getRemoteUser() {try{__CLR3_0_28q8qiw50z4ip.R.inc(461);
              __CLR3_0_28q8qiw50z4ip.R.inc(462);return authToken.getUserName();
            }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

            @Override
            public Principal getUserPrincipal() {try{__CLR3_0_28q8qiw50z4ip.R.inc(463);
              __CLR3_0_28q8qiw50z4ip.R.inc(464);return ((((authToken != AuthenticationToken.ANONYMOUS) )&&(__CLR3_0_28q8qiw50z4ip.R.iget(465)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(466)==0&false))? authToken : null;
            }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}
          };
          __CLR3_0_28q8qiw50z4ip.R.inc(467);if ((((newToken && !token.isExpired() && token != AuthenticationToken.ANONYMOUS)&&(__CLR3_0_28q8qiw50z4ip.R.iget(468)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(469)==0&false))) {{
            __CLR3_0_28q8qiw50z4ip.R.inc(470);String signedToken = signer.sign(token.toString());
            __CLR3_0_28q8qiw50z4ip.R.inc(471);createAuthCookie(httpResponse, signedToken, getCookieDomain(),
                    getCookiePath(), token.getExpires(), isHttps);
          }
          }__CLR3_0_28q8qiw50z4ip.R.inc(472);filterChain.doFilter(httpRequest, httpResponse);
        }
      }} }else {{
        __CLR3_0_28q8qiw50z4ip.R.inc(473);unauthorizedResponse = false;
      }
    }} catch (AuthenticationException ex) {
      // exception from the filter itself is fatal
      __CLR3_0_28q8qiw50z4ip.R.inc(474);errCode = HttpServletResponse.SC_FORBIDDEN;
      __CLR3_0_28q8qiw50z4ip.R.inc(475);authenticationEx = ex;
      __CLR3_0_28q8qiw50z4ip.R.inc(476);LOG.warn("Authentication exception: " + ex.getMessage(), ex);
    }
    __CLR3_0_28q8qiw50z4ip.R.inc(477);if ((((unauthorizedResponse)&&(__CLR3_0_28q8qiw50z4ip.R.iget(478)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(479)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(480);if ((((!httpResponse.isCommitted())&&(__CLR3_0_28q8qiw50z4ip.R.iget(481)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(482)==0&false))) {{
        __CLR3_0_28q8qiw50z4ip.R.inc(483);createAuthCookie(httpResponse, "", getCookieDomain(),
                getCookiePath(), 0, isHttps);
        __CLR3_0_28q8qiw50z4ip.R.inc(484);if ((((authenticationEx == null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(485)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(486)==0&false))) {{
          __CLR3_0_28q8qiw50z4ip.R.inc(487);httpResponse.sendError(errCode, "Authentication required");
        } }else {{
          __CLR3_0_28q8qiw50z4ip.R.inc(488);httpResponse.sendError(errCode, authenticationEx.getMessage());
        }
      }}
    }}
  }}finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}

  /**
   * Creates the Hadoop authentication HTTP cookie.
   *
   * @param token authentication token for the cookie.
   * @param expires UNIX timestamp that indicates the expire date of the
   *                cookie. It has no effect if its value < 0.
   *
   * XXX the following code duplicate some logic in Jetty / Servlet API,
   * because of the fact that Hadoop is stuck at servlet 2.5 and jetty 6
   * right now.
   */
  public static void createAuthCookie(HttpServletResponse resp, String token,
                                      String domain, String path, long expires,
                                      boolean isSecure) {try{__CLR3_0_28q8qiw50z4ip.R.inc(489);
    __CLR3_0_28q8qiw50z4ip.R.inc(490);StringBuilder sb = new StringBuilder(AuthenticatedURL.AUTH_COOKIE)
                           .append("=");
    __CLR3_0_28q8qiw50z4ip.R.inc(491);if ((((token != null && token.length() > 0)&&(__CLR3_0_28q8qiw50z4ip.R.iget(492)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(493)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(494);sb.append("\"")
          .append(token)
          .append("\"");
    }
    }__CLR3_0_28q8qiw50z4ip.R.inc(495);sb.append("; Version=1");

    __CLR3_0_28q8qiw50z4ip.R.inc(496);if ((((path != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(497)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(498)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(499);sb.append("; Path=").append(path);
    }

    }__CLR3_0_28q8qiw50z4ip.R.inc(500);if ((((domain != null)&&(__CLR3_0_28q8qiw50z4ip.R.iget(501)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(502)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(503);sb.append("; Domain=").append(domain);
    }

    }__CLR3_0_28q8qiw50z4ip.R.inc(504);if ((((expires >= 0)&&(__CLR3_0_28q8qiw50z4ip.R.iget(505)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(506)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(507);Date date = new Date(expires);
      __CLR3_0_28q8qiw50z4ip.R.inc(508);SimpleDateFormat df = new SimpleDateFormat("EEE, " +
              "dd-MMM-yyyy HH:mm:ss zzz");
      __CLR3_0_28q8qiw50z4ip.R.inc(509);df.setTimeZone(TimeZone.getTimeZone("GMT"));
      __CLR3_0_28q8qiw50z4ip.R.inc(510);sb.append("; Expires=").append(df.format(date));
    }

    }__CLR3_0_28q8qiw50z4ip.R.inc(511);if ((((isSecure)&&(__CLR3_0_28q8qiw50z4ip.R.iget(512)!=0|true))||(__CLR3_0_28q8qiw50z4ip.R.iget(513)==0&false))) {{
      __CLR3_0_28q8qiw50z4ip.R.inc(514);sb.append("; Secure");
    }

    }__CLR3_0_28q8qiw50z4ip.R.inc(515);sb.append("; HttpOnly");
    __CLR3_0_28q8qiw50z4ip.R.inc(516);resp.addHeader("Set-Cookie", sb.toString());
  }finally{__CLR3_0_28q8qiw50z4ip.R.flushNeeded();}}
}
