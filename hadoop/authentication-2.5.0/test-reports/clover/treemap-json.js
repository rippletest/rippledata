processTreeMapJson (  {"id":"Clover database Wed Nov 30 2016 09:25:58 EST0","name":"","data":{
    "$area":1098.0,"$color":84.24408,"title":
    " 1098 Elements, 84.2% Coverage"},"children":[{"id":
      "org.apache.hadoop.security.authentication.client0","name":
      "org.apache.hadoop.security.authentication.client","data":{"$area":
        283.0,"$color":72.79152,"title":
        "org.apache.hadoop.security.authentication.client 283 Elements, 72.8% Coverage"},
      "children":[{"id":"AuthenticatedURL0","name":"AuthenticatedURL","data":
          {"$area":72.0,"$color":81.94444,"path":
            "org/apache/hadoop/security/authentication/client/AuthenticatedURL.html#AuthenticatedURL",
            "title":"AuthenticatedURL 72 Elements, 81.9% Coverage"},
          "children":[]},{"id":"AuthenticatedURL.Token0","name":
          "AuthenticatedURL.Token","data":{"$area":25.0,"$color":88.0,"path":
            
            "org/apache/hadoop/security/authentication/client/AuthenticatedURL.html#AuthenticatedURL.Token",
            "title":"AuthenticatedURL.Token 25 Elements, 88% Coverage"},
          "children":[]},{"id":"AuthenticationException97","name":
          "AuthenticationException","data":{"$area":6.0,"$color":66.66667,
            "path":
            "org/apache/hadoop/security/authentication/client/AuthenticationException.html#AuthenticationException",
            "title":"AuthenticationException 6 Elements, 66.7% Coverage"},
          "children":[]},{"id":"Authenticator103","name":"Authenticator",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/hadoop/security/authentication/client/Authenticator.html#Authenticator",
            "title":"Authenticator 0 Elements,  -  Coverage"},"children":[]},
        {"id":"ConnectionConfigurator103","name":"ConnectionConfigurator",
          "data":{"$area":0.0,"$color":-100.0,"path":
            "org/apache/hadoop/security/authentication/client/ConnectionConfigurator.html#ConnectionConfigurator",
            "title":"ConnectionConfigurator 0 Elements,  -  Coverage"},
          "children":[]},{"id":"KerberosAuthenticator103","name":
          "KerberosAuthenticator","data":{"$area":120.0,"$color":86.666664,
            "path":
            "org/apache/hadoop/security/authentication/client/KerberosAuthenticator.html#KerberosAuthenticator",
            "title":"KerberosAuthenticator 120 Elements, 86.7% Coverage"},
          "children":[]},{"id":
          "KerberosAuthenticator.KerberosConfiguration103","name":
          "KerberosAuthenticator.KerberosConfiguration","data":{"$area":
            41.0,"$color":0.0,"path":
            "org/apache/hadoop/security/authentication/client/KerberosAuthenticator.html#KerberosAuthenticator.KerberosConfiguration",
            "title":
            "KerberosAuthenticator.KerberosConfiguration 41 Elements, 0% Coverage"},
          "children":[]},{"id":"PseudoAuthenticator264","name":
          "PseudoAuthenticator","data":{"$area":19.0,"$color":89.47369,
            "path":
            "org/apache/hadoop/security/authentication/client/PseudoAuthenticator.html#PseudoAuthenticator",
            "title":"PseudoAuthenticator 19 Elements, 89.5% Coverage"},
          "children":[]}]},{"id":"org.apache.hadoop.util1096","name":
      "org.apache.hadoop.util","data":{"$area":2.0,"$color":0.0,"title":
        "org.apache.hadoop.util 2 Elements, 0% Coverage"},"children":[{"id":
          "PlatformName1096","name":"PlatformName","data":{"$area":2.0,
            "$color":0.0,"path":
            "org/apache/hadoop/util/PlatformName.html#PlatformName","title":
            "PlatformName 2 Elements, 0% Coverage"},"children":[]}]},{"id":
      "org.apache.hadoop.security.authentication.server283","name":
      "org.apache.hadoop.security.authentication.server","data":{"$area":
        497.0,"$color":87.726364,"title":
        "org.apache.hadoop.security.authentication.server 497 Elements, 87.7% Coverage"},
      "children":[{"id":"AltKerberosAuthenticationHandler283","name":
          "AltKerberosAuthenticationHandler","data":{"$area":31.0,"$color":
            90.32258,"path":
            "org/apache/hadoop/security/authentication/server/AltKerberosAuthenticationHandler.html#AltKerberosAuthenticationHandler",
            "title":
            "AltKerberosAuthenticationHandler 31 Elements, 90.3% Coverage"},
          "children":[]},{"id":"AuthenticationFilter314","name":
          "AuthenticationFilter","data":{"$area":203.0,"$color":90.14778,
            "path":
            "org/apache/hadoop/security/authentication/server/AuthenticationFilter.html#AuthenticationFilter",
            "title":"AuthenticationFilter 203 Elements, 90.1% Coverage"},
          "children":[]},{"id":"AuthenticationHandler517","name":
          "AuthenticationHandler","data":{"$area":0.0,"$color":-100.0,"path":
            
            "org/apache/hadoop/security/authentication/server/AuthenticationHandler.html#AuthenticationHandler",
            "title":"AuthenticationHandler 0 Elements,  -  Coverage"},
          "children":[]},{"id":"AuthenticationToken517","name":
          "AuthenticationToken","data":{"$area":71.0,"$color":95.77465,
            "path":
            "org/apache/hadoop/security/authentication/server/AuthenticationToken.html#AuthenticationToken",
            "title":"AuthenticationToken 71 Elements, 95.8% Coverage"},
          "children":[]},{"id":"KerberosAuthenticationHandler588","name":
          "KerberosAuthenticationHandler","data":{"$area":120.0,"$color":
            86.666664,"path":
            "org/apache/hadoop/security/authentication/server/KerberosAuthenticationHandler.html#KerberosAuthenticationHandler",
            "title":
            "KerberosAuthenticationHandler 120 Elements, 86.7% Coverage"},
          "children":[]},{"id":
          "KerberosAuthenticationHandler.KerberosConfiguration588","name":
          "KerberosAuthenticationHandler.KerberosConfiguration","data":{
            "$area":39.0,"$color":56.41026,"path":
            "org/apache/hadoop/security/authentication/server/KerberosAuthenticationHandler.html#KerberosAuthenticationHandler.KerberosConfiguration",
            "title":
            "KerberosAuthenticationHandler.KerberosConfiguration 39 Elements, 56.4% Coverage"},
          "children":[]},{"id":"PseudoAuthenticationHandler747","name":
          "PseudoAuthenticationHandler","data":{"$area":33.0,"$color":
            93.93939,"path":
            "org/apache/hadoop/security/authentication/server/PseudoAuthenticationHandler.html#PseudoAuthenticationHandler",
            "title":
            "PseudoAuthenticationHandler 33 Elements, 93.9% Coverage"},
          "children":[]}]},{"id":
      "org.apache.hadoop.security.authentication.util780","name":
      "org.apache.hadoop.security.authentication.util","data":{"$area":
        316.0,"$color":89.55696,"title":
        "org.apache.hadoop.security.authentication.util 316 Elements, 89.6% Coverage"},
      "children":[{"id":"KerberosName780","name":"KerberosName","data":{
            "$area":98.0,"$color":89.79591,"path":
            "org/apache/hadoop/security/authentication/util/KerberosName.html#KerberosName",
            "title":"KerberosName 98 Elements, 89.8% Coverage"},"children":[]},
        {"id":"KerberosName.Rule822","name":"KerberosName.Rule","data":{
            "$area":115.0,"$color":93.04348,"path":
            "org/apache/hadoop/security/authentication/util/KerberosName.html#KerberosName.Rule",
            "title":"KerberosName.Rule 115 Elements, 93% Coverage"},
          "children":[]},{"id":"KerberosName.BadFormatString955","name":
          "KerberosName.BadFormatString","data":{"$area":4.0,"$color":0.0,
            "path":
            "org/apache/hadoop/security/authentication/util/KerberosName.html#KerberosName.BadFormatString",
            "title":"KerberosName.BadFormatString 4 Elements, 0% Coverage"},
          "children":[]},{"id":"KerberosName.NoMatchingRule959","name":
          "KerberosName.NoMatchingRule","data":{"$area":2.0,"$color":100.0,
            "path":
            "org/apache/hadoop/security/authentication/util/KerberosName.html#KerberosName.NoMatchingRule",
            "title":
            "KerberosName.NoMatchingRule 2 Elements, 100% Coverage"},
          "children":[]},{"id":"KerberosUtil999","name":"KerberosUtil",
          "data":{"$area":60.0,"$color":83.33333,"path":
            "org/apache/hadoop/security/authentication/util/KerberosUtil.html#KerberosUtil",
            "title":"KerberosUtil 60 Elements, 83.3% Coverage"},"children":[]},
        {"id":"Signer1059","name":"Signer","data":{"$area":35.0,"$color":
            97.14286,"path":
            "org/apache/hadoop/security/authentication/util/Signer.html#Signer",
            "title":"Signer 35 Elements, 97.1% Coverage"},"children":[]},{
          "id":"SignerException1094","name":"SignerException","data":{
            "$area":2.0,"$color":100.0,"path":
            "org/apache/hadoop/security/authentication/util/SignerException.html#SignerException",
            "title":"SignerException 2 Elements, 100% Coverage"},"children":[]}]}]}

 ); 