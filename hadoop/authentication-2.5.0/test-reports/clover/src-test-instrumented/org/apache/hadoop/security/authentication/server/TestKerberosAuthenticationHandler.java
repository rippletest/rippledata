/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.minikdc.KerberosSecurityTestcase;
import org.apache.hadoop.security.authentication.KerberosTestUtils;
import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.client.KerberosAuthenticator;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.security.authentication.util.KerberosName;
import org.apache.hadoop.security.authentication.util.KerberosUtil;
import org.ietf.jgss.GSSContext;
import org.ietf.jgss.GSSManager;
import org.ietf.jgss.GSSName;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.ietf.jgss.Oid;

import javax.security.auth.Subject;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Callable;

public class TestKerberosAuthenticationHandler
    extends KerberosSecurityTestcase {static class __CLR3_0_21i71i7iw50z4x3{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,2100);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  protected KerberosAuthenticationHandler handler;

  protected KerberosAuthenticationHandler getNewAuthenticationHandler() {try{__CLR3_0_21i71i7iw50z4x3.R.inc(1951);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1952);return new KerberosAuthenticationHandler();
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  protected String getExpectedType() {try{__CLR3_0_21i71i7iw50z4x3.R.inc(1953);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1954);return KerberosAuthenticationHandler.TYPE;
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  protected Properties getDefaultProperties() {try{__CLR3_0_21i71i7iw50z4x3.R.inc(1955);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1956);Properties props = new Properties();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1957);props.setProperty(KerberosAuthenticationHandler.PRINCIPAL,
            KerberosTestUtils.getServerPrincipal());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1958);props.setProperty(KerberosAuthenticationHandler.KEYTAB,
            KerberosTestUtils.getKeytabFile());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1959);props.setProperty(KerberosAuthenticationHandler.NAME_RULES,
            "RULE:[1:$1@$0](.*@" + KerberosTestUtils.getRealm()+")s/@.*//\n");
    __CLR3_0_21i71i7iw50z4x3.R.inc(1960);return props;
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @Before
  public void setup() throws Exception {try{__CLR3_0_21i71i7iw50z4x3.R.inc(1961);
    // create keytab
    __CLR3_0_21i71i7iw50z4x3.R.inc(1962);File keytabFile = new File(KerberosTestUtils.getKeytabFile());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1963);String clientPrincipal = KerberosTestUtils.getClientPrincipal();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1964);String serverPrincipal = KerberosTestUtils.getServerPrincipal();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1965);clientPrincipal = clientPrincipal.substring(0, clientPrincipal.lastIndexOf("@"));
    __CLR3_0_21i71i7iw50z4x3.R.inc(1966);serverPrincipal = serverPrincipal.substring(0, serverPrincipal.lastIndexOf("@"));
    __CLR3_0_21i71i7iw50z4x3.R.inc(1967);getKdc().createPrincipal(keytabFile, clientPrincipal, serverPrincipal);
    // handler
    __CLR3_0_21i71i7iw50z4x3.R.inc(1968);handler = getNewAuthenticationHandler();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1969);Properties props = getDefaultProperties();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1970);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(1971);handler.init(props);
    } catch (Exception ex) {
      __CLR3_0_21i71i7iw50z4x3.R.inc(1972);handler = null;
      __CLR3_0_21i71i7iw50z4x3.R.inc(1973);throw ex;
    }
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testNameRules() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),1974);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_21315q71iu();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testNameRules",1974,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_21315q71iu() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(1974);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1975);KerberosName kn = new KerberosName(KerberosTestUtils.getServerPrincipal());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1976);Assert.assertEquals(KerberosTestUtils.getRealm(), kn.getRealm());

    //destroy handler created in setUp()
    __CLR3_0_21i71i7iw50z4x3.R.inc(1977);handler.destroy();

    __CLR3_0_21i71i7iw50z4x3.R.inc(1978);KerberosName.setRules("RULE:[1:$1@$0](.*@FOO)s/@.*//\nDEFAULT");
    
    __CLR3_0_21i71i7iw50z4x3.R.inc(1979);handler = getNewAuthenticationHandler();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1980);Properties props = getDefaultProperties();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1981);props.setProperty(KerberosAuthenticationHandler.NAME_RULES, "RULE:[1:$1@$0](.*@BAR)s/@.*//\nDEFAULT");
    __CLR3_0_21i71i7iw50z4x3.R.inc(1982);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(1983);handler.init(props);
    } catch (Exception ex) {
    }
    __CLR3_0_21i71i7iw50z4x3.R.inc(1984);kn = new KerberosName("bar@BAR");
    __CLR3_0_21i71i7iw50z4x3.R.inc(1985);Assert.assertEquals("bar", kn.getShortName());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1986);kn = new KerberosName("bar@FOO");
    __CLR3_0_21i71i7iw50z4x3.R.inc(1987);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(1988);kn.getShortName();
      __CLR3_0_21i71i7iw50z4x3.R.inc(1989);Assert.fail();
    }
    catch (Exception ex) {      
    }
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testInit() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),1990);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ai0cvr1ja();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testInit",1990,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ai0cvr1ja() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(1990);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1991);Assert.assertEquals(KerberosTestUtils.getKeytabFile(), handler.getKeytab());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1992);Set<KerberosPrincipal> principals = handler.getPrincipals();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1993);Principal expectedPrincipal =
        new KerberosPrincipal(KerberosTestUtils.getServerPrincipal());
    __CLR3_0_21i71i7iw50z4x3.R.inc(1994);Assert.assertTrue(principals.contains(expectedPrincipal));
    __CLR3_0_21i71i7iw50z4x3.R.inc(1995);Assert.assertEquals(1, principals.size());
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  // dynamic configuration of HTTP principals
  @Test(timeout=60000)
  public void testDynamicPrincipalDiscovery() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),1996);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2eoy3pm1jg();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testDynamicPrincipalDiscovery",1996,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2eoy3pm1jg() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(1996);
    __CLR3_0_21i71i7iw50z4x3.R.inc(1997);String[] keytabUsers = new String[]{
        "HTTP/host1", "HTTP/host2", "HTTP2/host1", "XHTTP/host"
    };
    __CLR3_0_21i71i7iw50z4x3.R.inc(1998);String keytab = KerberosTestUtils.getKeytabFile();
    __CLR3_0_21i71i7iw50z4x3.R.inc(1999);getKdc().createPrincipal(new File(keytab), keytabUsers);

    // destroy handler created in setUp()
    __CLR3_0_21i71i7iw50z4x3.R.inc(2000);handler.destroy();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2001);Properties props = new Properties();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2002);props.setProperty(KerberosAuthenticationHandler.KEYTAB, keytab);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2003);props.setProperty(KerberosAuthenticationHandler.PRINCIPAL, "*");
    __CLR3_0_21i71i7iw50z4x3.R.inc(2004);handler = getNewAuthenticationHandler();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2005);handler.init(props);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2006);Assert.assertEquals(KerberosTestUtils.getKeytabFile(), handler.getKeytab());    
    
    __CLR3_0_21i71i7iw50z4x3.R.inc(2007);Set<KerberosPrincipal> loginPrincipals = handler.getPrincipals();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2008);for (String user : keytabUsers) {{
      __CLR3_0_21i71i7iw50z4x3.R.inc(2009);Principal principal = new KerberosPrincipal(
          user + "@" + KerberosTestUtils.getRealm());
      __CLR3_0_21i71i7iw50z4x3.R.inc(2010);boolean expected = user.startsWith("HTTP/");
      __CLR3_0_21i71i7iw50z4x3.R.inc(2011);Assert.assertEquals("checking for "+user, expected, 
          loginPrincipals.contains(principal));
    }
  }}finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  // dynamic configuration of HTTP principals
  @Test(timeout=60000)
  public void testDynamicPrincipalDiscoveryMissingPrincipals() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2012);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2chfk591jw();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testDynamicPrincipalDiscoveryMissingPrincipals",2012,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2chfk591jw() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2012);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2013);String[] keytabUsers = new String[]{"hdfs/localhost"};
    __CLR3_0_21i71i7iw50z4x3.R.inc(2014);String keytab = KerberosTestUtils.getKeytabFile();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2015);getKdc().createPrincipal(new File(keytab), keytabUsers);

    // destroy handler created in setUp()
    __CLR3_0_21i71i7iw50z4x3.R.inc(2016);handler.destroy();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2017);Properties props = new Properties();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2018);props.setProperty(KerberosAuthenticationHandler.KEYTAB, keytab);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2019);props.setProperty(KerberosAuthenticationHandler.PRINCIPAL, "*");
    __CLR3_0_21i71i7iw50z4x3.R.inc(2020);handler = getNewAuthenticationHandler();
    __CLR3_0_21i71i7iw50z4x3.R.inc(2021);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2022);handler.init(props);
      __CLR3_0_21i71i7iw50z4x3.R.inc(2023);Assert.fail("init should have failed");
    } catch (ServletException ex) {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2024);Assert.assertEquals("Principals do not exist in the keytab",
          ex.getCause().getMessage());
    } catch (Throwable t) {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2025);Assert.fail("wrong exception: "+t);
    }
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testType() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2026);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2yf672l1ka();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testType",2026,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2yf672l1ka() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2026);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2027);Assert.assertEquals(getExpectedType(), handler.getType());
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  public void testRequestWithoutAuthorization() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2028);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ao2i511kc();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testRequestWithoutAuthorization",2028,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ao2i511kc() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2028);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2029);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2030);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2031);Assert.assertNull(handler.authenticate(request, response));
    __CLR3_0_21i71i7iw50z4x3.R.inc(2032);Mockito.verify(response).setHeader(KerberosAuthenticator.WWW_AUTHENTICATE, KerberosAuthenticator.NEGOTIATE);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2033);Mockito.verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  public void testRequestWithInvalidAuthorization() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2034);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2oknrws1ki();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testRequestWithInvalidAuthorization",2034,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2oknrws1ki() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2034);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2035);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2036);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2037);Mockito.when(request.getHeader(KerberosAuthenticator.AUTHORIZATION)).thenReturn("invalid");
    __CLR3_0_21i71i7iw50z4x3.R.inc(2038);Assert.assertNull(handler.authenticate(request, response));
    __CLR3_0_21i71i7iw50z4x3.R.inc(2039);Mockito.verify(response).setHeader(KerberosAuthenticator.WWW_AUTHENTICATE, KerberosAuthenticator.NEGOTIATE);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2040);Mockito.verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testRequestWithIncompleteAuthorization() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2041);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_24qcs5b1kp();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testRequestWithIncompleteAuthorization",2041,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_24qcs5b1kp() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2041);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2042);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2043);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2044);Mockito.when(request.getHeader(KerberosAuthenticator.AUTHORIZATION))
      .thenReturn(KerberosAuthenticator.NEGOTIATE);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2045);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2046);handler.authenticate(request, response);
      __CLR3_0_21i71i7iw50z4x3.R.inc(2047);Assert.fail();
    } catch (AuthenticationException ex) {
      // Expected
    } catch (Exception ex) {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2048);Assert.fail();
    }
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  public void testRequestWithAuthorization() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2049);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_24bqcvx1kx();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testRequestWithAuthorization",2049,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_24bqcvx1kx() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2049);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2050);String token = KerberosTestUtils.doAsClient(new Callable<String>() {
      @Override
      public String call() throws Exception {try{__CLR3_0_21i71i7iw50z4x3.R.inc(2051);
        __CLR3_0_21i71i7iw50z4x3.R.inc(2052);GSSManager gssManager = GSSManager.getInstance();
        __CLR3_0_21i71i7iw50z4x3.R.inc(2053);GSSContext gssContext = null;
        __CLR3_0_21i71i7iw50z4x3.R.inc(2054);try {
          __CLR3_0_21i71i7iw50z4x3.R.inc(2055);String servicePrincipal = KerberosTestUtils.getServerPrincipal();
          __CLR3_0_21i71i7iw50z4x3.R.inc(2056);Oid oid = KerberosUtil.getOidInstance("NT_GSS_KRB5_PRINCIPAL");
          __CLR3_0_21i71i7iw50z4x3.R.inc(2057);GSSName serviceName = gssManager.createName(servicePrincipal,
              oid);
          __CLR3_0_21i71i7iw50z4x3.R.inc(2058);oid = KerberosUtil.getOidInstance("GSS_KRB5_MECH_OID");
          __CLR3_0_21i71i7iw50z4x3.R.inc(2059);gssContext = gssManager.createContext(serviceName, oid, null,
                                                  GSSContext.DEFAULT_LIFETIME);
          __CLR3_0_21i71i7iw50z4x3.R.inc(2060);gssContext.requestCredDeleg(true);
          __CLR3_0_21i71i7iw50z4x3.R.inc(2061);gssContext.requestMutualAuth(true);

          __CLR3_0_21i71i7iw50z4x3.R.inc(2062);byte[] inToken = new byte[0];
          __CLR3_0_21i71i7iw50z4x3.R.inc(2063);byte[] outToken = gssContext.initSecContext(inToken, 0, inToken.length);
          __CLR3_0_21i71i7iw50z4x3.R.inc(2064);Base64 base64 = new Base64(0);
          __CLR3_0_21i71i7iw50z4x3.R.inc(2065);return base64.encodeToString(outToken);

        } finally {
          __CLR3_0_21i71i7iw50z4x3.R.inc(2066);if ((((gssContext != null)&&(__CLR3_0_21i71i7iw50z4x3.R.iget(2067)!=0|true))||(__CLR3_0_21i71i7iw50z4x3.R.iget(2068)==0&false))) {{
            __CLR3_0_21i71i7iw50z4x3.R.inc(2069);gssContext.dispose();
          }
        }}
      }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}
    });

    __CLR3_0_21i71i7iw50z4x3.R.inc(2070);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2071);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2072);Mockito.when(request.getHeader(KerberosAuthenticator.AUTHORIZATION))
      .thenReturn(KerberosAuthenticator.NEGOTIATE + " " + token);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2073);Mockito.when(request.getServerName()).thenReturn("localhost");
    
    __CLR3_0_21i71i7iw50z4x3.R.inc(2074);AuthenticationToken authToken = handler.authenticate(request, response);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2075);if ((((authToken != null)&&(__CLR3_0_21i71i7iw50z4x3.R.iget(2076)!=0|true))||(__CLR3_0_21i71i7iw50z4x3.R.iget(2077)==0&false))) {{
      __CLR3_0_21i71i7iw50z4x3.R.inc(2078);Mockito.verify(response).setHeader(Mockito.eq(KerberosAuthenticator.WWW_AUTHENTICATE),
                                         Mockito.matches(KerberosAuthenticator.NEGOTIATE + " .*"));
      __CLR3_0_21i71i7iw50z4x3.R.inc(2079);Mockito.verify(response).setStatus(HttpServletResponse.SC_OK);

      __CLR3_0_21i71i7iw50z4x3.R.inc(2080);Assert.assertEquals(KerberosTestUtils.getClientPrincipal(), authToken.getName());
      __CLR3_0_21i71i7iw50z4x3.R.inc(2081);Assert.assertTrue(KerberosTestUtils.getClientPrincipal().startsWith(authToken.getUserName()));
      __CLR3_0_21i71i7iw50z4x3.R.inc(2082);Assert.assertEquals(getExpectedType(), authToken.getType());
    } }else {{
      __CLR3_0_21i71i7iw50z4x3.R.inc(2083);Mockito.verify(response).setHeader(Mockito.eq(KerberosAuthenticator.WWW_AUTHENTICATE),
                                         Mockito.matches(KerberosAuthenticator.NEGOTIATE + " .*"));
      __CLR3_0_21i71i7iw50z4x3.R.inc(2084);Mockito.verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    }
  }}finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  public void testRequestWithInvalidKerberosAuthorization() throws Exception {__CLR3_0_21i71i7iw50z4x3.R.globalSliceStart(getClass().getName(),2085);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2jueswh1lx();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21i71i7iw50z4x3.R.rethrow($CLV_t2$);}finally{__CLR3_0_21i71i7iw50z4x3.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestKerberosAuthenticationHandler.testRequestWithInvalidKerberosAuthorization",2085,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2jueswh1lx() throws Exception{try{__CLR3_0_21i71i7iw50z4x3.R.inc(2085);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2086);String token = new Base64(0).encodeToString(new byte[]{0, 1, 2});

    __CLR3_0_21i71i7iw50z4x3.R.inc(2087);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2088);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2089);Mockito.when(request.getHeader(KerberosAuthenticator.AUTHORIZATION)).thenReturn(
      KerberosAuthenticator.NEGOTIATE + token);

    __CLR3_0_21i71i7iw50z4x3.R.inc(2090);try {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2091);handler.authenticate(request, response);
      __CLR3_0_21i71i7iw50z4x3.R.inc(2092);Assert.fail();
    } catch (AuthenticationException ex) {
      // Expected
    } catch (Exception ex) {
      __CLR3_0_21i71i7iw50z4x3.R.inc(2093);Assert.fail();
    }
  }finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}

  @After
  public void tearDown() throws Exception {try{__CLR3_0_21i71i7iw50z4x3.R.inc(2094);
    __CLR3_0_21i71i7iw50z4x3.R.inc(2095);if ((((handler != null)&&(__CLR3_0_21i71i7iw50z4x3.R.iget(2096)!=0|true))||(__CLR3_0_21i71i7iw50z4x3.R.iget(2097)==0&false))) {{
      __CLR3_0_21i71i7iw50z4x3.R.inc(2098);handler.destroy();
      __CLR3_0_21i71i7iw50z4x3.R.inc(2099);handler = null;
    }
  }}finally{__CLR3_0_21i71i7iw50z4x3.R.flushNeeded();}}
}
