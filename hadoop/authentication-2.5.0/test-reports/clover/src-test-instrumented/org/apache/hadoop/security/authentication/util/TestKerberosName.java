/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ */package org.apache.hadoop.security.authentication.util;

/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;

import org.apache.hadoop.security.authentication.KerberosTestUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.junit.Assert;

public class TestKerberosName {static class __CLR3_0_21nt1ntiw50z4xt{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,2199);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Before
  public void setUp() throws Exception {try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2153);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2154);System.setProperty("java.security.krb5.realm", KerberosTestUtils.getRealm());
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2155);System.setProperty("java.security.krb5.kdc", "localhost:88");

    __CLR3_0_21nt1ntiw50z4xt.R.inc(2156);String rules =
      "RULE:[1:$1@$0](.*@YAHOO\\.COM)s/@.*//\n" +
      "RULE:[2:$1](johndoe)s/^.*$/guest/\n" +
      "RULE:[2:$1;$2](^.*;admin$)s/;admin$//\n" +
      "RULE:[2:$2](root)\n" +
      "DEFAULT";
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2157);KerberosName.setRules(rules);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2158);KerberosName.printRules();
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  private void checkTranslation(String from, String to) throws Exception {try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2159);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2160);System.out.println("Translate " + from);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2161);KerberosName nm = new KerberosName(from);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2162);String simple = nm.getShortName();
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2163);System.out.println("to " + simple);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2164);Assert.assertEquals("short name incorrect", to, simple);
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  @Test
  public void testRules() throws Exception {__CLR3_0_21nt1ntiw50z4xt.R.globalSliceStart(getClass().getName(),2165);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2br32241o5();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21nt1ntiw50z4xt.R.rethrow($CLV_t2$);}finally{__CLR3_0_21nt1ntiw50z4xt.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosName.testRules",2165,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2br32241o5() throws Exception{try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2165);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2166);checkTranslation("omalley@" + KerberosTestUtils.getRealm(), "omalley");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2167);checkTranslation("hdfs/10.0.0.1@" + KerberosTestUtils.getRealm(), "hdfs");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2168);checkTranslation("oom@YAHOO.COM", "oom");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2169);checkTranslation("johndoe/zoo@FOO.COM", "guest");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2170);checkTranslation("joe/admin@FOO.COM", "joe");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2171);checkTranslation("joe/root@FOO.COM", "root");
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  private void checkBadName(String name) {try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2172);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2173);System.out.println("Checking " + name + " to ensure it is bad.");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2174);try {
      __CLR3_0_21nt1ntiw50z4xt.R.inc(2175);new KerberosName(name);
      __CLR3_0_21nt1ntiw50z4xt.R.inc(2176);Assert.fail("didn't get exception for " + name);
    } catch (IllegalArgumentException iae) {
      // PASS
    }
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  private void checkBadTranslation(String from) {try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2177);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2178);System.out.println("Checking bad translation for " + from);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2179);KerberosName nm = new KerberosName(from);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2180);try {
      __CLR3_0_21nt1ntiw50z4xt.R.inc(2181);nm.getShortName();
      __CLR3_0_21nt1ntiw50z4xt.R.inc(2182);Assert.fail("didn't get exception for " + from);
    } catch (IOException ie) {
      // PASS
    }
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  @Test
  public void testAntiPatterns() throws Exception {__CLR3_0_21nt1ntiw50z4xt.R.globalSliceStart(getClass().getName(),2183);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_23zp30e1on();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21nt1ntiw50z4xt.R.rethrow($CLV_t2$);}finally{__CLR3_0_21nt1ntiw50z4xt.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosName.testAntiPatterns",2183,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_23zp30e1on() throws Exception{try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2183);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2184);checkBadName("owen/owen/owen@FOO.COM");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2185);checkBadName("owen@foo/bar.com");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2186);checkBadTranslation("foo@ACME.COM");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2187);checkBadTranslation("root/joe@FOO.COM");
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  @Test
  public void testToLowerCase() throws Exception {__CLR3_0_21nt1ntiw50z4xt.R.globalSliceStart(getClass().getName(),2188);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ta5itx1os();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21nt1ntiw50z4xt.R.rethrow($CLV_t2$);}finally{__CLR3_0_21nt1ntiw50z4xt.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosName.testToLowerCase",2188,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ta5itx1os() throws Exception{try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2188);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2189);String rules =
        "RULE:[1:$1]/L\n" +
        "RULE:[2:$1]/L\n" +
        "RULE:[2:$1;$2](^.*;admin$)s/;admin$///L\n" +
        "RULE:[2:$1;$2](^.*;guest$)s/;guest$//g/L\n" +
        "DEFAULT";
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2190);KerberosName.setRules(rules);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2191);KerberosName.printRules();
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2192);checkTranslation("Joe@FOO.COM", "joe");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2193);checkTranslation("Joe/root@FOO.COM", "joe");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2194);checkTranslation("Joe/admin@FOO.COM", "joe");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2195);checkTranslation("Joe/guestguest@FOO.COM", "joe");
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}

  @After
  public void clear() {try{__CLR3_0_21nt1ntiw50z4xt.R.inc(2196);
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2197);System.clearProperty("java.security.krb5.realm");
    __CLR3_0_21nt1ntiw50z4xt.R.inc(2198);System.clearProperty("java.security.krb5.kdc");
  }finally{__CLR3_0_21nt1ntiw50z4xt.R.flushNeeded();}}
}
