/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.client.PseudoAuthenticator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;

public class TestPseudoAuthenticationHandler {static class __CLR3_0_21mc1mciw50z4xc{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,2153);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Test
  public void testInit() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2100);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ai0cvr1mc();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testInit",2100,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ai0cvr1mc() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2100);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2101);PseudoAuthenticationHandler handler = new PseudoAuthenticationHandler();
    __CLR3_0_21mc1mciw50z4xc.R.inc(2102);try {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2103);Properties props = new Properties();
      __CLR3_0_21mc1mciw50z4xc.R.inc(2104);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, "false");
      __CLR3_0_21mc1mciw50z4xc.R.inc(2105);handler.init(props);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2106);Assert.assertEquals(false, handler.getAcceptAnonymous());
    } finally {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2107);handler.destroy();
    }
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  @Test
  public void testType() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2108);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2yf672l1mk();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testType",2108,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2yf672l1mk() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2108);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2109);PseudoAuthenticationHandler handler = new PseudoAuthenticationHandler();
    __CLR3_0_21mc1mciw50z4xc.R.inc(2110);Assert.assertEquals(PseudoAuthenticationHandler.TYPE, handler.getType());
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  @Test
  public void testAnonymousOn() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2111);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2n0o9xd1mn();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testAnonymousOn",2111,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2n0o9xd1mn() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2111);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2112);PseudoAuthenticationHandler handler = new PseudoAuthenticationHandler();
    __CLR3_0_21mc1mciw50z4xc.R.inc(2113);try {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2114);Properties props = new Properties();
      __CLR3_0_21mc1mciw50z4xc.R.inc(2115);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, "true");
      __CLR3_0_21mc1mciw50z4xc.R.inc(2116);handler.init(props);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2117);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2118);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2119);AuthenticationToken token = handler.authenticate(request, response);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2120);Assert.assertEquals(AuthenticationToken.ANONYMOUS, token);
    } finally {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2121);handler.destroy();
    }
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  @Test
  public void testAnonymousOff() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2122);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2mooqmd1my();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testAnonymousOff",2122,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2mooqmd1my() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2122);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2123);PseudoAuthenticationHandler handler = new PseudoAuthenticationHandler();
    __CLR3_0_21mc1mciw50z4xc.R.inc(2124);try {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2125);Properties props = new Properties();
      __CLR3_0_21mc1mciw50z4xc.R.inc(2126);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, "false");
      __CLR3_0_21mc1mciw50z4xc.R.inc(2127);handler.init(props);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2128);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2129);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2130);handler.authenticate(request, response);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2131);Assert.fail();
    } catch (AuthenticationException ex) {
      // Expected
    } catch (Exception ex) {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2132);Assert.fail();
    } finally {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2133);handler.destroy();
    }
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  private void _testUserName(boolean anonymous) throws Exception {try{__CLR3_0_21mc1mciw50z4xc.R.inc(2134);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2135);PseudoAuthenticationHandler handler = new PseudoAuthenticationHandler();
    __CLR3_0_21mc1mciw50z4xc.R.inc(2136);try {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2137);Properties props = new Properties();
      __CLR3_0_21mc1mciw50z4xc.R.inc(2138);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, Boolean.toString(anonymous));
      __CLR3_0_21mc1mciw50z4xc.R.inc(2139);handler.init(props);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2140);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2141);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2142);Mockito.when(request.getQueryString()).thenReturn(PseudoAuthenticator.USER_NAME + "=" + "user");

      __CLR3_0_21mc1mciw50z4xc.R.inc(2143);AuthenticationToken token = handler.authenticate(request, response);

      __CLR3_0_21mc1mciw50z4xc.R.inc(2144);Assert.assertNotNull(token);
      __CLR3_0_21mc1mciw50z4xc.R.inc(2145);Assert.assertEquals("user", token.getUserName());
      __CLR3_0_21mc1mciw50z4xc.R.inc(2146);Assert.assertEquals("user", token.getName());
      __CLR3_0_21mc1mciw50z4xc.R.inc(2147);Assert.assertEquals(PseudoAuthenticationHandler.TYPE, token.getType());
    } finally {
      __CLR3_0_21mc1mciw50z4xc.R.inc(2148);handler.destroy();
    }
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  @Test
  public void testUserNameAnonymousOff() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2149);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_25g7b811np();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testUserNameAnonymousOff",2149,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_25g7b811np() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2149);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2150);_testUserName(false);
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

  @Test
  public void testUserNameAnonymousOn() throws Exception {__CLR3_0_21mc1mciw50z4xc.R.globalSliceStart(getClass().getName(),2151);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2zdsaaz1nr();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21mc1mciw50z4xc.R.rethrow($CLV_t2$);}finally{__CLR3_0_21mc1mciw50z4xc.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestPseudoAuthenticationHandler.testUserNameAnonymousOn",2151,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2zdsaaz1nr() throws Exception{try{__CLR3_0_21mc1mciw50z4xc.R.inc(2151);
    __CLR3_0_21mc1mciw50z4xc.R.inc(2152);_testUserName(true);
  }finally{__CLR3_0_21mc1mciw50z4xc.R.flushNeeded();}}

}
