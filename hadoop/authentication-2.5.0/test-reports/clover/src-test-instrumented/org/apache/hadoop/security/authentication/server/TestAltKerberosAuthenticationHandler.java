/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import java.io.IOException;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

public class TestAltKerberosAuthenticationHandler
    extends TestKerberosAuthenticationHandler {static class __CLR3_0_212u12uiw50z4t9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1449);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Override
  protected KerberosAuthenticationHandler getNewAuthenticationHandler() {try{__CLR3_0_212u12uiw50z4t9.R.inc(1398);
    // AltKerberosAuthenticationHandler is abstract; a subclass would normally
    // perform some other authentication when alternateAuthenticate() is called.
    // For the test, we'll just return an AuthenticationToken as the other
    // authentication is left up to the developer of the subclass
    __CLR3_0_212u12uiw50z4t9.R.inc(1399);return new AltKerberosAuthenticationHandler() {
      @Override
      public AuthenticationToken alternateAuthenticate(
              HttpServletRequest request,
              HttpServletResponse response)
              throws IOException, AuthenticationException {try{__CLR3_0_212u12uiw50z4t9.R.inc(1400);
        __CLR3_0_212u12uiw50z4t9.R.inc(1401);return new AuthenticationToken("A", "B", getType());
      }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}
    };
  }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}

  @Override
  protected String getExpectedType() {try{__CLR3_0_212u12uiw50z4t9.R.inc(1402);
    __CLR3_0_212u12uiw50z4t9.R.inc(1403);return AltKerberosAuthenticationHandler.TYPE;
  }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testAlternateAuthenticationAsBrowser() throws Exception {__CLR3_0_212u12uiw50z4t9.R.globalSliceStart(getClass().getName(),1404);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2tcjskd130();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_212u12uiw50z4t9.R.rethrow($CLV_t2$);}finally{__CLR3_0_212u12uiw50z4t9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAltKerberosAuthenticationHandler.testAlternateAuthenticationAsBrowser",1404,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2tcjskd130() throws Exception{try{__CLR3_0_212u12uiw50z4t9.R.inc(1404);
    __CLR3_0_212u12uiw50z4t9.R.inc(1405);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_212u12uiw50z4t9.R.inc(1406);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    // By default, a User-Agent without "java", "curl", "wget", or "perl" in it
    // is considered a browser
    __CLR3_0_212u12uiw50z4t9.R.inc(1407);Mockito.when(request.getHeader("User-Agent")).thenReturn("Some Browser");

    __CLR3_0_212u12uiw50z4t9.R.inc(1408);AuthenticationToken token = handler.authenticate(request, response);
    __CLR3_0_212u12uiw50z4t9.R.inc(1409);Assert.assertEquals("A", token.getUserName());
    __CLR3_0_212u12uiw50z4t9.R.inc(1410);Assert.assertEquals("B", token.getName());
    __CLR3_0_212u12uiw50z4t9.R.inc(1411);Assert.assertEquals(getExpectedType(), token.getType());
  }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testNonDefaultNonBrowserUserAgentAsBrowser() throws Exception {__CLR3_0_212u12uiw50z4t9.R.globalSliceStart(getClass().getName(),1412);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2w84g2s138();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_212u12uiw50z4t9.R.rethrow($CLV_t2$);}finally{__CLR3_0_212u12uiw50z4t9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAltKerberosAuthenticationHandler.testNonDefaultNonBrowserUserAgentAsBrowser",1412,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2w84g2s138() throws Exception{try{__CLR3_0_212u12uiw50z4t9.R.inc(1412);
    __CLR3_0_212u12uiw50z4t9.R.inc(1413);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_212u12uiw50z4t9.R.inc(1414);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

    __CLR3_0_212u12uiw50z4t9.R.inc(1415);if ((((handler != null)&&(__CLR3_0_212u12uiw50z4t9.R.iget(1416)!=0|true))||(__CLR3_0_212u12uiw50z4t9.R.iget(1417)==0&false))) {{
      __CLR3_0_212u12uiw50z4t9.R.inc(1418);handler.destroy();
      __CLR3_0_212u12uiw50z4t9.R.inc(1419);handler = null;
    }
    }__CLR3_0_212u12uiw50z4t9.R.inc(1420);handler = getNewAuthenticationHandler();
    __CLR3_0_212u12uiw50z4t9.R.inc(1421);Properties props = getDefaultProperties();
    __CLR3_0_212u12uiw50z4t9.R.inc(1422);props.setProperty("alt-kerberos.non-browser.user-agents", "foo, bar");
    __CLR3_0_212u12uiw50z4t9.R.inc(1423);try {
      __CLR3_0_212u12uiw50z4t9.R.inc(1424);handler.init(props);
    } catch (Exception ex) {
      __CLR3_0_212u12uiw50z4t9.R.inc(1425);handler = null;
      __CLR3_0_212u12uiw50z4t9.R.inc(1426);throw ex;
    }

    // Pretend we're something that will not match with "foo" (or "bar")
    __CLR3_0_212u12uiw50z4t9.R.inc(1427);Mockito.when(request.getHeader("User-Agent")).thenReturn("blah");
    // Should use alt authentication
    __CLR3_0_212u12uiw50z4t9.R.inc(1428);AuthenticationToken token = handler.authenticate(request, response);
    __CLR3_0_212u12uiw50z4t9.R.inc(1429);Assert.assertEquals("A", token.getUserName());
    __CLR3_0_212u12uiw50z4t9.R.inc(1430);Assert.assertEquals("B", token.getName());
    __CLR3_0_212u12uiw50z4t9.R.inc(1431);Assert.assertEquals(getExpectedType(), token.getType());
  }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testNonDefaultNonBrowserUserAgentAsNonBrowser() throws Exception {__CLR3_0_212u12uiw50z4t9.R.globalSliceStart(getClass().getName(),1432);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2exsfir13s();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_212u12uiw50z4t9.R.rethrow($CLV_t2$);}finally{__CLR3_0_212u12uiw50z4t9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAltKerberosAuthenticationHandler.testNonDefaultNonBrowserUserAgentAsNonBrowser",1432,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2exsfir13s() throws Exception{try{__CLR3_0_212u12uiw50z4t9.R.inc(1432);
    __CLR3_0_212u12uiw50z4t9.R.inc(1433);if ((((handler != null)&&(__CLR3_0_212u12uiw50z4t9.R.iget(1434)!=0|true))||(__CLR3_0_212u12uiw50z4t9.R.iget(1435)==0&false))) {{
      __CLR3_0_212u12uiw50z4t9.R.inc(1436);handler.destroy();
      __CLR3_0_212u12uiw50z4t9.R.inc(1437);handler = null;
    }
    }__CLR3_0_212u12uiw50z4t9.R.inc(1438);handler = getNewAuthenticationHandler();
    __CLR3_0_212u12uiw50z4t9.R.inc(1439);Properties props = getDefaultProperties();
    __CLR3_0_212u12uiw50z4t9.R.inc(1440);props.setProperty("alt-kerberos.non-browser.user-agents", "foo, bar");
    __CLR3_0_212u12uiw50z4t9.R.inc(1441);try {
      __CLR3_0_212u12uiw50z4t9.R.inc(1442);handler.init(props);
    } catch (Exception ex) {
      __CLR3_0_212u12uiw50z4t9.R.inc(1443);handler = null;
      __CLR3_0_212u12uiw50z4t9.R.inc(1444);throw ex;
    }

    // Run the kerberos tests again
    __CLR3_0_212u12uiw50z4t9.R.inc(1445);testRequestWithoutAuthorization();
    __CLR3_0_212u12uiw50z4t9.R.inc(1446);testRequestWithInvalidAuthorization();
    __CLR3_0_212u12uiw50z4t9.R.inc(1447);testRequestWithAuthorization();
    __CLR3_0_212u12uiw50z4t9.R.inc(1448);testRequestWithInvalidKerberosAuthorization();
  }finally{__CLR3_0_212u12uiw50z4t9.R.flushNeeded();}}
}
