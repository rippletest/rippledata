/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.apache.hadoop.security.authentication.server.AuthenticationFilter;
import org.apache.hadoop.security.authentication.server.PseudoAuthenticationHandler;
import org.junit.Assert;
import org.junit.Test;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

public class TestPseudoAuthenticator {static class __CLR3_0_211l11liw50z4so{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1398);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  private Properties getAuthenticationHandlerConfiguration(boolean anonymousAllowed) {try{__CLR3_0_211l11liw50z4so.R.inc(1353);
    __CLR3_0_211l11liw50z4so.R.inc(1354);Properties props = new Properties();
    __CLR3_0_211l11liw50z4so.R.inc(1355);props.setProperty(AuthenticationFilter.AUTH_TYPE, "simple");
    __CLR3_0_211l11liw50z4so.R.inc(1356);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, Boolean.toString(anonymousAllowed));
    __CLR3_0_211l11liw50z4so.R.inc(1357);return props;
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testGetUserName() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1358);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2qeuykf11q();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testGetUserName",1358,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2qeuykf11q() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1358);
    __CLR3_0_211l11liw50z4so.R.inc(1359);PseudoAuthenticator authenticator = new PseudoAuthenticator();
    __CLR3_0_211l11liw50z4so.R.inc(1360);Assert.assertEquals(System.getProperty("user.name"), authenticator.getUserName());
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAnonymousAllowed() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1361);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2spo75811t();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAnonymousAllowed",1361,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2spo75811t() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1361);
    __CLR3_0_211l11liw50z4so.R.inc(1362);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1363);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(true));
    __CLR3_0_211l11liw50z4so.R.inc(1364);auth.start();
    __CLR3_0_211l11liw50z4so.R.inc(1365);try {
      __CLR3_0_211l11liw50z4so.R.inc(1366);URL url = new URL(auth.getBaseURL());
      __CLR3_0_211l11liw50z4so.R.inc(1367);HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      __CLR3_0_211l11liw50z4so.R.inc(1368);conn.connect();
      __CLR3_0_211l11liw50z4so.R.inc(1369);Assert.assertEquals(HttpURLConnection.HTTP_OK, conn.getResponseCode());
    } finally {
      __CLR3_0_211l11liw50z4so.R.inc(1370);auth.stop();
    }
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAnonymousDisallowed() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1371);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2n531yc123();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAnonymousDisallowed",1371,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2n531yc123() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1371);
    __CLR3_0_211l11liw50z4so.R.inc(1372);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1373);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(false));
    __CLR3_0_211l11liw50z4so.R.inc(1374);auth.start();
    __CLR3_0_211l11liw50z4so.R.inc(1375);try {
      __CLR3_0_211l11liw50z4so.R.inc(1376);URL url = new URL(auth.getBaseURL());
      __CLR3_0_211l11liw50z4so.R.inc(1377);HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      __CLR3_0_211l11liw50z4so.R.inc(1378);conn.connect();
      __CLR3_0_211l11liw50z4so.R.inc(1379);Assert.assertEquals(HttpURLConnection.HTTP_FORBIDDEN, conn.getResponseCode());
      __CLR3_0_211l11liw50z4so.R.inc(1380);Assert.assertEquals("Anonymous requests are disallowed", conn.getResponseMessage());
    } finally {
      __CLR3_0_211l11liw50z4so.R.inc(1381);auth.stop();
    }
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAuthenticationAnonymousAllowed() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1382);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ram1hw12e();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAuthenticationAnonymousAllowed",1382,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ram1hw12e() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1382);
    __CLR3_0_211l11liw50z4so.R.inc(1383);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1384);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(true));
    __CLR3_0_211l11liw50z4so.R.inc(1385);auth._testAuthentication(new PseudoAuthenticator(), false);
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAuthenticationAnonymousDisallowed() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1386);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2x17pa412i();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAuthenticationAnonymousDisallowed",1386,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2x17pa412i() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1386);
    __CLR3_0_211l11liw50z4so.R.inc(1387);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1388);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(false));
    __CLR3_0_211l11liw50z4so.R.inc(1389);auth._testAuthentication(new PseudoAuthenticator(), false);
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAuthenticationAnonymousAllowedWithPost() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1390);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2pqz9aq12m();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAuthenticationAnonymousAllowedWithPost",1390,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2pqz9aq12m() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1390);
    __CLR3_0_211l11liw50z4so.R.inc(1391);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1392);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(true));
    __CLR3_0_211l11liw50z4so.R.inc(1393);auth._testAuthentication(new PseudoAuthenticator(), true);
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

  @Test
  public void testAuthenticationAnonymousDisallowedWithPost() throws Exception {__CLR3_0_211l11liw50z4so.R.globalSliceStart(getClass().getName(),1394);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_21vh5na12q();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_211l11liw50z4so.R.rethrow($CLV_t2$);}finally{__CLR3_0_211l11liw50z4so.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestPseudoAuthenticator.testAuthenticationAnonymousDisallowedWithPost",1394,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_21vh5na12q() throws Exception{try{__CLR3_0_211l11liw50z4so.R.inc(1394);
    __CLR3_0_211l11liw50z4so.R.inc(1395);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_211l11liw50z4so.R.inc(1396);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration(false));
    __CLR3_0_211l11liw50z4so.R.inc(1397);auth._testAuthentication(new PseudoAuthenticator(), true);
  }finally{__CLR3_0_211l11liw50z4so.R.flushNeeded();}}

}
