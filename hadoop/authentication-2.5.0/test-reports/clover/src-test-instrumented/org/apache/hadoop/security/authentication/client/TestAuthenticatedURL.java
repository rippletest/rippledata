/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestAuthenticatedURL {static class __CLR3_0_2y3y3iw50z4rm{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1300);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Test
  public void testToken() throws Exception {__CLR3_0_2y3y3iw50z4rm.R.globalSliceStart(getClass().getName(),1227);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2er6782y3();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2y3y3iw50z4rm.R.rethrow($CLV_t2$);}finally{__CLR3_0_2y3y3iw50z4rm.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestAuthenticatedURL.testToken",1227,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2er6782y3() throws Exception{try{__CLR3_0_2y3y3iw50z4rm.R.inc(1227);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1228);AuthenticatedURL.Token token = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1229);Assert.assertFalse(token.isSet());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1230);token = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1231);Assert.assertTrue(token.isSet());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1232);Assert.assertEquals("foo", token.toString());

    __CLR3_0_2y3y3iw50z4rm.R.inc(1233);AuthenticatedURL.Token token1 = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1234);AuthenticatedURL.Token token2 = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1235);Assert.assertEquals(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1236);Assert.assertTrue(token1.equals(token2));

    __CLR3_0_2y3y3iw50z4rm.R.inc(1237);token1 = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1238);token2 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1239);Assert.assertNotSame(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1240);Assert.assertFalse(token1.equals(token2));

    __CLR3_0_2y3y3iw50z4rm.R.inc(1241);token1 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1242);token2 = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1243);Assert.assertNotSame(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1244);Assert.assertFalse(token1.equals(token2));

    __CLR3_0_2y3y3iw50z4rm.R.inc(1245);token1 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1246);token2 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1247);Assert.assertEquals(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1248);Assert.assertTrue(token1.equals(token2));

    __CLR3_0_2y3y3iw50z4rm.R.inc(1249);token1 = new AuthenticatedURL.Token("bar");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1250);token2 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1251);Assert.assertNotSame(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1252);Assert.assertFalse(token1.equals(token2));

    __CLR3_0_2y3y3iw50z4rm.R.inc(1253);token1 = new AuthenticatedURL.Token("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1254);token2 = new AuthenticatedURL.Token("bar");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1255);Assert.assertNotSame(token1.hashCode(), token2.hashCode());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1256);Assert.assertFalse(token1.equals(token2));
  }finally{__CLR3_0_2y3y3iw50z4rm.R.flushNeeded();}}

  @Test
  public void testInjectToken() throws Exception {__CLR3_0_2y3y3iw50z4rm.R.globalSliceStart(getClass().getName(),1257);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2wah9c3yx();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2y3y3iw50z4rm.R.rethrow($CLV_t2$);}finally{__CLR3_0_2y3y3iw50z4rm.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestAuthenticatedURL.testInjectToken",1257,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2wah9c3yx() throws Exception{try{__CLR3_0_2y3y3iw50z4rm.R.inc(1257);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1258);HttpURLConnection conn = Mockito.mock(HttpURLConnection.class);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1259);AuthenticatedURL.Token token = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1260);token.set("foo");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1261);AuthenticatedURL.injectToken(conn, token);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1262);Mockito.verify(conn).addRequestProperty(Mockito.eq("Cookie"), Mockito.anyString());
  }finally{__CLR3_0_2y3y3iw50z4rm.R.flushNeeded();}}

  @Test
  public void testExtractTokenOK() throws Exception {__CLR3_0_2y3y3iw50z4rm.R.globalSliceStart(getClass().getName(),1263);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2eju4r1z3();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2y3y3iw50z4rm.R.rethrow($CLV_t2$);}finally{__CLR3_0_2y3y3iw50z4rm.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestAuthenticatedURL.testExtractTokenOK",1263,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2eju4r1z3() throws Exception{try{__CLR3_0_2y3y3iw50z4rm.R.inc(1263);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1264);HttpURLConnection conn = Mockito.mock(HttpURLConnection.class);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1265);Mockito.when(conn.getResponseCode()).thenReturn(HttpURLConnection.HTTP_OK);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1266);String tokenStr = "foo";
    __CLR3_0_2y3y3iw50z4rm.R.inc(1267);Map<String, List<String>> headers = new HashMap<String, List<String>>();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1268);List<String> cookies = new ArrayList<String>();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1269);cookies.add(AuthenticatedURL.AUTH_COOKIE + "=" + tokenStr);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1270);headers.put("Set-Cookie", cookies);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1271);Mockito.when(conn.getHeaderFields()).thenReturn(headers);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1272);AuthenticatedURL.Token token = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1273);AuthenticatedURL.extractToken(conn, token);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1274);Assert.assertEquals(tokenStr, token.toString());
  }finally{__CLR3_0_2y3y3iw50z4rm.R.flushNeeded();}}

  @Test
  public void testExtractTokenFail() throws Exception {__CLR3_0_2y3y3iw50z4rm.R.globalSliceStart(getClass().getName(),1275);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2x93yz5zf();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2y3y3iw50z4rm.R.rethrow($CLV_t2$);}finally{__CLR3_0_2y3y3iw50z4rm.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestAuthenticatedURL.testExtractTokenFail",1275,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2x93yz5zf() throws Exception{try{__CLR3_0_2y3y3iw50z4rm.R.inc(1275);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1276);HttpURLConnection conn = Mockito.mock(HttpURLConnection.class);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1277);Mockito.when(conn.getResponseCode()).thenReturn(HttpURLConnection.HTTP_UNAUTHORIZED);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1278);String tokenStr = "foo";
    __CLR3_0_2y3y3iw50z4rm.R.inc(1279);Map<String, List<String>> headers = new HashMap<String, List<String>>();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1280);List<String> cookies = new ArrayList<String>();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1281);cookies.add(AuthenticatedURL.AUTH_COOKIE + "=" + tokenStr);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1282);headers.put("Set-Cookie", cookies);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1283);Mockito.when(conn.getHeaderFields()).thenReturn(headers);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1284);AuthenticatedURL.Token token = new AuthenticatedURL.Token();
    __CLR3_0_2y3y3iw50z4rm.R.inc(1285);token.set("bar");
    __CLR3_0_2y3y3iw50z4rm.R.inc(1286);try {
      __CLR3_0_2y3y3iw50z4rm.R.inc(1287);AuthenticatedURL.extractToken(conn, token);
      __CLR3_0_2y3y3iw50z4rm.R.inc(1288);Assert.fail();
    } catch (AuthenticationException ex) {
      // Expected
      __CLR3_0_2y3y3iw50z4rm.R.inc(1289);Assert.assertFalse(token.isSet());
    } catch (Exception ex) {
      __CLR3_0_2y3y3iw50z4rm.R.inc(1290);Assert.fail();
    }
  }finally{__CLR3_0_2y3y3iw50z4rm.R.flushNeeded();}}

  @Test
  public void testConnectionConfigurator() throws Exception {__CLR3_0_2y3y3iw50z4rm.R.globalSliceStart(getClass().getName(),1291);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2v6x62czv();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2y3y3iw50z4rm.R.rethrow($CLV_t2$);}finally{__CLR3_0_2y3y3iw50z4rm.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestAuthenticatedURL.testConnectionConfigurator",1291,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2v6x62czv() throws Exception{try{__CLR3_0_2y3y3iw50z4rm.R.inc(1291);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1292);HttpURLConnection conn = Mockito.mock(HttpURLConnection.class);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1293);Mockito.when(conn.getResponseCode()).
        thenReturn(HttpURLConnection.HTTP_UNAUTHORIZED);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1294);ConnectionConfigurator connConf =
        Mockito.mock(ConnectionConfigurator.class);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1295);Mockito.when(connConf.configure(Mockito.<HttpURLConnection>any())).
        thenReturn(conn);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1296);Authenticator authenticator = Mockito.mock(Authenticator.class);

    __CLR3_0_2y3y3iw50z4rm.R.inc(1297);AuthenticatedURL aURL = new AuthenticatedURL(authenticator, connConf);
    __CLR3_0_2y3y3iw50z4rm.R.inc(1298);aURL.openConnection(new URL("http://foo"), new AuthenticatedURL.Token());
    __CLR3_0_2y3y3iw50z4rm.R.inc(1299);Mockito.verify(connConf).configure(Mockito.<HttpURLConnection>any());
  }finally{__CLR3_0_2y3y3iw50z4rm.R.flushNeeded();}}

}
