/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with this
 * work for additional information regarding copyright ownership. The ASF
 * licenses this file to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package org.apache.hadoop.security.authentication.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.directory.server.kerberos.shared.keytab.Keytab;
import org.apache.directory.server.kerberos.shared.keytab.KeytabEntry;
import org.apache.directory.shared.kerberos.KerberosTime;
import org.apache.directory.shared.kerberos.codec.types.EncryptionType;
import org.apache.directory.shared.kerberos.components.EncryptionKey;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class TestKerberosUtil {static class __CLR3_0_21p31p3iw50z4y4{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,2259);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  static String testKeytab = "test.keytab";
  static String[] testPrincipals = new String[]{
      "HTTP@testRealm",
      "test/testhost@testRealm",
      "HTTP/testhost@testRealm",
      "HTTP1/testhost@testRealm",
      "HTTP/testhostanother@testRealm"
  };

  @After
  public void deleteKeytab() {try{__CLR3_0_21p31p3iw50z4y4.R.inc(2199);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2200);File keytabFile = new File(testKeytab);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2201);if ((((keytabFile.exists())&&(__CLR3_0_21p31p3iw50z4y4.R.iget(2202)!=0|true))||(__CLR3_0_21p31p3iw50z4y4.R.iget(2203)==0&false))){{
      __CLR3_0_21p31p3iw50z4y4.R.inc(2204);keytabFile.delete();
    }
  }}finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}

  @Test
  public void testGetServerPrincipal() throws IOException {__CLR3_0_21p31p3iw50z4y4.R.globalSliceStart(getClass().getName(),2205);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_26p29am1p9();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21p31p3iw50z4y4.R.rethrow($CLV_t2$);}finally{__CLR3_0_21p31p3iw50z4y4.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosUtil.testGetServerPrincipal",2205,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_26p29am1p9() throws IOException{try{__CLR3_0_21p31p3iw50z4y4.R.inc(2205);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2206);String service = "TestKerberosUtil";
    __CLR3_0_21p31p3iw50z4y4.R.inc(2207);String localHostname = KerberosUtil.getLocalHostName();
    __CLR3_0_21p31p3iw50z4y4.R.inc(2208);String testHost = "FooBar";

    // send null hostname
    __CLR3_0_21p31p3iw50z4y4.R.inc(2209);Assert.assertEquals("When no hostname is sent",
        service + "/" + localHostname.toLowerCase(),
        KerberosUtil.getServicePrincipal(service, null));
    // send empty hostname
    __CLR3_0_21p31p3iw50z4y4.R.inc(2210);Assert.assertEquals("When empty hostname is sent",
        service + "/" + localHostname.toLowerCase(),
        KerberosUtil.getServicePrincipal(service, ""));
    // send 0.0.0.0 hostname
    __CLR3_0_21p31p3iw50z4y4.R.inc(2211);Assert.assertEquals("When 0.0.0.0 hostname is sent",
        service + "/" + localHostname.toLowerCase(),
        KerberosUtil.getServicePrincipal(service, "0.0.0.0"));
    // send uppercase hostname
    __CLR3_0_21p31p3iw50z4y4.R.inc(2212);Assert.assertEquals("When uppercase hostname is sent",
        service + "/" + testHost.toLowerCase(),
        KerberosUtil.getServicePrincipal(service, testHost));
    // send lowercase hostname
    __CLR3_0_21p31p3iw50z4y4.R.inc(2213);Assert.assertEquals("When lowercase hostname is sent",
        service + "/" + testHost.toLowerCase(),
        KerberosUtil.getServicePrincipal(service, testHost.toLowerCase()));
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}
  
  @Test
  public void testGetPrincipalNamesMissingKeytab() {__CLR3_0_21p31p3iw50z4y4.R.globalSliceStart(getClass().getName(),2214);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2iti52z1pi();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21p31p3iw50z4y4.R.rethrow($CLV_t2$);}finally{__CLR3_0_21p31p3iw50z4y4.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosUtil.testGetPrincipalNamesMissingKeytab",2214,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2iti52z1pi(){try{__CLR3_0_21p31p3iw50z4y4.R.inc(2214);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2215);try {
      __CLR3_0_21p31p3iw50z4y4.R.inc(2216);KerberosUtil.getPrincipalNames(testKeytab);
      __CLR3_0_21p31p3iw50z4y4.R.inc(2217);Assert.fail("Exception should have been thrown");
    } catch (IOException e) {
      //expects exception
    }
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}

  @Test
  public void testGetPrincipalNamesMissingPattern() throws IOException {__CLR3_0_21p31p3iw50z4y4.R.globalSliceStart(getClass().getName(),2218);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_28dsvan1pm();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21p31p3iw50z4y4.R.rethrow($CLV_t2$);}finally{__CLR3_0_21p31p3iw50z4y4.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosUtil.testGetPrincipalNamesMissingPattern",2218,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_28dsvan1pm() throws IOException{try{__CLR3_0_21p31p3iw50z4y4.R.inc(2218);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2219);createKeyTab(testKeytab, new String[]{"test/testhost@testRealm"});
    __CLR3_0_21p31p3iw50z4y4.R.inc(2220);try {
      __CLR3_0_21p31p3iw50z4y4.R.inc(2221);KerberosUtil.getPrincipalNames(testKeytab, null);
      __CLR3_0_21p31p3iw50z4y4.R.inc(2222);Assert.fail("Exception should have been thrown");
    } catch (Exception e) {
      //expects exception
    }
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}

  @Test
  public void testGetPrincipalNamesFromKeytab() throws IOException {__CLR3_0_21p31p3iw50z4y4.R.globalSliceStart(getClass().getName(),2223);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ushchn1pr();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21p31p3iw50z4y4.R.rethrow($CLV_t2$);}finally{__CLR3_0_21p31p3iw50z4y4.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosUtil.testGetPrincipalNamesFromKeytab",2223,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ushchn1pr() throws IOException{try{__CLR3_0_21p31p3iw50z4y4.R.inc(2223);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2224);createKeyTab(testKeytab, testPrincipals); 
    // read all principals in the keytab file
    __CLR3_0_21p31p3iw50z4y4.R.inc(2225);String[] principals = KerberosUtil.getPrincipalNames(testKeytab);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2226);Assert.assertNotNull("principals cannot be null", principals);
    
    __CLR3_0_21p31p3iw50z4y4.R.inc(2227);int expectedSize = 0;
    __CLR3_0_21p31p3iw50z4y4.R.inc(2228);List<String> principalList = Arrays.asList(principals);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2229);for (String principal : testPrincipals) {{
      __CLR3_0_21p31p3iw50z4y4.R.inc(2230);Assert.assertTrue("missing principal "+principal,
          principalList.contains(principal));
      __CLR3_0_21p31p3iw50z4y4.R.inc(2231);expectedSize++;
    }
    }__CLR3_0_21p31p3iw50z4y4.R.inc(2232);Assert.assertEquals(expectedSize, principals.length);
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}
  
  @Test
  public void testGetPrincipalNamesFromKeytabWithPattern() throws IOException {__CLR3_0_21p31p3iw50z4y4.R.globalSliceStart(getClass().getName(),2233);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2fxi4h91q1();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21p31p3iw50z4y4.R.rethrow($CLV_t2$);}finally{__CLR3_0_21p31p3iw50z4y4.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestKerberosUtil.testGetPrincipalNamesFromKeytabWithPattern",2233,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2fxi4h91q1() throws IOException{try{__CLR3_0_21p31p3iw50z4y4.R.inc(2233);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2234);createKeyTab(testKeytab, testPrincipals); 
    // read the keytab file
    // look for principals with HTTP as the first part
    __CLR3_0_21p31p3iw50z4y4.R.inc(2235);Pattern httpPattern = Pattern.compile("HTTP/.*");
    __CLR3_0_21p31p3iw50z4y4.R.inc(2236);String[] httpPrincipals =
        KerberosUtil.getPrincipalNames(testKeytab, httpPattern);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2237);Assert.assertNotNull("principals cannot be null", httpPrincipals);
    
    __CLR3_0_21p31p3iw50z4y4.R.inc(2238);int expectedSize = 0;
    __CLR3_0_21p31p3iw50z4y4.R.inc(2239);List<String> httpPrincipalList = Arrays.asList(httpPrincipals);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2240);for (String principal : testPrincipals) {{
      __CLR3_0_21p31p3iw50z4y4.R.inc(2241);if ((((httpPattern.matcher(principal).matches())&&(__CLR3_0_21p31p3iw50z4y4.R.iget(2242)!=0|true))||(__CLR3_0_21p31p3iw50z4y4.R.iget(2243)==0&false))) {{
        __CLR3_0_21p31p3iw50z4y4.R.inc(2244);Assert.assertTrue("missing principal "+principal,
            httpPrincipalList.contains(principal));
        __CLR3_0_21p31p3iw50z4y4.R.inc(2245);expectedSize++;
      }
    }}
    }__CLR3_0_21p31p3iw50z4y4.R.inc(2246);Assert.assertEquals(expectedSize, httpPrincipals.length);
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}
  
  private void createKeyTab(String fileName, String[] principalNames)
      throws IOException {try{__CLR3_0_21p31p3iw50z4y4.R.inc(2247);
    //create a test keytab file
    __CLR3_0_21p31p3iw50z4y4.R.inc(2248);List<KeytabEntry> lstEntries = new ArrayList<KeytabEntry>();
    __CLR3_0_21p31p3iw50z4y4.R.inc(2249);for (String principal : principalNames){{
      // create 3 versions of the key to ensure methods don't return
      // duplicate principals
      __CLR3_0_21p31p3iw50z4y4.R.inc(2250);for (int kvno=1; (((kvno <= 3)&&(__CLR3_0_21p31p3iw50z4y4.R.iget(2251)!=0|true))||(__CLR3_0_21p31p3iw50z4y4.R.iget(2252)==0&false)); kvno++) {{
        __CLR3_0_21p31p3iw50z4y4.R.inc(2253);EncryptionKey key = new EncryptionKey(
            EncryptionType.UNKNOWN, "samplekey1".getBytes(), kvno);
        __CLR3_0_21p31p3iw50z4y4.R.inc(2254);KeytabEntry keytabEntry = new KeytabEntry(
            principal, 1 , new KerberosTime(), (byte) 1, key);
        __CLR3_0_21p31p3iw50z4y4.R.inc(2255);lstEntries.add(keytabEntry);      
      }
    }}
    }__CLR3_0_21p31p3iw50z4y4.R.inc(2256);Keytab keytab = Keytab.getInstance();
    __CLR3_0_21p31p3iw50z4y4.R.inc(2257);keytab.setEntries(lstEntries);
    __CLR3_0_21p31p3iw50z4y4.R.inc(2258);keytab.write(new File(testKeytab));
  }finally{__CLR3_0_21p31p3iw50z4y4.R.flushNeeded();}}
}