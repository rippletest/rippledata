/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.apache.hadoop.security.authentication.server.AuthenticationFilter;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.FilterHolder;
import org.mortbay.jetty.servlet.ServletHolder;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.util.Properties;
import org.junit.Assert;

public class AuthenticatorTestCase {static class __CLR3_0_2vvvviw50z4r0{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1227);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  private Server server;
  private String host = null;
  private int port = -1;
  Context context;

  private static Properties authenticatorConfig;

  protected static void setAuthenticationHandlerConfig(Properties config) {try{__CLR3_0_2vvvviw50z4r0.R.inc(1147);
    __CLR3_0_2vvvviw50z4r0.R.inc(1148);authenticatorConfig = config;
  }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

  public static class TestFilter extends AuthenticationFilter {

    @Override
    protected Properties getConfiguration(String configPrefix, FilterConfig filterConfig) throws ServletException {try{__CLR3_0_2vvvviw50z4r0.R.inc(1149);
      __CLR3_0_2vvvviw50z4r0.R.inc(1150);return authenticatorConfig;
    }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}
  }

  @SuppressWarnings("serial")
  public static class TestServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {try{__CLR3_0_2vvvviw50z4r0.R.inc(1151);
      __CLR3_0_2vvvviw50z4r0.R.inc(1152);resp.setStatus(HttpServletResponse.SC_OK);
    }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {try{__CLR3_0_2vvvviw50z4r0.R.inc(1153);
      __CLR3_0_2vvvviw50z4r0.R.inc(1154);InputStream is = req.getInputStream();
      __CLR3_0_2vvvviw50z4r0.R.inc(1155);OutputStream os = resp.getOutputStream();
      __CLR3_0_2vvvviw50z4r0.R.inc(1156);int c = is.read();
      __CLR3_0_2vvvviw50z4r0.R.inc(1157);while ((((c > -1)&&(__CLR3_0_2vvvviw50z4r0.R.iget(1158)!=0|true))||(__CLR3_0_2vvvviw50z4r0.R.iget(1159)==0&false))) {{
        __CLR3_0_2vvvviw50z4r0.R.inc(1160);os.write(c);
        __CLR3_0_2vvvviw50z4r0.R.inc(1161);c = is.read();
      }
      }__CLR3_0_2vvvviw50z4r0.R.inc(1162);is.close();
      __CLR3_0_2vvvviw50z4r0.R.inc(1163);os.close();
      __CLR3_0_2vvvviw50z4r0.R.inc(1164);resp.setStatus(HttpServletResponse.SC_OK);
    }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}
  }

  protected void start() throws Exception {try{__CLR3_0_2vvvviw50z4r0.R.inc(1165);
    __CLR3_0_2vvvviw50z4r0.R.inc(1166);server = new Server(0);
    __CLR3_0_2vvvviw50z4r0.R.inc(1167);context = new Context();
    __CLR3_0_2vvvviw50z4r0.R.inc(1168);context.setContextPath("/foo");
    __CLR3_0_2vvvviw50z4r0.R.inc(1169);server.setHandler(context);
    __CLR3_0_2vvvviw50z4r0.R.inc(1170);context.addFilter(new FilterHolder(TestFilter.class), "/*", 0);
    __CLR3_0_2vvvviw50z4r0.R.inc(1171);context.addServlet(new ServletHolder(TestServlet.class), "/bar");
    __CLR3_0_2vvvviw50z4r0.R.inc(1172);host = "localhost";
    __CLR3_0_2vvvviw50z4r0.R.inc(1173);ServerSocket ss = new ServerSocket(0);
    __CLR3_0_2vvvviw50z4r0.R.inc(1174);port = ss.getLocalPort();
    __CLR3_0_2vvvviw50z4r0.R.inc(1175);ss.close();
    __CLR3_0_2vvvviw50z4r0.R.inc(1176);server.getConnectors()[0].setHost(host);
    __CLR3_0_2vvvviw50z4r0.R.inc(1177);server.getConnectors()[0].setPort(port);
    __CLR3_0_2vvvviw50z4r0.R.inc(1178);server.start();
    __CLR3_0_2vvvviw50z4r0.R.inc(1179);System.out.println("Running embedded servlet container at: http://" + host + ":" + port);
  }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

  protected void stop() throws Exception {try{__CLR3_0_2vvvviw50z4r0.R.inc(1180);
    __CLR3_0_2vvvviw50z4r0.R.inc(1181);try {
      __CLR3_0_2vvvviw50z4r0.R.inc(1182);server.stop();
    } catch (Exception e) {
    }

    __CLR3_0_2vvvviw50z4r0.R.inc(1183);try {
      __CLR3_0_2vvvviw50z4r0.R.inc(1184);server.destroy();
    } catch (Exception e) {
    }
  }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

  protected String getBaseURL() {try{__CLR3_0_2vvvviw50z4r0.R.inc(1185);
    __CLR3_0_2vvvviw50z4r0.R.inc(1186);return "http://" + host + ":" + port + "/foo/bar";
  }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

  private static class TestConnectionConfigurator
      implements ConnectionConfigurator {
    boolean invoked;

    @Override
    public HttpURLConnection configure(HttpURLConnection conn)
        throws IOException {try{__CLR3_0_2vvvviw50z4r0.R.inc(1187);
      __CLR3_0_2vvvviw50z4r0.R.inc(1188);invoked = true;
      __CLR3_0_2vvvviw50z4r0.R.inc(1189);return conn;
    }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}
  }

  private String POST = "test";

  protected void _testAuthentication(Authenticator authenticator, boolean doPost) throws Exception {try{__CLR3_0_2vvvviw50z4r0.R.inc(1190);
    __CLR3_0_2vvvviw50z4r0.R.inc(1191);start();
    __CLR3_0_2vvvviw50z4r0.R.inc(1192);try {
      __CLR3_0_2vvvviw50z4r0.R.inc(1193);URL url = new URL(getBaseURL());
      __CLR3_0_2vvvviw50z4r0.R.inc(1194);AuthenticatedURL.Token token = new AuthenticatedURL.Token();
      __CLR3_0_2vvvviw50z4r0.R.inc(1195);Assert.assertFalse(token.isSet());
      __CLR3_0_2vvvviw50z4r0.R.inc(1196);TestConnectionConfigurator connConf = new TestConnectionConfigurator();
      __CLR3_0_2vvvviw50z4r0.R.inc(1197);AuthenticatedURL aUrl = new AuthenticatedURL(authenticator, connConf);
      __CLR3_0_2vvvviw50z4r0.R.inc(1198);HttpURLConnection conn = aUrl.openConnection(url, token);
      __CLR3_0_2vvvviw50z4r0.R.inc(1199);Assert.assertTrue(connConf.invoked);
      __CLR3_0_2vvvviw50z4r0.R.inc(1200);String tokenStr = token.toString();
      __CLR3_0_2vvvviw50z4r0.R.inc(1201);if ((((doPost)&&(__CLR3_0_2vvvviw50z4r0.R.iget(1202)!=0|true))||(__CLR3_0_2vvvviw50z4r0.R.iget(1203)==0&false))) {{
        __CLR3_0_2vvvviw50z4r0.R.inc(1204);conn.setRequestMethod("POST");
        __CLR3_0_2vvvviw50z4r0.R.inc(1205);conn.setDoOutput(true);
      }
      }__CLR3_0_2vvvviw50z4r0.R.inc(1206);conn.connect();
      __CLR3_0_2vvvviw50z4r0.R.inc(1207);if ((((doPost)&&(__CLR3_0_2vvvviw50z4r0.R.iget(1208)!=0|true))||(__CLR3_0_2vvvviw50z4r0.R.iget(1209)==0&false))) {{
        __CLR3_0_2vvvviw50z4r0.R.inc(1210);Writer writer = new OutputStreamWriter(conn.getOutputStream());
        __CLR3_0_2vvvviw50z4r0.R.inc(1211);writer.write(POST);
        __CLR3_0_2vvvviw50z4r0.R.inc(1212);writer.close();
      }
      }__CLR3_0_2vvvviw50z4r0.R.inc(1213);Assert.assertEquals(HttpURLConnection.HTTP_OK, conn.getResponseCode());
      __CLR3_0_2vvvviw50z4r0.R.inc(1214);if ((((doPost)&&(__CLR3_0_2vvvviw50z4r0.R.iget(1215)!=0|true))||(__CLR3_0_2vvvviw50z4r0.R.iget(1216)==0&false))) {{
        __CLR3_0_2vvvviw50z4r0.R.inc(1217);BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        __CLR3_0_2vvvviw50z4r0.R.inc(1218);String echo = reader.readLine();
        __CLR3_0_2vvvviw50z4r0.R.inc(1219);Assert.assertEquals(POST, echo);
        __CLR3_0_2vvvviw50z4r0.R.inc(1220);Assert.assertNull(reader.readLine());
      }
      }__CLR3_0_2vvvviw50z4r0.R.inc(1221);aUrl = new AuthenticatedURL();
      __CLR3_0_2vvvviw50z4r0.R.inc(1222);conn = aUrl.openConnection(url, token);
      __CLR3_0_2vvvviw50z4r0.R.inc(1223);conn.connect();
      __CLR3_0_2vvvviw50z4r0.R.inc(1224);Assert.assertEquals(HttpURLConnection.HTTP_OK, conn.getResponseCode());
      __CLR3_0_2vvvviw50z4r0.R.inc(1225);Assert.assertEquals(tokenStr, token.toString());
    } finally {
      __CLR3_0_2vvvviw50z4r0.R.inc(1226);stop();
    }
  }finally{__CLR3_0_2vvvviw50z4r0.R.flushNeeded();}}

}
