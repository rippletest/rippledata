/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import java.io.IOException;
import java.net.HttpCookie;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.security.authentication.client.AuthenticatedURL;
import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.apache.hadoop.security.authentication.util.Signer;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class TestAuthenticationFilter {static class __CLR3_0_2149149iw50z4v9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1885);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  private static final long TOKEN_VALIDITY_SEC = 1000;

  @Test
  public void testGetConfiguration() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1449);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2zgs45j149();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testGetConfiguration",1449,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2zgs45j149() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1449);
    __CLR3_0_2149149iw50z4v9.R.inc(1450);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1451);FilterConfig config = Mockito.mock(FilterConfig.class);
    __CLR3_0_2149149iw50z4v9.R.inc(1452);Mockito.when(config.getInitParameter(AuthenticationFilter.CONFIG_PREFIX)).thenReturn("");
    __CLR3_0_2149149iw50z4v9.R.inc(1453);Mockito.when(config.getInitParameter("a")).thenReturn("A");
    __CLR3_0_2149149iw50z4v9.R.inc(1454);Mockito.when(config.getInitParameterNames()).thenReturn(new Vector<String>(Arrays.asList("a")).elements());
    __CLR3_0_2149149iw50z4v9.R.inc(1455);Properties props = filter.getConfiguration("", config);
    __CLR3_0_2149149iw50z4v9.R.inc(1456);Assert.assertEquals("A", props.getProperty("a"));

    __CLR3_0_2149149iw50z4v9.R.inc(1457);config = Mockito.mock(FilterConfig.class);
    __CLR3_0_2149149iw50z4v9.R.inc(1458);Mockito.when(config.getInitParameter(AuthenticationFilter.CONFIG_PREFIX)).thenReturn("foo");
    __CLR3_0_2149149iw50z4v9.R.inc(1459);Mockito.when(config.getInitParameter("foo.a")).thenReturn("A");
    __CLR3_0_2149149iw50z4v9.R.inc(1460);Mockito.when(config.getInitParameterNames()).thenReturn(new Vector<String>(Arrays.asList("foo.a")).elements());
    __CLR3_0_2149149iw50z4v9.R.inc(1461);props = filter.getConfiguration("foo.", config);
    __CLR3_0_2149149iw50z4v9.R.inc(1462);Assert.assertEquals("A", props.getProperty("a"));
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testInitEmpty() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1463);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2o6zzbm14n();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testInitEmpty",1463,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2o6zzbm14n() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1463);
    __CLR3_0_2149149iw50z4v9.R.inc(1464);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1465);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1466);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1467);Mockito.when(config.getInitParameterNames()).thenReturn(new Vector<String>().elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1468);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1469);Assert.fail();
    } catch (ServletException ex) {
      // Expected
      __CLR3_0_2149149iw50z4v9.R.inc(1470);Assert.assertEquals("Authentication type must be specified: simple|kerberos|<class>", 
          ex.getMessage());
    } catch (Exception ex) {
      __CLR3_0_2149149iw50z4v9.R.inc(1471);Assert.fail();
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1472);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  public static class DummyAuthenticationHandler implements AuthenticationHandler {
    public static boolean init;
    public static boolean managementOperationReturn;
    public static boolean destroy;
    public static boolean expired;

    public static final String TYPE = "dummy";

    public static void reset() {try{__CLR3_0_2149149iw50z4v9.R.inc(1473);
      __CLR3_0_2149149iw50z4v9.R.inc(1474);init = false;
      __CLR3_0_2149149iw50z4v9.R.inc(1475);destroy = false;
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

    @Override
    public void init(Properties config) throws ServletException {try{__CLR3_0_2149149iw50z4v9.R.inc(1476);
      __CLR3_0_2149149iw50z4v9.R.inc(1477);init = true;
      __CLR3_0_2149149iw50z4v9.R.inc(1478);managementOperationReturn =
        config.getProperty("management.operation.return", "true").equals("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1479);expired = config.getProperty("expired.token", "false").equals("true");
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

    @Override
    public boolean managementOperation(AuthenticationToken token,
                                       HttpServletRequest request,
                                       HttpServletResponse response)
      throws IOException, AuthenticationException {try{__CLR3_0_2149149iw50z4v9.R.inc(1480);
      __CLR3_0_2149149iw50z4v9.R.inc(1481);if ((((!managementOperationReturn)&&(__CLR3_0_2149149iw50z4v9.R.iget(1482)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1483)==0&false))) {{
        __CLR3_0_2149149iw50z4v9.R.inc(1484);response.setStatus(HttpServletResponse.SC_ACCEPTED);
      }
      }__CLR3_0_2149149iw50z4v9.R.inc(1485);return managementOperationReturn;
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

    @Override
    public void destroy() {try{__CLR3_0_2149149iw50z4v9.R.inc(1486);
      __CLR3_0_2149149iw50z4v9.R.inc(1487);destroy = true;
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

    @Override
    public String getType() {try{__CLR3_0_2149149iw50z4v9.R.inc(1488);
      __CLR3_0_2149149iw50z4v9.R.inc(1489);return TYPE;
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

    @Override
    public AuthenticationToken authenticate(HttpServletRequest request, HttpServletResponse response)
      throws IOException, AuthenticationException {try{__CLR3_0_2149149iw50z4v9.R.inc(1490);
      __CLR3_0_2149149iw50z4v9.R.inc(1491);AuthenticationToken token = null;
      __CLR3_0_2149149iw50z4v9.R.inc(1492);String param = request.getParameter("authenticated");
      __CLR3_0_2149149iw50z4v9.R.inc(1493);if ((((param != null && param.equals("true"))&&(__CLR3_0_2149149iw50z4v9.R.iget(1494)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1495)==0&false))) {{
        __CLR3_0_2149149iw50z4v9.R.inc(1496);token = new AuthenticationToken("u", "p", "t");
        __CLR3_0_2149149iw50z4v9.R.inc(1497);token.setExpires(((((expired) )&&(__CLR3_0_2149149iw50z4v9.R.iget(1498)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1499)==0&false))? 0 : System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      } }else {{
        __CLR3_0_2149149iw50z4v9.R.inc(1500);if ((((request.getHeader("WWW-Authenticate") == null)&&(__CLR3_0_2149149iw50z4v9.R.iget(1501)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1502)==0&false))) {{
          __CLR3_0_2149149iw50z4v9.R.inc(1503);response.setHeader("WWW-Authenticate", "dummyauth");
        } }else {{
          __CLR3_0_2149149iw50z4v9.R.inc(1504);throw new AuthenticationException("AUTH FAILED");
        }
      }}
      }__CLR3_0_2149149iw50z4v9.R.inc(1505);return token;
    }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
  }

  @Test
  public void testInit() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1506);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ai0cvr15u();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testInit",1506,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ai0cvr15u() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1506);

    // minimal configuration & simple auth handler (Pseudo)
    __CLR3_0_2149149iw50z4v9.R.inc(1507);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1508);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1509);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1510);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn("simple");
      __CLR3_0_2149149iw50z4v9.R.inc(1511);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TOKEN_VALIDITY)).thenReturn(
          (new Long(TOKEN_VALIDITY_SEC)).toString());
      __CLR3_0_2149149iw50z4v9.R.inc(1512);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                                 AuthenticationFilter.AUTH_TOKEN_VALIDITY)).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1513);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1514);Assert.assertEquals(PseudoAuthenticationHandler.class, filter.getAuthenticationHandler().getClass());
      __CLR3_0_2149149iw50z4v9.R.inc(1515);Assert.assertTrue(filter.isRandomSecret());
      __CLR3_0_2149149iw50z4v9.R.inc(1516);Assert.assertNull(filter.getCookieDomain());
      __CLR3_0_2149149iw50z4v9.R.inc(1517);Assert.assertNull(filter.getCookiePath());
      __CLR3_0_2149149iw50z4v9.R.inc(1518);Assert.assertEquals(TOKEN_VALIDITY_SEC, filter.getValidity());
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1519);filter.destroy();
    }

    // custom secret
    __CLR3_0_2149149iw50z4v9.R.inc(1520);filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1521);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1522);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1523);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn("simple");
      __CLR3_0_2149149iw50z4v9.R.inc(1524);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn("secret");
      __CLR3_0_2149149iw50z4v9.R.inc(1525);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                                 AuthenticationFilter.SIGNATURE_SECRET)).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1526);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1527);Assert.assertFalse(filter.isRandomSecret());
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1528);filter.destroy();
    }

    // custom cookie domain and cookie path
    __CLR3_0_2149149iw50z4v9.R.inc(1529);filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1530);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1531);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1532);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn("simple");
      __CLR3_0_2149149iw50z4v9.R.inc(1533);Mockito.when(config.getInitParameter(AuthenticationFilter.COOKIE_DOMAIN)).thenReturn(".foo.com");
      __CLR3_0_2149149iw50z4v9.R.inc(1534);Mockito.when(config.getInitParameter(AuthenticationFilter.COOKIE_PATH)).thenReturn("/bar");
      __CLR3_0_2149149iw50z4v9.R.inc(1535);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                                 AuthenticationFilter.COOKIE_DOMAIN,
                                 AuthenticationFilter.COOKIE_PATH)).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1536);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1537);Assert.assertEquals(".foo.com", filter.getCookieDomain());
      __CLR3_0_2149149iw50z4v9.R.inc(1538);Assert.assertEquals("/bar", filter.getCookiePath());
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1539);filter.destroy();
    }

    // authentication handler lifecycle, and custom impl
    __CLR3_0_2149149iw50z4v9.R.inc(1540);DummyAuthenticationHandler.reset();
    __CLR3_0_2149149iw50z4v9.R.inc(1541);filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1542);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1543);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1544);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1545);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1546);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1547);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1548);Assert.assertTrue(DummyAuthenticationHandler.init);
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1549);filter.destroy();
      __CLR3_0_2149149iw50z4v9.R.inc(1550);Assert.assertTrue(DummyAuthenticationHandler.destroy);
    }

    // kerberos auth handler
    __CLR3_0_2149149iw50z4v9.R.inc(1551);filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1552);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1553);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1554);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn("kerberos");
      __CLR3_0_2149149iw50z4v9.R.inc(1555);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE)).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1556);filter.init(config);
    } catch (ServletException ex) {
      // Expected
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1557);Assert.assertEquals(KerberosAuthenticationHandler.class, filter.getAuthenticationHandler().getClass());
      __CLR3_0_2149149iw50z4v9.R.inc(1558);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
  
  @Test
  public void testInitCaseSensitivity() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1559);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2418pik17b();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testInitCaseSensitivity",1559,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2418pik17b() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1559);
    // minimal configuration & simple auth handler (Pseudo)
    __CLR3_0_2149149iw50z4v9.R.inc(1560);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1561);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1562);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1563);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn("SimPle");
      __CLR3_0_2149149iw50z4v9.R.inc(1564);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TOKEN_VALIDITY)).thenReturn(
          (new Long(TOKEN_VALIDITY_SEC)).toString());
      __CLR3_0_2149149iw50z4v9.R.inc(1565);Mockito.when(config.getInitParameterNames()).thenReturn(
          new Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
              AuthenticationFilter.AUTH_TOKEN_VALIDITY)).elements());

      __CLR3_0_2149149iw50z4v9.R.inc(1566);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1567);Assert.assertEquals(PseudoAuthenticationHandler.class, 
          filter.getAuthenticationHandler().getClass());
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1568);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testGetRequestURL() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1569);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2hnt9yj17l();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testGetRequestURL",1569,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2hnt9yj17l() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1569);
    __CLR3_0_2149149iw50z4v9.R.inc(1570);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1571);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1572);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1573);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1574);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1575);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1576);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1577);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1578);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));
      __CLR3_0_2149149iw50z4v9.R.inc(1579);Mockito.when(request.getQueryString()).thenReturn("a=A&b=B");

      __CLR3_0_2149149iw50z4v9.R.inc(1580);Assert.assertEquals("http://foo:8080/bar?a=A&b=B", filter.getRequestURL(request));
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1581);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testGetToken() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1582);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_26xsqe417y();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testGetToken",1582,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_26xsqe417y() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1582);
    __CLR3_0_2149149iw50z4v9.R.inc(1583);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1584);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1585);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1586);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1587);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1588);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn("secret");
      __CLR3_0_2149149iw50z4v9.R.inc(1589);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        AuthenticationFilter.SIGNATURE_SECRET,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1590);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1591);AuthenticationToken token = new AuthenticationToken("u", "p", DummyAuthenticationHandler.TYPE);
      __CLR3_0_2149149iw50z4v9.R.inc(1592);token.setExpires(System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1593);Signer signer = new Signer("secret".getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1594);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1595);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1596);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1597);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1598);AuthenticationToken newToken = filter.getToken(request);

      __CLR3_0_2149149iw50z4v9.R.inc(1599);Assert.assertEquals(token.toString(), newToken.toString());
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1600);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testGetTokenExpired() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1601);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2kmy2x518h();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testGetTokenExpired",1601,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2kmy2x518h() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1601);
    __CLR3_0_2149149iw50z4v9.R.inc(1602);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1603);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1604);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1605);Mockito.when(config.getInitParameter("management.operation.return")).thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1606);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1607);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn("secret");
      __CLR3_0_2149149iw50z4v9.R.inc(1608);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        AuthenticationFilter.SIGNATURE_SECRET,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1609);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1610);AuthenticationToken token =
          new AuthenticationToken("u", "p", DummyAuthenticationHandler.TYPE);
      __CLR3_0_2149149iw50z4v9.R.inc(1611);token.setExpires(System.currentTimeMillis() - TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1612);Signer signer = new Signer("secret".getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1613);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1614);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1615);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1616);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1617);boolean failed = false;
      __CLR3_0_2149149iw50z4v9.R.inc(1618);try {
        __CLR3_0_2149149iw50z4v9.R.inc(1619);filter.getToken(request);
      } catch (AuthenticationException ex) {
        __CLR3_0_2149149iw50z4v9.R.inc(1620);Assert.assertEquals("AuthenticationToken expired", ex.getMessage());
        __CLR3_0_2149149iw50z4v9.R.inc(1621);failed = true;
      } finally {
        __CLR3_0_2149149iw50z4v9.R.inc(1622);Assert.assertTrue("token not expired", failed);
      }
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1623);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testGetTokenInvalidType() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1624);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2ul9eal194();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testGetTokenInvalidType",1624,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2ul9eal194() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1624);
    __CLR3_0_2149149iw50z4v9.R.inc(1625);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1626);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1627);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1628);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1629);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1630);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn("secret");
      __CLR3_0_2149149iw50z4v9.R.inc(1631);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        AuthenticationFilter.SIGNATURE_SECRET,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1632);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1633);AuthenticationToken token = new AuthenticationToken("u", "p", "invalidtype");
      __CLR3_0_2149149iw50z4v9.R.inc(1634);token.setExpires(System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1635);Signer signer = new Signer("secret".getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1636);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1637);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1638);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1639);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1640);boolean failed = false;
      __CLR3_0_2149149iw50z4v9.R.inc(1641);try {
        __CLR3_0_2149149iw50z4v9.R.inc(1642);filter.getToken(request);
      } catch (AuthenticationException ex) {
        __CLR3_0_2149149iw50z4v9.R.inc(1643);Assert.assertEquals("Invalid AuthenticationToken type", ex.getMessage());
        __CLR3_0_2149149iw50z4v9.R.inc(1644);failed = true;
      } finally {
        __CLR3_0_2149149iw50z4v9.R.inc(1645);Assert.assertTrue("token not invalid type", failed);
      }
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1646);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterNotAuthenticated() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1647);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2cg2l8o19r();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterNotAuthenticated",1647,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2cg2l8o19r() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1647);
    __CLR3_0_2149149iw50z4v9.R.inc(1648);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1649);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1650);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1651);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1652);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1653);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1654);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1655);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1656);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));

      __CLR3_0_2149149iw50z4v9.R.inc(1657);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1658);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1659);Mockito.doAnswer(
        new Answer<Object>() {
          @Override
          public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1660);
            __CLR3_0_2149149iw50z4v9.R.inc(1661);Assert.fail();
            __CLR3_0_2149149iw50z4v9.R.inc(1662);return null;
          }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
        }
      ).when(chain).doFilter(Mockito.<ServletRequest>anyObject(), Mockito.<ServletResponse>anyObject());

      __CLR3_0_2149149iw50z4v9.R.inc(1663);filter.doFilter(request, response, chain);

      __CLR3_0_2149149iw50z4v9.R.inc(1664);Mockito.verify(response).sendError(
          HttpServletResponse.SC_UNAUTHORIZED, "Authentication required");
      __CLR3_0_2149149iw50z4v9.R.inc(1665);Mockito.verify(response).setHeader("WWW-Authenticate", "dummyauth");
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1666);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  private void _testDoFilterAuthentication(boolean withDomainPath,
                                           boolean invalidToken,
                                           boolean expired) throws Exception {try{__CLR3_0_2149149iw50z4v9.R.inc(1667);
    __CLR3_0_2149149iw50z4v9.R.inc(1668);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1669);FilterConfig config = Mockito.mock(FilterConfig.class);
    __CLR3_0_2149149iw50z4v9.R.inc(1670);Mockito.when(config.getInitParameter("management.operation.return")).
            thenReturn("true");
    __CLR3_0_2149149iw50z4v9.R.inc(1671);Mockito.when(config.getInitParameter("expired.token")).
            thenReturn(Boolean.toString(expired));
    __CLR3_0_2149149iw50z4v9.R.inc(1672);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE))
            .thenReturn(DummyAuthenticationHandler.class.getName());
    __CLR3_0_2149149iw50z4v9.R.inc(1673);Mockito.when(config.getInitParameter(AuthenticationFilter
            .AUTH_TOKEN_VALIDITY)).thenReturn(new Long(TOKEN_VALIDITY_SEC).toString());
    __CLR3_0_2149149iw50z4v9.R.inc(1674);Mockito.when(config.getInitParameter(AuthenticationFilter
            .SIGNATURE_SECRET)).thenReturn("secret");
    __CLR3_0_2149149iw50z4v9.R.inc(1675);Mockito.when(config.getInitParameterNames()).thenReturn(new
            Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
            AuthenticationFilter.AUTH_TOKEN_VALIDITY,
            AuthenticationFilter.SIGNATURE_SECRET, "management.operation" +
            ".return", "expired.token")).elements());

    __CLR3_0_2149149iw50z4v9.R.inc(1676);if ((((withDomainPath)&&(__CLR3_0_2149149iw50z4v9.R.iget(1677)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1678)==0&false))) {{
      __CLR3_0_2149149iw50z4v9.R.inc(1679);Mockito.when(config.getInitParameter(AuthenticationFilter
              .COOKIE_DOMAIN)).thenReturn(".foo.com");
      __CLR3_0_2149149iw50z4v9.R.inc(1680);Mockito.when(config.getInitParameter(AuthenticationFilter.COOKIE_PATH))
              .thenReturn("/bar");
      __CLR3_0_2149149iw50z4v9.R.inc(1681);Mockito.when(config.getInitParameterNames()).thenReturn(new
              Vector<String>(Arrays.asList(AuthenticationFilter.AUTH_TYPE,
              AuthenticationFilter.AUTH_TOKEN_VALIDITY,
              AuthenticationFilter.SIGNATURE_SECRET,
              AuthenticationFilter.COOKIE_DOMAIN, AuthenticationFilter
              .COOKIE_PATH, "management.operation.return")).elements());
    }

    }__CLR3_0_2149149iw50z4v9.R.inc(1682);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    __CLR3_0_2149149iw50z4v9.R.inc(1683);Mockito.when(request.getParameter("authenticated")).thenReturn("true");
    __CLR3_0_2149149iw50z4v9.R.inc(1684);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer
            ("http://foo:8080/bar"));
    __CLR3_0_2149149iw50z4v9.R.inc(1685);Mockito.when(request.getQueryString()).thenReturn("authenticated=true");

    __CLR3_0_2149149iw50z4v9.R.inc(1686);if ((((invalidToken)&&(__CLR3_0_2149149iw50z4v9.R.iget(1687)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1688)==0&false))) {{
      __CLR3_0_2149149iw50z4v9.R.inc(1689);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{new Cookie
              (AuthenticatedURL.AUTH_COOKIE, "foo")});
    }

    }__CLR3_0_2149149iw50z4v9.R.inc(1690);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
    __CLR3_0_2149149iw50z4v9.R.inc(1691);FilterChain chain = Mockito.mock(FilterChain.class);

    __CLR3_0_2149149iw50z4v9.R.inc(1692);final HashMap<String, String> cookieMap = new HashMap<String, String>();
    __CLR3_0_2149149iw50z4v9.R.inc(1693);Mockito.doAnswer(new Answer<Object>() {
      @Override
      public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1694);
        __CLR3_0_2149149iw50z4v9.R.inc(1695);String cookieHeader = (String)invocation.getArguments()[1];
        __CLR3_0_2149149iw50z4v9.R.inc(1696);parseCookieMap(cookieHeader, cookieMap);
        __CLR3_0_2149149iw50z4v9.R.inc(1697);return null;
      }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
    }).when(response).addHeader(Mockito.eq("Set-Cookie"), Mockito.anyString());

    __CLR3_0_2149149iw50z4v9.R.inc(1698);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1699);filter.init(config);
      __CLR3_0_2149149iw50z4v9.R.inc(1700);filter.doFilter(request, response, chain);

      __CLR3_0_2149149iw50z4v9.R.inc(1701);if ((((expired)&&(__CLR3_0_2149149iw50z4v9.R.iget(1702)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1703)==0&false))) {{
        __CLR3_0_2149149iw50z4v9.R.inc(1704);Mockito.verify(response, Mockito.never()).
          addHeader(Mockito.eq("Set-Cookie"), Mockito.anyString());
      } }else {{
        __CLR3_0_2149149iw50z4v9.R.inc(1705);String v = cookieMap.get(AuthenticatedURL.AUTH_COOKIE);
        __CLR3_0_2149149iw50z4v9.R.inc(1706);Assert.assertNotNull("cookie missing", v);
        __CLR3_0_2149149iw50z4v9.R.inc(1707);Assert.assertTrue(v.contains("u=") && v.contains("p=") && v.contains
                ("t=") && v.contains("e=") && v.contains("s="));
        __CLR3_0_2149149iw50z4v9.R.inc(1708);Mockito.verify(chain).doFilter(Mockito.any(ServletRequest.class),
                Mockito.any(ServletResponse.class));

        __CLR3_0_2149149iw50z4v9.R.inc(1709);Signer signer = new Signer("secret".getBytes());
        __CLR3_0_2149149iw50z4v9.R.inc(1710);String value = signer.verifyAndExtract(v);
        __CLR3_0_2149149iw50z4v9.R.inc(1711);AuthenticationToken token = AuthenticationToken.parse(value);
        __CLR3_0_2149149iw50z4v9.R.inc(1712);assertThat(token.getExpires(), not(0L));

        __CLR3_0_2149149iw50z4v9.R.inc(1713);if ((((withDomainPath)&&(__CLR3_0_2149149iw50z4v9.R.iget(1714)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1715)==0&false))) {{
          __CLR3_0_2149149iw50z4v9.R.inc(1716);Assert.assertEquals(".foo.com", cookieMap.get("Domain"));
          __CLR3_0_2149149iw50z4v9.R.inc(1717);Assert.assertEquals("/bar", cookieMap.get("Path"));
        } }else {{
          __CLR3_0_2149149iw50z4v9.R.inc(1718);Assert.assertFalse(cookieMap.containsKey("Domain"));
          __CLR3_0_2149149iw50z4v9.R.inc(1719);Assert.assertFalse(cookieMap.containsKey("Path"));
        }
      }}
    }} finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1720);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  private static void parseCookieMap(String cookieHeader, HashMap<String,
          String> cookieMap) {try{__CLR3_0_2149149iw50z4v9.R.inc(1721);
    __CLR3_0_2149149iw50z4v9.R.inc(1722);List<HttpCookie> cookies = HttpCookie.parse(cookieHeader);
    __CLR3_0_2149149iw50z4v9.R.inc(1723);for (HttpCookie cookie : cookies) {{
      __CLR3_0_2149149iw50z4v9.R.inc(1724);if ((((AuthenticatedURL.AUTH_COOKIE.equals(cookie.getName()))&&(__CLR3_0_2149149iw50z4v9.R.iget(1725)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1726)==0&false))) {{
        __CLR3_0_2149149iw50z4v9.R.inc(1727);cookieMap.put(cookie.getName(), cookie.getValue());
        __CLR3_0_2149149iw50z4v9.R.inc(1728);if ((((cookie.getPath() != null)&&(__CLR3_0_2149149iw50z4v9.R.iget(1729)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1730)==0&false))) {{
          __CLR3_0_2149149iw50z4v9.R.inc(1731);cookieMap.put("Path", cookie.getPath());
        }
        }__CLR3_0_2149149iw50z4v9.R.inc(1732);if ((((cookie.getDomain() != null)&&(__CLR3_0_2149149iw50z4v9.R.iget(1733)!=0|true))||(__CLR3_0_2149149iw50z4v9.R.iget(1734)==0&false))) {{
          __CLR3_0_2149149iw50z4v9.R.inc(1735);cookieMap.put("Domain", cookie.getDomain());
        }
      }}
    }}
  }}finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthentication() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1736);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_227mwb81c8();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthentication",1736,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_227mwb81c8() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1736);
    __CLR3_0_2149149iw50z4v9.R.inc(1737);_testDoFilterAuthentication(false, false, false);
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticationImmediateExpiration() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1738);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2bkcu4m1ca();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticationImmediateExpiration",1738,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2bkcu4m1ca() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1738);
    __CLR3_0_2149149iw50z4v9.R.inc(1739);_testDoFilterAuthentication(false, false, true);
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticationWithInvalidToken() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1740);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2b0rasc1cc();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticationWithInvalidToken",1740,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2b0rasc1cc() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1740);
    __CLR3_0_2149149iw50z4v9.R.inc(1741);_testDoFilterAuthentication(false, true, false);
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticationWithDomainPath() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1742);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_24pzz0j1ce();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticationWithDomainPath",1742,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_24pzz0j1ce() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1742);
    __CLR3_0_2149149iw50z4v9.R.inc(1743);_testDoFilterAuthentication(true, false, false);
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticated() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1744);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2xwp2731cg();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticated",1744,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2xwp2731cg() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1744);
    __CLR3_0_2149149iw50z4v9.R.inc(1745);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1746);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1747);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1748);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1749);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1750);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1751);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1752);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1753);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));

      __CLR3_0_2149149iw50z4v9.R.inc(1754);AuthenticationToken token = new AuthenticationToken("u", "p", "t");
      __CLR3_0_2149149iw50z4v9.R.inc(1755);token.setExpires(System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1756);Signer signer = new Signer("secret".getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1757);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1758);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1759);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1760);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1761);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1762);Mockito.doAnswer(
        new Answer<Object>() {
          @Override
          public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1763);
            __CLR3_0_2149149iw50z4v9.R.inc(1764);Object[] args = invocation.getArguments();
            __CLR3_0_2149149iw50z4v9.R.inc(1765);HttpServletRequest request = (HttpServletRequest) args[0];
            __CLR3_0_2149149iw50z4v9.R.inc(1766);Assert.assertEquals("u", request.getRemoteUser());
            __CLR3_0_2149149iw50z4v9.R.inc(1767);Assert.assertEquals("p", request.getUserPrincipal().getName());
            __CLR3_0_2149149iw50z4v9.R.inc(1768);return null;
          }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
        }
      ).when(chain).doFilter(Mockito.<ServletRequest>anyObject(), Mockito.<ServletResponse>anyObject());

      __CLR3_0_2149149iw50z4v9.R.inc(1769);filter.doFilter(request, response, chain);

    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1770);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticationFailure() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1771);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2as8jr81d7();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticationFailure",1771,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2as8jr81d7() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1771);
    __CLR3_0_2149149iw50z4v9.R.inc(1772);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1773);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1774);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1775);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1776);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1777);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1778);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1779);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1780);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));
      __CLR3_0_2149149iw50z4v9.R.inc(1781);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{});
      __CLR3_0_2149149iw50z4v9.R.inc(1782);Mockito.when(request.getHeader("WWW-Authenticate")).thenReturn("dummyauth");
      __CLR3_0_2149149iw50z4v9.R.inc(1783);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1784);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1785);final HashMap<String, String> cookieMap = new HashMap<String, String>();
      __CLR3_0_2149149iw50z4v9.R.inc(1786);Mockito.doAnswer(
        new Answer<Object>() {
          @Override
          public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1787);
            __CLR3_0_2149149iw50z4v9.R.inc(1788);Object[] args = invocation.getArguments();
            __CLR3_0_2149149iw50z4v9.R.inc(1789);parseCookieMap((String) args[1], cookieMap);
            __CLR3_0_2149149iw50z4v9.R.inc(1790);return null;
          }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
        }
      ).when(response).addHeader(Mockito.eq("Set-Cookie"), Mockito.anyString());

      __CLR3_0_2149149iw50z4v9.R.inc(1791);Mockito.doAnswer(
        new Answer<Object>() {
          @Override
          public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1792);
            __CLR3_0_2149149iw50z4v9.R.inc(1793);Assert.fail("shouldn't get here");
            __CLR3_0_2149149iw50z4v9.R.inc(1794);return null;
          }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
        }
      ).when(chain).doFilter(Mockito.<ServletRequest>anyObject(), Mockito.<ServletResponse>anyObject());

      __CLR3_0_2149149iw50z4v9.R.inc(1795);filter.doFilter(request, response, chain);

      __CLR3_0_2149149iw50z4v9.R.inc(1796);Mockito.verify(response).sendError(
          HttpServletResponse.SC_FORBIDDEN, "AUTH FAILED");
      __CLR3_0_2149149iw50z4v9.R.inc(1797);Mockito.verify(response, Mockito.never()).setHeader(Mockito.eq("WWW-Authenticate"), Mockito.anyString());

      __CLR3_0_2149149iw50z4v9.R.inc(1798);String value = cookieMap.get(AuthenticatedURL.AUTH_COOKIE);
      __CLR3_0_2149149iw50z4v9.R.inc(1799);Assert.assertNotNull("cookie missing", value);
      __CLR3_0_2149149iw50z4v9.R.inc(1800);Assert.assertEquals("", value);
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1801);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticatedExpired() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1802);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_214q48e1e2();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticatedExpired",1802,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_214q48e1e2() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1802);
    __CLR3_0_2149149iw50z4v9.R.inc(1803);String secret = "secret";
    __CLR3_0_2149149iw50z4v9.R.inc(1804);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1805);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1806);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1807);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1808);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1809);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn(
        secret);
      __CLR3_0_2149149iw50z4v9.R.inc(1810);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        AuthenticationFilter.SIGNATURE_SECRET,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1811);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1812);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1813);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));

      __CLR3_0_2149149iw50z4v9.R.inc(1814);AuthenticationToken token = new AuthenticationToken("u", "p", DummyAuthenticationHandler.TYPE);
      __CLR3_0_2149149iw50z4v9.R.inc(1815);token.setExpires(System.currentTimeMillis() - TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1816);Signer signer = new Signer(secret.getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1817);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1818);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1819);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1820);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1821);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1822);verifyUnauthorized(filter, request, response, chain);
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1823);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  private static void verifyUnauthorized(AuthenticationFilter filter,
                                         HttpServletRequest request,
                                         HttpServletResponse response,
                                         FilterChain chain) throws
                                                            IOException,
                                                            ServletException {try{__CLR3_0_2149149iw50z4v9.R.inc(1824);
    __CLR3_0_2149149iw50z4v9.R.inc(1825);final HashMap<String, String> cookieMap = new HashMap<String, String>();
    __CLR3_0_2149149iw50z4v9.R.inc(1826);Mockito.doAnswer(new Answer<Object>() {
      @Override
      public Object answer(InvocationOnMock invocation) throws Throwable {try{__CLR3_0_2149149iw50z4v9.R.inc(1827);
        __CLR3_0_2149149iw50z4v9.R.inc(1828);String cookieHeader = (String) invocation.getArguments()[1];
        __CLR3_0_2149149iw50z4v9.R.inc(1829);parseCookieMap(cookieHeader, cookieMap);
        __CLR3_0_2149149iw50z4v9.R.inc(1830);return null;
      }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}
    }).when(response).addHeader(Mockito.eq("Set-Cookie"), Mockito.anyString());

    __CLR3_0_2149149iw50z4v9.R.inc(1831);filter.doFilter(request, response, chain);

    __CLR3_0_2149149iw50z4v9.R.inc(1832);Mockito.verify(response).sendError(Mockito.eq(HttpServletResponse
            .SC_UNAUTHORIZED), Mockito.anyString());
    __CLR3_0_2149149iw50z4v9.R.inc(1833);Mockito.verify(chain, Mockito.never()).doFilter(Mockito.any
            (ServletRequest.class), Mockito.any(ServletResponse.class));

    __CLR3_0_2149149iw50z4v9.R.inc(1834);Assert.assertTrue("cookie is missing",
        cookieMap.containsKey(AuthenticatedURL.AUTH_COOKIE));
    __CLR3_0_2149149iw50z4v9.R.inc(1835);Assert.assertEquals("", cookieMap.get(AuthenticatedURL.AUTH_COOKIE));
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testDoFilterAuthenticatedInvalidType() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1836);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2deoyf21f0();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testDoFilterAuthenticatedInvalidType",1836,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2deoyf21f0() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1836);
    __CLR3_0_2149149iw50z4v9.R.inc(1837);String secret = "secret";
    __CLR3_0_2149149iw50z4v9.R.inc(1838);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1839);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1840);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1841);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("true");
      __CLR3_0_2149149iw50z4v9.R.inc(1842);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).thenReturn(
        DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1843);Mockito.when(config.getInitParameter(AuthenticationFilter.SIGNATURE_SECRET)).thenReturn(
        secret);
      __CLR3_0_2149149iw50z4v9.R.inc(1844);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        AuthenticationFilter.SIGNATURE_SECRET,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1845);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1846);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1847);Mockito.when(request.getRequestURL()).thenReturn(new StringBuffer("http://foo:8080/bar"));

      __CLR3_0_2149149iw50z4v9.R.inc(1848);AuthenticationToken token = new AuthenticationToken("u", "p", "invalidtype");
      __CLR3_0_2149149iw50z4v9.R.inc(1849);token.setExpires(System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1850);Signer signer = new Signer(secret.getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1851);String tokenSigned = signer.sign(token.toString());

      __CLR3_0_2149149iw50z4v9.R.inc(1852);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1853);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1854);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1855);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1856);verifyUnauthorized(filter, request, response, chain);
    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1857);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

  @Test
  public void testManagementOperation() throws Exception {__CLR3_0_2149149iw50z4v9.R.globalSliceStart(getClass().getName(),1858);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2h6y54p1fm();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2149149iw50z4v9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2149149iw50z4v9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationFilter.testManagementOperation",1858,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2h6y54p1fm() throws Exception{try{__CLR3_0_2149149iw50z4v9.R.inc(1858);
    __CLR3_0_2149149iw50z4v9.R.inc(1859);AuthenticationFilter filter = new AuthenticationFilter();
    __CLR3_0_2149149iw50z4v9.R.inc(1860);try {
      __CLR3_0_2149149iw50z4v9.R.inc(1861);FilterConfig config = Mockito.mock(FilterConfig.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1862);Mockito.when(config.getInitParameter("management.operation.return")).
        thenReturn("false");
      __CLR3_0_2149149iw50z4v9.R.inc(1863);Mockito.when(config.getInitParameter(AuthenticationFilter.AUTH_TYPE)).
        thenReturn(DummyAuthenticationHandler.class.getName());
      __CLR3_0_2149149iw50z4v9.R.inc(1864);Mockito.when(config.getInitParameterNames()).thenReturn(
        new Vector<String>(
          Arrays.asList(AuthenticationFilter.AUTH_TYPE,
                        "management.operation.return")).elements());
      __CLR3_0_2149149iw50z4v9.R.inc(1865);filter.init(config);

      __CLR3_0_2149149iw50z4v9.R.inc(1866);HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
      __CLR3_0_2149149iw50z4v9.R.inc(1867);Mockito.when(request.getRequestURL()).
        thenReturn(new StringBuffer("http://foo:8080/bar"));

      __CLR3_0_2149149iw50z4v9.R.inc(1868);HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1869);FilterChain chain = Mockito.mock(FilterChain.class);

      __CLR3_0_2149149iw50z4v9.R.inc(1870);filter.doFilter(request, response, chain);
      __CLR3_0_2149149iw50z4v9.R.inc(1871);Mockito.verify(response).setStatus(HttpServletResponse.SC_ACCEPTED);
      __CLR3_0_2149149iw50z4v9.R.inc(1872);Mockito.verifyNoMoreInteractions(response);

      __CLR3_0_2149149iw50z4v9.R.inc(1873);Mockito.reset(request);
      __CLR3_0_2149149iw50z4v9.R.inc(1874);Mockito.reset(response);

      __CLR3_0_2149149iw50z4v9.R.inc(1875);AuthenticationToken token = new AuthenticationToken("u", "p", "t");
      __CLR3_0_2149149iw50z4v9.R.inc(1876);token.setExpires(System.currentTimeMillis() + TOKEN_VALIDITY_SEC);
      __CLR3_0_2149149iw50z4v9.R.inc(1877);Signer signer = new Signer("secret".getBytes());
      __CLR3_0_2149149iw50z4v9.R.inc(1878);String tokenSigned = signer.sign(token.toString());
      __CLR3_0_2149149iw50z4v9.R.inc(1879);Cookie cookie = new Cookie(AuthenticatedURL.AUTH_COOKIE, tokenSigned);
      __CLR3_0_2149149iw50z4v9.R.inc(1880);Mockito.when(request.getCookies()).thenReturn(new Cookie[]{cookie});

      __CLR3_0_2149149iw50z4v9.R.inc(1881);filter.doFilter(request, response, chain);

      __CLR3_0_2149149iw50z4v9.R.inc(1882);Mockito.verify(response).setStatus(HttpServletResponse.SC_ACCEPTED);
      __CLR3_0_2149149iw50z4v9.R.inc(1883);Mockito.verifyNoMoreInteractions(response);

    } finally {
      __CLR3_0_2149149iw50z4v9.R.inc(1884);filter.destroy();
    }
  }finally{__CLR3_0_2149149iw50z4v9.R.flushNeeded();}}

}
