/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.server;

import org.apache.hadoop.security.authentication.client.AuthenticationException;
import org.junit.Assert;
import org.junit.Test;

public class TestAuthenticationToken {static class __CLR3_0_21gd1gdiw50z4we{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1951);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Test
  public void testAnonymous() {__CLR3_0_21gd1gdiw50z4we.R.globalSliceStart(getClass().getName(),1885);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2slbd3y1gd();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21gd1gdiw50z4we.R.rethrow($CLV_t2$);}finally{__CLR3_0_21gd1gdiw50z4we.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationToken.testAnonymous",1885,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2slbd3y1gd(){try{__CLR3_0_21gd1gdiw50z4we.R.inc(1885);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1886);Assert.assertNotNull(AuthenticationToken.ANONYMOUS);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1887);Assert.assertEquals(null, AuthenticationToken.ANONYMOUS.getUserName());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1888);Assert.assertEquals(null, AuthenticationToken.ANONYMOUS.getName());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1889);Assert.assertEquals(null, AuthenticationToken.ANONYMOUS.getType());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1890);Assert.assertEquals(-1, AuthenticationToken.ANONYMOUS.getExpires());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1891);Assert.assertFalse(AuthenticationToken.ANONYMOUS.isExpired());
  }finally{__CLR3_0_21gd1gdiw50z4we.R.flushNeeded();}}

  @Test
  public void testConstructor() throws Exception {__CLR3_0_21gd1gdiw50z4we.R.globalSliceStart(getClass().getName(),1892);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2uefs8h1gk();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21gd1gdiw50z4we.R.rethrow($CLV_t2$);}finally{__CLR3_0_21gd1gdiw50z4we.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationToken.testConstructor",1892,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2uefs8h1gk() throws Exception{try{__CLR3_0_21gd1gdiw50z4we.R.inc(1892);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1893);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1894);new AuthenticationToken(null, "p", "t");
      __CLR3_0_21gd1gdiw50z4we.R.inc(1895);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1896);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1897);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1898);new AuthenticationToken("", "p", "t");
      __CLR3_0_21gd1gdiw50z4we.R.inc(1899);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1900);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1901);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1902);new AuthenticationToken("u", null, "t");
      __CLR3_0_21gd1gdiw50z4we.R.inc(1903);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1904);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1905);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1906);new AuthenticationToken("u", "", "t");
      __CLR3_0_21gd1gdiw50z4we.R.inc(1907);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1908);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1909);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1910);new AuthenticationToken("u", "p", null);
      __CLR3_0_21gd1gdiw50z4we.R.inc(1911);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1912);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1913);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1914);new AuthenticationToken("u", "p", "");
      __CLR3_0_21gd1gdiw50z4we.R.inc(1915);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1916);Assert.fail();
    }
    __CLR3_0_21gd1gdiw50z4we.R.inc(1917);new AuthenticationToken("u", "p", "t");
  }finally{__CLR3_0_21gd1gdiw50z4we.R.flushNeeded();}}

  @Test
  public void testGetters() throws Exception {__CLR3_0_21gd1gdiw50z4we.R.globalSliceStart(getClass().getName(),1918);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2jiqsv71ha();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21gd1gdiw50z4we.R.rethrow($CLV_t2$);}finally{__CLR3_0_21gd1gdiw50z4we.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationToken.testGetters",1918,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2jiqsv71ha() throws Exception{try{__CLR3_0_21gd1gdiw50z4we.R.inc(1918);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1919);long expires = System.currentTimeMillis() + 50;
    __CLR3_0_21gd1gdiw50z4we.R.inc(1920);AuthenticationToken token = new AuthenticationToken("u", "p", "t");
    __CLR3_0_21gd1gdiw50z4we.R.inc(1921);token.setExpires(expires);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1922);Assert.assertEquals("u", token.getUserName());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1923);Assert.assertEquals("p", token.getName());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1924);Assert.assertEquals("t", token.getType());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1925);Assert.assertEquals(expires, token.getExpires());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1926);Assert.assertFalse(token.isExpired());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1927);Thread.sleep(51);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1928);Assert.assertTrue(token.isExpired());
  }finally{__CLR3_0_21gd1gdiw50z4we.R.flushNeeded();}}

  @Test
  public void testToStringAndParse() throws Exception {__CLR3_0_21gd1gdiw50z4we.R.globalSliceStart(getClass().getName(),1929);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2jciov1hl();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21gd1gdiw50z4we.R.rethrow($CLV_t2$);}finally{__CLR3_0_21gd1gdiw50z4we.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationToken.testToStringAndParse",1929,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2jciov1hl() throws Exception{try{__CLR3_0_21gd1gdiw50z4we.R.inc(1929);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1930);long expires = System.currentTimeMillis() + 50;
    __CLR3_0_21gd1gdiw50z4we.R.inc(1931);AuthenticationToken token = new AuthenticationToken("u", "p", "t");
    __CLR3_0_21gd1gdiw50z4we.R.inc(1932);token.setExpires(expires);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1933);String str = token.toString();
    __CLR3_0_21gd1gdiw50z4we.R.inc(1934);token = AuthenticationToken.parse(str);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1935);Assert.assertEquals("p", token.getName());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1936);Assert.assertEquals("t", token.getType());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1937);Assert.assertEquals(expires, token.getExpires());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1938);Assert.assertFalse(token.isExpired());
    __CLR3_0_21gd1gdiw50z4we.R.inc(1939);Thread.sleep(51);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1940);Assert.assertTrue(token.isExpired());
  }finally{__CLR3_0_21gd1gdiw50z4we.R.flushNeeded();}}

  @Test
  public void testParseInvalid() throws Exception {__CLR3_0_21gd1gdiw50z4we.R.globalSliceStart(getClass().getName(),1941);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2o10xp1hx();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21gd1gdiw50z4we.R.rethrow($CLV_t2$);}finally{__CLR3_0_21gd1gdiw50z4we.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.server.TestAuthenticationToken.testParseInvalid",1941,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2o10xp1hx() throws Exception{try{__CLR3_0_21gd1gdiw50z4we.R.inc(1941);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1942);long expires = System.currentTimeMillis() + 50;
    __CLR3_0_21gd1gdiw50z4we.R.inc(1943);AuthenticationToken token = new AuthenticationToken("u", "p", "t");
    __CLR3_0_21gd1gdiw50z4we.R.inc(1944);token.setExpires(expires);
    __CLR3_0_21gd1gdiw50z4we.R.inc(1945);String str = token.toString();
    __CLR3_0_21gd1gdiw50z4we.R.inc(1946);str = str.substring(0, str.indexOf("e="));
    __CLR3_0_21gd1gdiw50z4we.R.inc(1947);try {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1948);AuthenticationToken.parse(str);
      __CLR3_0_21gd1gdiw50z4we.R.inc(1949);Assert.fail();
    } catch (AuthenticationException ex) {
      // Expected
    } catch (Exception ex) {
      __CLR3_0_21gd1gdiw50z4we.R.inc(1950);Assert.fail();
    }
  }finally{__CLR3_0_21gd1gdiw50z4we.R.flushNeeded();}}
}
