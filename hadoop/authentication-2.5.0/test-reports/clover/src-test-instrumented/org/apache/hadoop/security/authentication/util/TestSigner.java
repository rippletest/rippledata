/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.util;

import org.junit.Assert;
import org.junit.Test;

public class TestSigner {static class __CLR3_0_21qr1qriw50z4yh{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,2301);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Test
  public void testNoSecret() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2259);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_26fw9ee1qr();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testNoSecret",2259,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_26fw9ee1qr() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2259);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2260);try {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2261);new Signer(null);
      __CLR3_0_21qr1qriw50z4yh.R.inc(2262);Assert.fail();
    }
    catch (IllegalArgumentException ex) {
    }
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}

  @Test
  public void testNullAndEmptyString() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2263);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2wrxc551qv();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testNullAndEmptyString",2263,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2wrxc551qv() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2263);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2264);Signer signer = new Signer("secret".getBytes());
    __CLR3_0_21qr1qriw50z4yh.R.inc(2265);try {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2266);signer.sign(null);
      __CLR3_0_21qr1qriw50z4yh.R.inc(2267);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2268);Assert.fail();
    }
    __CLR3_0_21qr1qriw50z4yh.R.inc(2269);try {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2270);signer.sign("");
      __CLR3_0_21qr1qriw50z4yh.R.inc(2271);Assert.fail();
    } catch (IllegalArgumentException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2272);Assert.fail();
    }
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}

  @Test
  public void testSignature() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2273);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_21kstgz1r5();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testSignature",2273,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_21kstgz1r5() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2273);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2274);Signer signer = new Signer("secret".getBytes());
    __CLR3_0_21qr1qriw50z4yh.R.inc(2275);String s1 = signer.sign("ok");
    __CLR3_0_21qr1qriw50z4yh.R.inc(2276);String s2 = signer.sign("ok");
    __CLR3_0_21qr1qriw50z4yh.R.inc(2277);String s3 = signer.sign("wrong");
    __CLR3_0_21qr1qriw50z4yh.R.inc(2278);Assert.assertEquals(s1, s2);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2279);Assert.assertNotSame(s1, s3);
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}

  @Test
  public void testVerify() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2280);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2wfqctq1rc();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testVerify",2280,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2wfqctq1rc() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2280);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2281);Signer signer = new Signer("secret".getBytes());
    __CLR3_0_21qr1qriw50z4yh.R.inc(2282);String t = "test";
    __CLR3_0_21qr1qriw50z4yh.R.inc(2283);String s = signer.sign(t);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2284);String e = signer.verifyAndExtract(s);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2285);Assert.assertEquals(t, e);
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}

  @Test
  public void testInvalidSignedText() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2286);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2kfop1x1ri();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testInvalidSignedText",2286,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2kfop1x1ri() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2286);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2287);Signer signer = new Signer("secret".getBytes());
    __CLR3_0_21qr1qriw50z4yh.R.inc(2288);try {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2289);signer.verifyAndExtract("test");
      __CLR3_0_21qr1qriw50z4yh.R.inc(2290);Assert.fail();
    } catch (SignerException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2291);Assert.fail();
    }
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}

  @Test
  public void testTampering() throws Exception {__CLR3_0_21qr1qriw50z4yh.R.globalSliceStart(getClass().getName(),2292);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2abjf461ro();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_21qr1qriw50z4yh.R.rethrow($CLV_t2$);}finally{__CLR3_0_21qr1qriw50z4yh.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.util.TestSigner.testTampering",2292,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2abjf461ro() throws Exception{try{__CLR3_0_21qr1qriw50z4yh.R.inc(2292);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2293);Signer signer = new Signer("secret".getBytes());
    __CLR3_0_21qr1qriw50z4yh.R.inc(2294);String t = "test";
    __CLR3_0_21qr1qriw50z4yh.R.inc(2295);String s = signer.sign(t);
    __CLR3_0_21qr1qriw50z4yh.R.inc(2296);s += "x";
    __CLR3_0_21qr1qriw50z4yh.R.inc(2297);try {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2298);signer.verifyAndExtract(s);
      __CLR3_0_21qr1qriw50z4yh.R.inc(2299);Assert.fail();
    } catch (SignerException ex) {
      // Expected
    } catch (Throwable ex) {
      __CLR3_0_21qr1qriw50z4yh.R.inc(2300);Assert.fail();
    }
  }finally{__CLR3_0_21qr1qriw50z4yh.R.flushNeeded();}}
}
