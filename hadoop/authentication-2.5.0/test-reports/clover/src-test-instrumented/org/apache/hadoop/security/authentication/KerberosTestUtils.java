/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication;

import javax.security.auth.Subject;
import javax.security.auth.kerberos.KerberosPrincipal;
import javax.security.auth.login.AppConfigurationEntry;
import javax.security.auth.login.Configuration;
import javax.security.auth.login.LoginContext;

import org.apache.hadoop.security.authentication.util.KerberosUtil;

import java.io.File;
import java.security.Principal;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.UUID;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Test helper class for Java Kerberos setup.
 */
public class KerberosTestUtils {static class __CLR3_0_2uiuiiw50z4q3{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1147);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}
  private static String keytabFile = new File(System.getProperty("test.dir", "target"),
          UUID.randomUUID().toString()).toString();

  public static String getRealm() {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1098);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1099);return "EXAMPLE.COM";
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  public static String getClientPrincipal() {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1100);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1101);return "client@EXAMPLE.COM";
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  public static String getServerPrincipal() {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1102);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1103);return "HTTP/localhost@EXAMPLE.COM";
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  public static String getKeytabFile() {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1104);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1105);return keytabFile;
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  private static class KerberosConfiguration extends Configuration {
    private String principal;

    public KerberosConfiguration(String principal) {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1106);
      __CLR3_0_2uiuiiw50z4q3.R.inc(1107);this.principal = principal;
    }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

    @Override
    public AppConfigurationEntry[] getAppConfigurationEntry(String name) {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1108);
      __CLR3_0_2uiuiiw50z4q3.R.inc(1109);Map<String, String> options = new HashMap<String, String>();
      __CLR3_0_2uiuiiw50z4q3.R.inc(1110);options.put("keyTab", KerberosTestUtils.getKeytabFile());
      __CLR3_0_2uiuiiw50z4q3.R.inc(1111);options.put("principal", principal);
      __CLR3_0_2uiuiiw50z4q3.R.inc(1112);options.put("useKeyTab", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1113);options.put("storeKey", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1114);options.put("doNotPrompt", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1115);options.put("useTicketCache", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1116);options.put("renewTGT", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1117);options.put("refreshKrb5Config", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1118);options.put("isInitiator", "true");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1119);String ticketCache = System.getenv("KRB5CCNAME");
      __CLR3_0_2uiuiiw50z4q3.R.inc(1120);if ((((ticketCache != null)&&(__CLR3_0_2uiuiiw50z4q3.R.iget(1121)!=0|true))||(__CLR3_0_2uiuiiw50z4q3.R.iget(1122)==0&false))) {{
        __CLR3_0_2uiuiiw50z4q3.R.inc(1123);options.put("ticketCache", ticketCache);
      }
      }__CLR3_0_2uiuiiw50z4q3.R.inc(1124);options.put("debug", "true");

      __CLR3_0_2uiuiiw50z4q3.R.inc(1125);return new AppConfigurationEntry[]{
        new AppConfigurationEntry(KerberosUtil.getKrb5LoginModuleName(),
                                  AppConfigurationEntry.LoginModuleControlFlag.REQUIRED,
                                  options),};
    }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}
  }

  public static <T> T doAs(String principal, final Callable<T> callable) throws Exception {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1126);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1127);LoginContext loginContext = null;
    __CLR3_0_2uiuiiw50z4q3.R.inc(1128);try {
      __CLR3_0_2uiuiiw50z4q3.R.inc(1129);Set<Principal> principals = new HashSet<Principal>();
      __CLR3_0_2uiuiiw50z4q3.R.inc(1130);principals.add(new KerberosPrincipal(KerberosTestUtils.getClientPrincipal()));
      __CLR3_0_2uiuiiw50z4q3.R.inc(1131);Subject subject = new Subject(false, principals, new HashSet<Object>(), new HashSet<Object>());
      __CLR3_0_2uiuiiw50z4q3.R.inc(1132);loginContext = new LoginContext("", subject, null, new KerberosConfiguration(principal));
      __CLR3_0_2uiuiiw50z4q3.R.inc(1133);loginContext.login();
      __CLR3_0_2uiuiiw50z4q3.R.inc(1134);subject = loginContext.getSubject();
      __CLR3_0_2uiuiiw50z4q3.R.inc(1135);return Subject.doAs(subject, new PrivilegedExceptionAction<T>() {
        @Override
        public T run() throws Exception {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1136);
          __CLR3_0_2uiuiiw50z4q3.R.inc(1137);return callable.call();
        }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}
      });
    } catch (PrivilegedActionException ex) {
      __CLR3_0_2uiuiiw50z4q3.R.inc(1138);throw ex.getException();
    } finally {
      __CLR3_0_2uiuiiw50z4q3.R.inc(1139);if ((((loginContext != null)&&(__CLR3_0_2uiuiiw50z4q3.R.iget(1140)!=0|true))||(__CLR3_0_2uiuiiw50z4q3.R.iget(1141)==0&false))) {{
        __CLR3_0_2uiuiiw50z4q3.R.inc(1142);loginContext.logout();
      }
    }}
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  public static <T> T doAsClient(Callable<T> callable) throws Exception {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1143);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1144);return doAs(getClientPrincipal(), callable);
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

  public static <T> T doAsServer(Callable<T> callable) throws Exception {try{__CLR3_0_2uiuiiw50z4q3.R.inc(1145);
    __CLR3_0_2uiuiiw50z4q3.R.inc(1146);return doAs(getServerPrincipal(), callable);
  }finally{__CLR3_0_2uiuiiw50z4q3.R.flushNeeded();}}

}
