/* $$ This file has been instrumented by Clover 3.0.2#20100413144746177 $$ *//**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. See accompanying LICENSE file.
 */
package org.apache.hadoop.security.authentication.client;

import org.apache.hadoop.minikdc.KerberosSecurityTestcase;
import org.apache.hadoop.security.authentication.KerberosTestUtils;
import org.apache.hadoop.security.authentication.server.AuthenticationFilter;
import org.apache.hadoop.security.authentication.server.PseudoAuthenticationHandler;
import org.apache.hadoop.security.authentication.server.KerberosAuthenticationHandler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.Callable;

public class TestKerberosAuthenticator extends KerberosSecurityTestcase {static class __CLR3_0_2104104iw50z4s9{public static final com_cenqua_clover.CoverageRecorder R;static{com_cenqua_clover.CoverageRecorder _R=null;try{com_cenqua_clover.CloverVersionInfo.An_old_version_of_clover_is_on_your_compilation_classpath___Please_remove___Required_version_is___3_0_2();if(20100413144746177L!=com_cenqua_clover.CloverVersionInfo.getBuildStamp()){com_cenqua_clover.Clover.l("[CLOVER] WARNING: The Clover version used in instrumentation does not match the runtime version. You need to run instrumented classes against the same version of Clover that you instrumented with.");com_cenqua_clover.Clover.l("[CLOVER] WARNING: Instr=3.0.2#20100413144746177,Runtime="+com_cenqua_clover.CloverVersionInfo.getReleaseNum()+"#"+com_cenqua_clover.CloverVersionInfo.getBuildStamp());}_R=com_cenqua_clover.Clover.getNullRecorder();_R=com_cenqua_clover.Clover.getRecorder(new char[]{47,104,111,109,101,47,116,121,117,47,68,111,99,117,109,101,110,116,115,47,104,97,100,111,111,112,45,50,46,53,46,50,45,115,114,99,47,104,97,100,111,111,112,45,99,111,109,109,111,110,45,112,114,111,106,101,99,116,47,104,97,100,111,111,112,45,97,117,116,104,47,116,97,114,103,101,116,47,99,108,111,118,101,114,47,104,97,100,111,111,112,45,99,111,118,101,114,97,103,101,46,100,98},null,1480515958437L,8589935092L,1353);}catch(java.lang.SecurityException e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because it has insufficient security privileges. Please consult the Clover documentation on the security policy file changes required. ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.NoClassDefFoundError e){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised. Are you sure you have Clover in the runtime classpath? ("+e.getClass()+":"+e.getMessage()+")");}catch(java.lang.Throwable t){java.lang.System.err.println("[CLOVER] FATAL ERROR: Clover could not be initialised because of an unexpected error. ("+t.getClass()+":"+t.getMessage()+")");}R=_R;}}

  @Before
  public void setup() throws Exception {try{__CLR3_0_2104104iw50z4s9.R.inc(1300);
    // create keytab
    __CLR3_0_2104104iw50z4s9.R.inc(1301);File keytabFile = new File(KerberosTestUtils.getKeytabFile());
    __CLR3_0_2104104iw50z4s9.R.inc(1302);String clientPrincipal = KerberosTestUtils.getClientPrincipal();
    __CLR3_0_2104104iw50z4s9.R.inc(1303);String serverPrincipal = KerberosTestUtils.getServerPrincipal();
    __CLR3_0_2104104iw50z4s9.R.inc(1304);clientPrincipal = clientPrincipal.substring(0, clientPrincipal.lastIndexOf("@"));
    __CLR3_0_2104104iw50z4s9.R.inc(1305);serverPrincipal = serverPrincipal.substring(0, serverPrincipal.lastIndexOf("@"));
    __CLR3_0_2104104iw50z4s9.R.inc(1306);getKdc().createPrincipal(keytabFile, clientPrincipal, serverPrincipal);
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  private Properties getAuthenticationHandlerConfiguration() {try{__CLR3_0_2104104iw50z4s9.R.inc(1307);
    __CLR3_0_2104104iw50z4s9.R.inc(1308);Properties props = new Properties();
    __CLR3_0_2104104iw50z4s9.R.inc(1309);props.setProperty(AuthenticationFilter.AUTH_TYPE, "kerberos");
    __CLR3_0_2104104iw50z4s9.R.inc(1310);props.setProperty(KerberosAuthenticationHandler.PRINCIPAL, KerberosTestUtils.getServerPrincipal());
    __CLR3_0_2104104iw50z4s9.R.inc(1311);props.setProperty(KerberosAuthenticationHandler.KEYTAB, KerberosTestUtils.getKeytabFile());
    __CLR3_0_2104104iw50z4s9.R.inc(1312);props.setProperty(KerberosAuthenticationHandler.NAME_RULES,
                      "RULE:[1:$1@$0](.*@" + KerberosTestUtils.getRealm()+")s/@.*//\n");
    __CLR3_0_2104104iw50z4s9.R.inc(1313);return props;
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testFallbacktoPseudoAuthenticator() throws Exception {__CLR3_0_2104104iw50z4s9.R.globalSliceStart(getClass().getName(),1314);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_24fbkj710i();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2104104iw50z4s9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2104104iw50z4s9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestKerberosAuthenticator.testFallbacktoPseudoAuthenticator",1314,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_24fbkj710i() throws Exception{try{__CLR3_0_2104104iw50z4s9.R.inc(1314);
    __CLR3_0_2104104iw50z4s9.R.inc(1315);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_2104104iw50z4s9.R.inc(1316);Properties props = new Properties();
    __CLR3_0_2104104iw50z4s9.R.inc(1317);props.setProperty(AuthenticationFilter.AUTH_TYPE, "simple");
    __CLR3_0_2104104iw50z4s9.R.inc(1318);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, "false");
    __CLR3_0_2104104iw50z4s9.R.inc(1319);AuthenticatorTestCase.setAuthenticationHandlerConfig(props);
    __CLR3_0_2104104iw50z4s9.R.inc(1320);auth._testAuthentication(new KerberosAuthenticator(), false);
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testFallbacktoPseudoAuthenticatorAnonymous() throws Exception {__CLR3_0_2104104iw50z4s9.R.globalSliceStart(getClass().getName(),1321);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2237ovi10p();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2104104iw50z4s9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2104104iw50z4s9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestKerberosAuthenticator.testFallbacktoPseudoAuthenticatorAnonymous",1321,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2237ovi10p() throws Exception{try{__CLR3_0_2104104iw50z4s9.R.inc(1321);
    __CLR3_0_2104104iw50z4s9.R.inc(1322);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_2104104iw50z4s9.R.inc(1323);Properties props = new Properties();
    __CLR3_0_2104104iw50z4s9.R.inc(1324);props.setProperty(AuthenticationFilter.AUTH_TYPE, "simple");
    __CLR3_0_2104104iw50z4s9.R.inc(1325);props.setProperty(PseudoAuthenticationHandler.ANONYMOUS_ALLOWED, "true");
    __CLR3_0_2104104iw50z4s9.R.inc(1326);AuthenticatorTestCase.setAuthenticationHandlerConfig(props);
    __CLR3_0_2104104iw50z4s9.R.inc(1327);auth._testAuthentication(new KerberosAuthenticator(), false);
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testNotAuthenticated() throws Exception {__CLR3_0_2104104iw50z4s9.R.globalSliceStart(getClass().getName(),1328);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2q0tzfv10w();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2104104iw50z4s9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2104104iw50z4s9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestKerberosAuthenticator.testNotAuthenticated",1328,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2q0tzfv10w() throws Exception{try{__CLR3_0_2104104iw50z4s9.R.inc(1328);
    __CLR3_0_2104104iw50z4s9.R.inc(1329);AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_2104104iw50z4s9.R.inc(1330);AuthenticatorTestCase.setAuthenticationHandlerConfig(getAuthenticationHandlerConfiguration());
    __CLR3_0_2104104iw50z4s9.R.inc(1331);auth.start();
    __CLR3_0_2104104iw50z4s9.R.inc(1332);try {
      __CLR3_0_2104104iw50z4s9.R.inc(1333);URL url = new URL(auth.getBaseURL());
      __CLR3_0_2104104iw50z4s9.R.inc(1334);HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      __CLR3_0_2104104iw50z4s9.R.inc(1335);conn.connect();
      __CLR3_0_2104104iw50z4s9.R.inc(1336);Assert.assertEquals(HttpURLConnection.HTTP_UNAUTHORIZED, conn.getResponseCode());
      __CLR3_0_2104104iw50z4s9.R.inc(1337);Assert.assertTrue(conn.getHeaderField(KerberosAuthenticator.WWW_AUTHENTICATE) != null);
    } finally {
      __CLR3_0_2104104iw50z4s9.R.inc(1338);auth.stop();
    }
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testAuthentication() throws Exception {__CLR3_0_2104104iw50z4s9.R.globalSliceStart(getClass().getName(),1339);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_21r5u41117();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2104104iw50z4s9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2104104iw50z4s9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestKerberosAuthenticator.testAuthentication",1339,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_21r5u41117() throws Exception{try{__CLR3_0_2104104iw50z4s9.R.inc(1339);
    __CLR3_0_2104104iw50z4s9.R.inc(1340);final AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_2104104iw50z4s9.R.inc(1341);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration());
    __CLR3_0_2104104iw50z4s9.R.inc(1342);KerberosTestUtils.doAsClient(new Callable<Void>() {
      @Override
      public Void call() throws Exception {try{__CLR3_0_2104104iw50z4s9.R.inc(1343);
        __CLR3_0_2104104iw50z4s9.R.inc(1344);auth._testAuthentication(new KerberosAuthenticator(), false);
        __CLR3_0_2104104iw50z4s9.R.inc(1345);return null;
      }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}
    });
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}

  @Test(timeout=60000)
  public void testAuthenticationPost() throws Exception {__CLR3_0_2104104iw50z4s9.R.globalSliceStart(getClass().getName(),1346);int $CLV_p$=0;java.lang.Throwable $CLV_t$=null;try{__CLR3_0_2gzkej311e();$CLV_p$=1;}catch(java.lang.Throwable $CLV_t2$){if($CLV_p$==0&&$CLV_t$==null){$CLV_t$=$CLV_t2$;}__CLR3_0_2104104iw50z4s9.R.rethrow($CLV_t2$);}finally{__CLR3_0_2104104iw50z4s9.R.globalSliceEnd(getClass().getName(),"org.apache.hadoop.security.authentication.client.TestKerberosAuthenticator.testAuthenticationPost",1346,$CLV_p$,$CLV_t$);}}private void  __CLR3_0_2gzkej311e() throws Exception{try{__CLR3_0_2104104iw50z4s9.R.inc(1346);
    __CLR3_0_2104104iw50z4s9.R.inc(1347);final AuthenticatorTestCase auth = new AuthenticatorTestCase();
    __CLR3_0_2104104iw50z4s9.R.inc(1348);AuthenticatorTestCase.setAuthenticationHandlerConfig(
            getAuthenticationHandlerConfiguration());
    __CLR3_0_2104104iw50z4s9.R.inc(1349);KerberosTestUtils.doAsClient(new Callable<Void>() {
      @Override
      public Void call() throws Exception {try{__CLR3_0_2104104iw50z4s9.R.inc(1350);
        __CLR3_0_2104104iw50z4s9.R.inc(1351);auth._testAuthentication(new KerberosAuthenticator(), true);
        __CLR3_0_2104104iw50z4s9.R.inc(1352);return null;
      }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}
    });
  }finally{__CLR3_0_2104104iw50z4s9.R.flushNeeded();}}
}
